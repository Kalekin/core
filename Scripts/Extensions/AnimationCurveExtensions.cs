﻿using UnityEngine;


namespace Core.Extensions
{
	public static class AnimationCurveExtensions
	{
		public static void ClearKeys(this AnimationCurve curve)
		{
			for (int i = curve.length - 1; i >= 0; i--)
			{
				curve.RemoveKey(i);
			}
		}
	}
}
