﻿using UnityEngine;


namespace Core.Extensions
{
    public static class TransformExtensions
    {
        public static float Distance(this Transform transform, Vector3 worldPosition)
        {
            return Vector3.Distance(transform.position, worldPosition);
        }


        public static float Distance(this Transform transform, Transform other)
        {
            return Vector3.Distance(transform.position, other.position);
        }


        public static float LocalDistance(this Transform transform, Vector3 localPosition)
        {
            return Vector3.Distance(transform.localPosition, localPosition);
        }


        public static float LocalDistance(this Transform transform, Transform other)
        {
            return Vector3.Distance(transform.localPosition, other.localPosition);
        }


	    public static void ResetLocal(this Transform transform)
	    {
		    transform.localPosition = Vector3.zero;
		    transform.localScale = Vector3.one;
		    transform.localRotation = Quaternion.identity;
	    }


	    public static void SetParentAndResetLocal(this Transform transform, Transform parent)
	    {
		    transform.SetParent(parent, true);
			transform.ResetLocal();
	    }
    }
}
