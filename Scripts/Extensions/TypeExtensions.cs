﻿using System;


namespace CoreEditor
{
	public static class TypeExtensions
	{
		public static bool IsSubclassOfRawGeneric(this Type type, Type generic)
		{
			if (type == generic)
			{
				return false;
			}

			while (type != null && type != typeof(object))
			{
				var cur = type.IsGenericType ? type.GetGenericTypeDefinition() : type;

				if (generic == cur)
				{
					return true;
				}

				type = type.BaseType;
			}

			return false;
		}


		public static Type GetBaseTypeRawGeneric(this Type type, Type generic)
		{
			if (type == generic)
			{
				return type;
			}

			while (type != null && type != typeof(object))
			{
				var cur = type.IsGenericType ? type.GetGenericTypeDefinition() : type;

				if (generic == cur)
				{
					return type;
				}

				type = type.BaseType;
			}

			return null;
		}
	}
}
