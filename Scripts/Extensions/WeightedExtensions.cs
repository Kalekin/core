﻿using System;
using System.Collections.Generic;
using Core.Debug;
using Core.Types;
using Random = UnityEngine.Random;


namespace Core.Extensions
{
	public interface IWeighted
	{
		float Weight { get; }
	}


	public static class WeightedExtensions
	{
		public static T RandomWeighted<T>(this IList<T> list)
			where T : IWeighted
		{

			return list.RandomWeighted(list.TotalWeight());
		}


		public static T RandomWeighted<T>(this IList<T> list, float totalWeight)
			where T : IWeighted
		{
			Assert.IsNotNull(list);
			Assert.IsTrue(list.Count > 0, "Cannot random from an empty list");

			float v = Random.Range(0f, totalWeight);
			float w = 0f;

			for (int i = 0; i < list.Count; i++)
			{
				T e = list[i];
				w += e.Weight;

				if (w >= v)
				{
					return e;
				}
			}

			Assert.Fail("TotalWeight and list weight do not match.");
			return default(T);
		}


		public static T RandomWeighted<T>(this IList<T> list, RetroRandom random)
			where T : IWeighted
		{
			return list.RandomWeighted(random, list.TotalWeight());
		}


		public static T RandomWeighted<T>(this IList<T> list, RetroRandom random, float totalWeight)
			where T : IWeighted
		{
			Assert.IsNotNull(list);
			Assert.IsTrue(list.Count > 0, "Cannot random from an empty list");

			float v = random.Range(0f, totalWeight);
			float w = 0f;

			for (int i = 0; i < list.Count; i++)
			{
				T e = list[i];
				w += e.Weight;

				if (w >= v)
				{
					return e;
				}
			}

			Assert.Fail("TotalWeight and list weight do not match.");
			return default(T);
		}


		public static T RandomAndRemoveWeighted<T>(this IList<T> list)
			where T : IWeighted
		{
			return list.RandomAndRemoveWeighted(list.TotalWeight());
		}


		public static T RandomAndRemoveWeighted<T>(this IList<T> list, float totalWeight)
			where T : IWeighted
		{
			Assert.IsNotNull(list);
			Assert.IsFalse(list.IsReadOnly);
			Assert.IsTrue(list.Count > 0, "Cannot random from an empty list");

			float v = Random.Range(0f, totalWeight);
			float w = 0f;

			for (int i = 0; i < list.Count; i++)
			{
				T e = list[i];
				w += e.Weight;

				if (w >= v)
				{
					list.RemoveAt(i);
					return e;
				}
			}

			Assert.Fail("TotalWeight and list weight do not match.");
			return default(T);
		}


		public static T RandomAndRemoveWeighted<T>(this IList<T> list, RetroRandom random)
			where T : IWeighted
		{
			return list.RandomAndRemoveWeighted(random, list.TotalWeight());
		}


		public static T RandomAndRemoveWeighted<T>(this IList<T> list, RetroRandom random, float totalWeight)
			where T : IWeighted
		{
			Assert.IsNotNull(list);
			Assert.IsFalse(list.IsReadOnly);
			Assert.IsTrue(list.Count > 0, "Cannot random from an empty list");

			float v = random.Range(0f, totalWeight);
			float w = 0f;

			for (int i = 0; i < list.Count; i++)
			{
				T e = list[i];
				w += e.Weight;

				if (w >= v)
				{
					list.RemoveAt(i);
					return e;
				}
			}

			throw new InvalidOperationException("TotalWeight and list weight do not match.");
		}


		public static T RandomAndRemoveWeightedUnsorted<T>(this IList<T> list)
			where T : IWeighted
		{
			return list.RandomAndRemoveWeightedUnsorted(list.TotalWeight());
		}


		public static T RandomAndRemoveWeightedUnsorted<T>(this IList<T> list, float totalWeight)
			where T : IWeighted
		{
			Assert.IsNotNull(list);
			Assert.IsFalse(list.IsReadOnly);
			Assert.IsTrue(list.Count > 0, "Cannot random from an empty list");

			float v = Random.Range(0f, totalWeight);
			float w = 0f;

			for (int i = 0; i < list.Count; i++)
			{
				T e = list[i];
				w += e.Weight;

				if (w >= v)
				{
					int index = list.Count - 1;
					list[i] = list[index];
					list.RemoveAt(index);
					return e;
				}
			}

			Assert.Fail("TotalWeight and list weight do not match.");
			return default(T);
		}


		public static T RandomAndRemoveWeightedUnsorted<T>(this IList<T> list, RetroRandom random)
			where T : IWeighted
		{
			return list.RandomAndRemoveWeightedUnsorted(random, list.TotalWeight());
		}


		public static T RandomAndRemoveWeightedUnsorted<T>(this IList<T> list, RetroRandom random, float totalWeight)
			where T : IWeighted
		{
			Assert.IsNotNull(list);
			Assert.IsFalse(list.IsReadOnly);
			Assert.IsTrue(list.Count > 0, "Cannot random from an empty list");

			float v = random.Range(0f, totalWeight);
			float w = 0f;

			for (int i = 0; i < list.Count; i++)
			{
				T e = list[i];
				w += e.Weight;

				if (w >= v)
				{
					int index = list.Count - 1;
					list[i] = list[index];
					list.RemoveAt(index);
					return e;
				}
			}

			Assert.Fail("TotalWeight and list weight do not match.");
			return default(T);
		}


		public static float TotalWeight<T>(this IList<T> list)
			where T : IWeighted
		{
			Assert.IsNotNull(list);

			if (list.Count == 0)
			{
				return 0f;
			}

			float totalWeight = 0f;

			for (int i = 0; i < list.Count; i++)
			{
				T e = list[i];
				totalWeight += e.Weight;
			}

			return totalWeight;
		}
	}
}