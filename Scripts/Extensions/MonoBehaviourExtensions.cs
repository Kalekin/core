﻿using System;
using System.Collections;
using Core.Routine;
using UnityEngine;


namespace Core.Extensions
{
	public static class MonoBehaviourExtensions
	{
		public static CoreRoutine StartCoreRoutine(this MonoBehaviour monoBehaviour, IEnumerator coroutine)
		{
			return CoreRoutine.Start(monoBehaviour, coroutine);
		}


		public static CoreRoutine StartCoreRoutine(this MonoBehaviour monoBehaviour, IEnumerator coroutine,
			RoutineCancellationToken cancellationToken)
		{
			return CoreRoutine.Start(monoBehaviour, coroutine, cancellationToken);
		}


		public static CoreRoutine<TResult> StartCoreRoutineResult<TResult>(this MonoBehaviour monoBehaviour,
			IEnumerator coroutine)
		{
			return CoreRoutine<TResult>.StartResult(monoBehaviour, coroutine);
		}


		public static CoreRoutine StartCoreRoutineResult<TResult>(this MonoBehaviour monoBehaviour, IEnumerator coroutine,
			RoutineCancellationToken cancellationToken)
		{
			return CoreRoutine<TResult>.StartResult(monoBehaviour, coroutine, cancellationToken);
		}


		public static CoreRoutine StartCoreRoutineTask(this MonoBehaviour monoBehaviour, Action task)
		{
			return CoreRoutine.Task(monoBehaviour, task);
		}


		public static CoreRoutine StartCoreRoutineTask(this MonoBehaviour monoBehaviour, Action task,
			RoutineCancellationToken cancellationToken)
		{
			return CoreRoutine.Task(monoBehaviour, task, cancellationToken);
		}


		public static CoreRoutine<TResult> StartCoreRoutineTaskResult<TResult>(this MonoBehaviour monoBehaviour,
			Func<TResult> task)
		{
			return CoreRoutine<TResult>.TaskResult(monoBehaviour, task);
		}


		public static CoreRoutine<TResult> StartCoreRoutineTaskResult<TResult>(this MonoBehaviour monoBehaviour,
			Func<TResult> task, RoutineCancellationToken cancellationToken)
		{
			return CoreRoutine<TResult>.TaskResult(monoBehaviour, task, cancellationToken);
		}
	}
}
