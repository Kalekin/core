﻿using System;


namespace Core.Routine
{
	/// <summary>
	/// Specifies the behavior for a routine that is created by using the various <see cref="CoreRoutine"/> ContinueWith methods.
	/// </summary>
    [Flags]
    public enum RoutineContinuationOptions
    {
		/// <summary>
		/// When no continuation options are specified, specifies that default behavior should be used when
		/// executing a continuation. The continuation runs when the antecedent task completes, regardless 
		/// of the antecedent's final <see cref="CoreRoutine.Status"/> property value.
		/// </summary>
        None = 0x00,
		/// <summary>
		/// Specifies that the continuation routine should not be executed if its antecedent was canceled. 
		/// An antecedent is canceled if its <see cref="CoreRoutine.Status"/> property upon completion is 
		/// <see cref="RoutineStatus.Canceled"/>. This option is not valid for multi-routine continuations.
		/// </summary>
        NotOnCanceled = 0x01,
		/// <summary>
		/// Specifies that the continuation routine should not be executed if its antecedent threw an unhandled 
		/// exception. An antecedent is faulted if its <see cref="CoreRoutine.Status"/> property upon completion
		/// is <see cref="RoutineStatus.Faulted"/>. This option is not valid for multi-routine continuations.
		/// </summary>
        NotOnFaulted = 0x02,
		/// <summary>
		/// Specifies that the continuation routine should not be scheduled if its antecedent ran to completion. 
		/// An antecedent runs to completion if its <see cref="CoreRoutine.Status"/> property upon completion is 
		/// <see cref="RoutineStatus.RanToCompletion"/>. This option is not valid for multi-routine continuations.
		/// </summary>
        NotOnRanToCompletion = 0x04,
		/// <summary>
		/// Specifies that the continuation routine should be executed only if its antecedent was canceled. An 
		/// antecedent is canceled if its <see cref="CoreRoutine.Status"/> property upon completion is 
		/// <see cref="RoutineStatus.Canceled"/>. This option is not valid for multi-routine continuations.
		/// </summary>
		OnlyOnCanceled = NotOnFaulted | NotOnRanToCompletion,
		/// <summary>
		/// Specifies that the continuation routine should be executed only if its antecedent was faulted. An 
		/// antecedent is faulted if its <see cref="CoreRoutine.Status"/> property upon completion is 
		/// <see cref="RoutineStatus.Faulted"/>. This option is not valid for multi-routine continuations.
		/// </summary>
		OnlyOnFaulted = NotOnCanceled | NotOnRanToCompletion,
		/// <summary>
		/// Specifies that the continuation routine should be executed only if its antecedent ran to completion. An 
		/// antecedent runs to completion if its <see cref="CoreRoutine.Status"/> property upon completion is 
		/// <see cref="RoutineStatus.RanToCompletion"/>. This option is not valid for multi-routine continuations.
		/// </summary>
        OnlyOnRanToCompletion = NotOnCanceled | NotOnFaulted,
    }
}
