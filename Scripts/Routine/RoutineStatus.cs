﻿namespace Core.Routine
{
	/// <summary>
	/// Represents the current stage in the lifecycle of a <see cref="CoreRoutine"/>.
	/// </summary>
	public enum RoutineStatus
	{
		/// <summary>
		/// The routine acknowledged cancellation by throwing a <see cref="RoutineCanceledException"/> with 
		/// its own <see cref="RoutineCancellationToken"/> while the token was in signaled state, or the 
		/// routine's <see cref="RoutineCancellationToken"/> was already signaled before the routine started 
		/// executing.
		/// </summary>
		Canceled,
		/// <summary>
		/// The routine completed due to an unhandled exception.
		/// </summary>
		Faulted,
		/// <summary>
		/// The routine completed execution successfully.
		/// </summary>
		RanToCompletion,
		/// <summary>
		/// The routine is running but has not yet completed.
		/// </summary>
		Running,
	}
}