﻿using System;


namespace Core.Routine
{
	/// <summary>
	/// Represents an exception used to communicate routine cancellation.
	/// </summary>
    public class RoutineCanceledException : OperationCanceledException
    {
        private RoutineCancellationToken _token;



		/// <summary>
		/// Gets a token associated with the routine that was canceled. 
		/// </summary>
        public RoutineCancellationToken Token
        {
            get
            {
                return _token;
            }
        }



		/// <summary>
		/// Initializes a new instance of the <see cref="RoutineCanceledException"/> class with a specified <see cref="RoutineCancellationToken"/>.
		/// </summary>
		/// <param name="token"></param>
        public RoutineCanceledException(RoutineCancellationToken token)
        {
            _token = token;
        }
    }
}
