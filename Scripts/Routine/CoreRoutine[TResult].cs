﻿using System;
using System.Collections;
using System.Threading;
using Core.Debug;
using UnityEngine;


namespace Core.Routine
{
	/// <summary>
	/// An extension of the Unity <see cref="Coroutine"/> that provides better tools for parallelised work that return results.
	/// </summary>
	/// <typeparam name="TResult">The type of the result produced by this <see cref="CoreRoutine{TResult}"/></typeparam>
	public class CoreRoutine<TResult> : CoreRoutine
	{
		private TResult _result;



		/// <summary>
		/// Gets the result value of this <see cref="CoreRoutine{TResult}"/>. 
		/// </summary>
		/// <exception cref="InvalidOperationException">The routine has not yet completed.</exception>
		/// <exception cref="RoutineCanceledException">The routine was canceled.</exception>
		/// <exception cref="Exception">An exception was thrown during the execution of the routine.</exception>
		public TResult Result
		{
			get
			{
				if (!IsDone)
				{
					throw new InvalidOperationException("Cannot access the result property for the routine has completed.");
				}

				if (Exception != null)
				{
					throw Exception;
				}

				return _result;
			}
			protected set
			{
				_result = value;
			}
		}




		private static CoreRoutine<TResult> Basic(MonoBehaviour monoBehaviour, RoutineCancellationToken cancellationToken)
		{
			var ret = new CoreRoutine<TResult>();

			ret.MonoBehaviour = monoBehaviour;
			ret.Exception = null;
			ret.Status = RoutineStatus.Running;
			ret.CancellationToken = cancellationToken;

			return ret;
		}

		/// <summary>
		/// Starts a <see cref="CoreRoutine"/> that will wrap the provided coroutine and return a result.
		/// </summary>
		/// <param name="monoBehaviour">The <see cref="MonoBehaviour"/> that will host the internal wrapping <see cref="Coroutine"/>.</param>
		/// <param name="coroutine">The <see cref="Coroutine"/> to be wrapped.</param>
		public static CoreRoutine<TResult> StartResult(MonoBehaviour monoBehaviour, IEnumerator coroutine)
		{
			return StartResult(monoBehaviour, coroutine, RoutineCancellationToken.None);
		}

		/// <summary>
		/// Starts a <see cref="CoreRoutine"/> that will wrap the provided coroutine and return a result.
		/// </summary>
		/// <param name="monoBehaviour">The <see cref="MonoBehaviour"/> that will host the internal wrapping <see cref="Coroutine"/>.</param>
		/// <param name="coroutine">The <see cref="Coroutine"/> to be wrapped.</param>
		/// <param name="cancellationToken">A cancellation token that should be used to cancel the work.</param>
		public static CoreRoutine<TResult> StartResult(MonoBehaviour monoBehaviour, IEnumerator coroutine, RoutineCancellationToken cancellationToken)
		{
			var ret = Basic(monoBehaviour, cancellationToken);

			ret.InitializeCoroutine(coroutine);

			return ret;
		}

		/// <summary>
		/// Starts a <see cref="CoreRoutine"/> that will wrap an asynchronously executed task that returns a result.
		/// </summary>
		/// <param name="monoBehaviour">The <see cref="MonoBehaviour"/> that will host the internal wrapping <see cref="Coroutine"/>.</param>
		/// <param name="task">The work to execute asynchronously.</param>
		public static CoreRoutine<TResult> TaskResult(MonoBehaviour monoBehaviour, Func<TResult> task)
		{
			return TaskResult(monoBehaviour, task, RoutineCancellationToken.None);
		}

		/// <summary>
		/// Starts a <see cref="CoreRoutine"/> that will wrap an asynchronously executed task that returns a result.
		/// </summary>
		/// <param name="monoBehaviour">The <see cref="MonoBehaviour"/> that will host the internal wrapping <see cref="Coroutine"/>.</param>
		/// <param name="task">The work to execute asynchronously.</param>
		/// <param name="cancellationToken">A cancellation token that should be used to cancel the work.</param>
		public static CoreRoutine<TResult> TaskResult(MonoBehaviour monoBehaviour, Func<TResult> task, RoutineCancellationToken cancellationToken)
		{
			var ret = Basic(monoBehaviour, cancellationToken);

			ret.InitializeCoroutine(ret.TaskRoutine(task));

			return ret;
		}

		/// <summary>
		/// Creates a routine that will execute and return a result when the <see cref="CoreRoutine"/> object has completed.
		/// </summary>
		/// <param name="monoBehaviour">The <see cref="MonoBehaviour"/> that will host the internal wrapping <see cref="Coroutine"/>.</param>
		/// <param name="routine">The <see cref="CoreRoutine"/> to wait on for completion.</param>
		/// <param name="coroutine">The work coroutine to execute.</param>
		/// <param name="continuationOptions">Options for how the continuation should behave.</param>
		/// <param name="cancellationToken">A cancellation token that should be used to cancel the work.</param>
		public static CoreRoutine<TResult> ContinueWithResult(MonoBehaviour monoBehaviour, CoreRoutine routine, Func<CoreRoutine, IEnumerator> coroutine, RoutineContinuationOptions continuationOptions, RoutineCancellationToken cancellationToken)
		{
			var ret = Basic(monoBehaviour, cancellationToken);

			ret.InitializeCoroutine(ret.ContinueWithRoutine(routine, coroutine, continuationOptions));

			return ret;
		}

		/// <summary>
		/// Creates a routine that will execute and return a result when the <see cref="CoreRoutine"/> object has completed.
		/// </summary>
		/// <typeparam name="TAntecedentResult">The type of the result produced by this <see cref="CoreRoutine{TResult}"/></typeparam>
		/// <param name="monoBehaviour">The <see cref="MonoBehaviour"/> that will host the internal wrapping <see cref="Coroutine"/>.</param>
		/// <param name="routine">The <see cref="CoreRoutine"/> to wait on for completion.</param>
		/// <param name="coroutine">The work coroutine to execute.</param>
		/// <param name="continuationOptions">Options for how the continuation should behave.</param>
		/// <param name="cancellationToken">A cancellation token that should be used to cancel the work.</param>
		public static CoreRoutine<TResult> ContinueWithResult<TAntecedentResult>(MonoBehaviour monoBehaviour, CoreRoutine<TAntecedentResult> routine, Func<CoreRoutine<TAntecedentResult>, IEnumerator> coroutine, RoutineContinuationOptions continuationOptions, RoutineCancellationToken cancellationToken)
		{
			var ret = Basic(monoBehaviour, cancellationToken);

			ret.InitializeCoroutine(ret.ContinueWithRoutine(routine, coroutine, continuationOptions));

			return ret;
		}

		/// <summary>
		/// Creates a callback routine that will execute and return a result when the <see cref="CoreRoutine"/> object has completed.
		/// </summary>
		/// <param name="monoBehaviour">The <see cref="MonoBehaviour"/> that will host the internal wrapping <see cref="Coroutine"/>.</param>
		/// <param name="routine">The <see cref="CoreRoutine"/> to wait on for completion.</param>
		/// <param name="callback">The work callback to execute.</param>
		/// <param name="continuationOptions">Options for how the continuation should behave.</param>
		/// <param name="cancellationToken">A cancellation token that should be used to cancel the work.</param>
		/// <returns></returns>
		public static CoreRoutine<TResult> ContinueWithCallbackResult(MonoBehaviour monoBehaviour, CoreRoutine routine, Func<CoreRoutine, TResult> callback, RoutineContinuationOptions continuationOptions, RoutineCancellationToken cancellationToken)
		{
			var ret = Basic(monoBehaviour, cancellationToken);

			ret.InitializeCoroutine(ret.ContinueWithCallbackResultRoutine(routine, callback, continuationOptions));

			return ret;
		}

		/// <summary>
		/// Creates a callback routine that will execute and return a result when the <see cref="CoreRoutine"/> object has completed.
		/// </summary>
		/// <typeparam name="TAntecedentResult">The type of the result produced by this <see cref="CoreRoutine{TResult}"/></typeparam>
		/// <param name="monoBehaviour">The <see cref="MonoBehaviour"/> that will host the internal wrapping <see cref="Coroutine"/>.</param>
		/// <param name="routine">The <see cref="CoreRoutine"/> to wait on for completion.</param>
		/// <param name="callback">The work callback to execute.</param>
		/// <param name="continuationOptions">Options for how the continuation should behave.</param>
		/// <param name="cancellationToken">A cancellation token that should be used to cancel the work.</param>
		public static CoreRoutine<TResult> ContinueWithCallbackResult<TAntecedentResult>(MonoBehaviour monoBehaviour, CoreRoutine<TAntecedentResult> routine, Func<CoreRoutine<TAntecedentResult>, TResult> callback, RoutineContinuationOptions continuationOptions, RoutineCancellationToken cancellationToken)
		{
			var ret = Basic(monoBehaviour, cancellationToken);

			ret.InitializeCoroutine(ret.ContinueWithCallbackResultRoutine(routine, callback, continuationOptions));

			return ret;
		}

		/// <summary>
		/// Creates a routine that will execute asynchronously and return a result when the <see cref="CoreRoutine"/> object has completed.
		/// </summary>
		/// <param name="monoBehaviour">The <see cref="MonoBehaviour"/> that will host the internal wrapping <see cref="Coroutine"/>.</param>
		/// <param name="routine">The <see cref="CoreRoutine"/> to wait on for completion.</param>
		/// <param name="task">The work to execute asynchronously.</param>
		/// <param name="continuationOptions">Options for how the continuation should behave.</param>
		/// <param name="cancellationToken">A cancellation token that should be used to cancel the work.</param>
		public static CoreRoutine<TResult> ContinueWithTaskResult(MonoBehaviour monoBehaviour, CoreRoutine routine, Func<CoreRoutine, TResult> task, RoutineContinuationOptions continuationOptions, RoutineCancellationToken cancellationToken)
		{
			var ret = Basic(monoBehaviour, cancellationToken);

			ret.InitializeCoroutine(ret.ContinueWithTaskResultRoutine(routine, task, continuationOptions));

			return ret;
		}

		/// <summary>
		/// Creates a routine that will execute asynchronously and return a result when the <see cref="CoreRoutine"/> object has completed.
		/// </summary>
		/// <typeparam name="TAntecedentResult">The type of the result produced by this <see cref="CoreRoutine{TResult}"/></typeparam>
		/// <param name="monoBehaviour">The <see cref="MonoBehaviour"/> that will host the internal wrapping <see cref="Coroutine"/>.</param>
		/// <param name="routine">The <see cref="CoreRoutine"/> to wait on for completion.</param>
		/// <param name="task">The work to execute asynchronously.</param>
		/// <param name="continuationOptions">Options for how the continuation should behave.</param>
		/// <param name="cancellationToken">A cancellation token that should be used to cancel the work.</param>
		public static CoreRoutine<TResult> ContinueWithTaskResult<TAntecedentResult>(MonoBehaviour monoBehaviour, CoreRoutine<TAntecedentResult> routine, Func<CoreRoutine<TAntecedentResult>, TResult> task, RoutineContinuationOptions continuationOptions, RoutineCancellationToken cancellationToken)
		{
			var ret = Basic(monoBehaviour, cancellationToken);

			ret.InitializeCoroutine(ret.ContinueWithTaskResultRoutine(routine, task, continuationOptions));

			return ret;
		}

		/// <summary>
		/// Creates a routine that will execute and return a result when all of the <see cref="CoreRoutine"/> objects in an array have completed.
		/// </summary>
		/// <param name="monoBehaviour">The <see cref="MonoBehaviour"/> that will host the internal wrapping <see cref="Coroutine"/>.</param>
		/// <param name="routines">The routines to wait on for completion.</param>
		/// <param name="coroutine">The work coroutine to execute.</param>
		/// <param name="cancellationToken">A cancellation token that should be used to cancel the work.</param>
		/// <returns></returns>
		public static CoreRoutine<TResult> ContinueResultWhenAll(MonoBehaviour monoBehaviour, CoreRoutine[] routines, Func<CoreRoutine[], IEnumerator> coroutine, RoutineCancellationToken cancellationToken)
		{
			var ret = Basic(monoBehaviour, cancellationToken);

			ret.InitializeCoroutine(ret.ContinueWhenAllRoutine(routines, coroutine));

			return ret;
		}

		/// <summary>
		/// Creates a routine that will execute and return a result when all of the <see cref="CoreRoutine"/> objects in an array have completed.
		/// </summary>
		/// <typeparam name="TAntecedentResult">The type of the result produced by this <see cref="CoreRoutine{TResult}"/></typeparam>
		/// <param name="monoBehaviour">The <see cref="MonoBehaviour"/> that will host the internal wrapping <see cref="Coroutine"/>.</param>
		/// <param name="routines">The routines to wait on for completion.</param>
		/// <param name="coroutine">The work coroutine to execute.</param>
		/// <param name="cancellationToken">A cancellation token that should be used to cancel the work.</param>
		public static CoreRoutine<TResult> ContinueResultWhenAll<TAntecedentResult>(MonoBehaviour monoBehaviour, CoreRoutine<TAntecedentResult>[] routines, Func<CoreRoutine<TAntecedentResult>[], IEnumerator> coroutine, RoutineCancellationToken cancellationToken)
		{
			var ret = Basic(monoBehaviour, cancellationToken);

			ret.InitializeCoroutine(ret.ContinueWhenAllRoutine(routines, coroutine));

			return ret;
		}

		/// <summary>
		/// Creates a callback routine that will execute and return a result when all of the <see cref="CoreRoutine"/> objects in an array have completed.
		/// </summary>
		/// <param name="monoBehaviour">The <see cref="MonoBehaviour"/> that will host the internal wrapping <see cref="Coroutine"/>.</param>
		/// <param name="routines">The routines to wait on for completion.</param>
		/// <param name="callback">The work callback to execute.</param>
		/// <param name="cancellationToken">A cancellation token that should be used to cancel the work.</param>
		public static CoreRoutine<TResult> ContinueCallbackResultWhenAll(MonoBehaviour monoBehaviour, CoreRoutine[] routines, Func<CoreRoutine[], TResult> callback, RoutineCancellationToken cancellationToken)
		{
			var ret = Basic(monoBehaviour, cancellationToken);

			ret.InitializeCoroutine(ret.ContinueCallbackResultWhenAllRoutine(routines, callback));

			return ret;
		}

		/// <summary>
		/// Creates a callback routine that will execute and return a result when all of the <see cref="CoreRoutine"/> objects in an array have completed.
		/// </summary>
		/// <typeparam name="TAntecedentResult">The type of the result produced by this <see cref="CoreRoutine{TResult}"/></typeparam>
		/// <param name="monoBehaviour">The <see cref="MonoBehaviour"/> that will host the internal wrapping <see cref="Coroutine"/>.</param>
		/// <param name="routines">The routines to wait on for completion.</param>
		/// <param name="callback">The work callback to execute.</param>
		/// <param name="cancellationToken">A cancellation token that should be used to cancel the work.</param>
		public static CoreRoutine<TResult> ContinueCallbackResultWhenAll<TAntecedentResult>(MonoBehaviour monoBehaviour, CoreRoutine<TAntecedentResult>[] routines, Func<CoreRoutine<TAntecedentResult>[], TResult> callback, RoutineCancellationToken cancellationToken)
		{
			var ret = Basic(monoBehaviour, cancellationToken);

			ret.InitializeCoroutine(ret.ContinueCallbackResultWhenAllRoutine(routines, callback));

			return ret;
		}

		/// <summary>
		/// Creates a routine that will execute asynchronously and return a result when all of the <see cref="CoreRoutine"/> objects in an array have completed.
		/// </summary>
		/// <param name="monoBehaviour">The <see cref="MonoBehaviour"/> that will host the internal wrapping <see cref="Coroutine"/>.</param>
		/// <param name="routines">The routines to wait on for completion.</param>
		/// <param name="task">The work to execute asynchronously.</param>
		/// <param name="cancellationToken">A cancellation token that should be used to cancel the work.</param>
		public static CoreRoutine<TResult> ContinueTaskResultWhenAll(MonoBehaviour monoBehaviour, CoreRoutine[] routines, Func<CoreRoutine[], TResult> task, RoutineCancellationToken cancellationToken)
		{
			var ret = Basic(monoBehaviour, cancellationToken);

			ret.InitializeCoroutine(ret.ContinueTaskResultWhenAllRoutine(routines, task));

			return ret;
		}

		/// <summary>
		/// Creates a routine that will execute asynchronously and return a result when all of the <see cref="CoreRoutine"/> objects in an array have completed.
		/// </summary>
		/// <typeparam name="TAntecedentResult">The type of the result produced by this <see cref="CoreRoutine{TResult}"/></typeparam>
		/// <param name="monoBehaviour">The <see cref="MonoBehaviour"/> that will host the internal wrapping <see cref="Coroutine"/>.</param>
		/// <param name="routines">The routines to wait on for completion.</param>
		/// <param name="task">The work to execute asynchronously.</param>
		/// <param name="cancellationToken">A cancellation token that should be used to cancel the work.</param>
		/// <returns></returns>
		public static CoreRoutine<TResult> ContinueTaskResultWhenAll<TAntecedentResult>(MonoBehaviour monoBehaviour, CoreRoutine<TAntecedentResult>[] routines, Func<CoreRoutine<TAntecedentResult>[], TResult> task, RoutineCancellationToken cancellationToken)
		{
			var ret = Basic(monoBehaviour, cancellationToken);

			ret.InitializeCoroutine(ret.ContinueTaskResultWhenAllRoutine(routines, task));

			return ret;
		}

		/// <summary>
		/// Creates a routine that will execute and return a result when any of the <see cref="CoreRoutine"/> objects in an array has completed.
		/// </summary>
		/// <param name="monoBehaviour">The <see cref="MonoBehaviour"/> that will host the internal wrapping <see cref="Coroutine"/>.</param>
		/// <param name="routines">The routines to wait on for completion.</param>
		/// <param name="coroutine">The work coroutine to execute.</param>
		/// <param name="cancellationToken">A cancellation token that should be used to cancel the work.</param>
		public static CoreRoutine<TResult> ContinueResultWhenAny(MonoBehaviour monoBehaviour, CoreRoutine[] routines, Func<CoreRoutine, IEnumerator> coroutine, RoutineCancellationToken cancellationToken)
		{
			var ret = Basic(monoBehaviour, cancellationToken);

			ret.InitializeCoroutine(ret.ContinueWhenAnyRoutine(routines, coroutine));

			return ret;
		}

		/// <summary>
		/// Creates a routine that will execute and return a result when any of the <see cref="CoreRoutine"/> objects in an array has completed.
		/// </summary>
		/// <typeparam name="TAntecedentResult">The type of the result produced by this <see cref="CoreRoutine{TResult}"/></typeparam>
		/// <param name="monoBehaviour">The <see cref="MonoBehaviour"/> that will host the internal wrapping <see cref="Coroutine"/>.</param>
		/// <param name="routines">The routines to wait on for completion.</param>
		/// <param name="coroutine">The work coroutine to execute.</param>
		/// <param name="cancellationToken">A cancellation token that should be used to cancel the work.</param>
		public static CoreRoutine<TResult> ContinueResultWhenAny<TAntecedentResult>(MonoBehaviour monoBehaviour, CoreRoutine<TAntecedentResult>[] routines, Func<CoreRoutine<TAntecedentResult>, IEnumerator> coroutine, RoutineCancellationToken cancellationToken)
		{
			var ret = Basic(monoBehaviour, cancellationToken);

			ret.InitializeCoroutine(ret.ContinueWhenAnyRoutine(routines, coroutine));

			return ret;
		}

		/// <summary>
		/// Creates a callback routine that will execute and return a result when any of the <see cref="CoreRoutine"/> objects in an array has completed.
		/// </summary>
		/// <param name="monoBehaviour">The <see cref="MonoBehaviour"/> that will host the internal wrapping <see cref="Coroutine"/>.</param>
		/// <param name="routines">The routines to wait on for completion.</param>
		/// <param name="callback">The work callback to execute.</param>
		/// <param name="cancellationToken">A cancellation token that should be used to cancel the work.</param>
		public static CoreRoutine<TResult> ContinueCallbackResultWhenAny(MonoBehaviour monoBehaviour, CoreRoutine[] routines, Func<CoreRoutine, TResult> callback, RoutineCancellationToken cancellationToken)
		{
			var ret = Basic(monoBehaviour, cancellationToken);

			ret.InitializeCoroutine(ret.ContinueCallbackResultWhenAnyRoutine(routines, callback));

			return ret;
		}

		/// <summary>
		/// Creates a callback routine that will execute and return a result when any of the <see cref="CoreRoutine"/> objects in an array has completed.
		/// </summary>
		/// <typeparam name="TAntecedentResult">The type of the result produced by this <see cref="CoreRoutine{TResult}"/></typeparam>
		/// <param name="monoBehaviour">The <see cref="MonoBehaviour"/> that will host the internal wrapping <see cref="Coroutine"/>.</param>
		/// <param name="routines">The routines to wait on for completion.</param>
		/// <param name="callback">The work callback to execute.</param>
		/// <param name="cancellationToken">A cancellation token that should be used to cancel the work.</param>
		public static CoreRoutine<TResult> ContinueCallbackResultWhenAny<TAntecedentResult>(MonoBehaviour monoBehaviour, CoreRoutine<TAntecedentResult>[] routines, Func<CoreRoutine<TAntecedentResult>, TResult> callback, RoutineCancellationToken cancellationToken)
		{
			var ret = Basic(monoBehaviour, cancellationToken);

			ret.InitializeCoroutine(ret.ContinueCallbackResultWhenAnyRoutine(routines, callback));

			return ret;
		}

		/// <summary>
		/// Creates a routine that will execute asynchronously and return a result when any of the <see cref="CoreRoutine"/> objects in an array has completed.
		/// </summary>
		/// <param name="monoBehaviour">The <see cref="MonoBehaviour"/> that will host the internal wrapping <see cref="Coroutine"/>.</param>
		/// <param name="routines">The routines to wait on for completion.</param>
		/// <param name="task">The work to execute asynchronously.</param>
		/// <param name="cancellationToken">A cancellation token that should be used to cancel the work.</param>
		public static CoreRoutine<TResult> ContinueTaskResultWhenAny(MonoBehaviour monoBehaviour, CoreRoutine[] routines, Func<CoreRoutine, TResult> task, RoutineCancellationToken cancellationToken)
		{
			var ret = Basic(monoBehaviour, cancellationToken);

			ret.InitializeCoroutine(ret.ContinueTaskResultWhenAnyRoutine(routines, task));

			return ret;
		}

		/// <summary>
		/// Creates a routine that will execute asynchronously and return a result when any of the <see cref="CoreRoutine"/> objects in an array has completed.
		/// </summary>
		/// <typeparam name="TAntecedentResult">The type of the result produced by this <see cref="CoreRoutine{TResult}"/></typeparam>
		/// <param name="monoBehaviour">The <see cref="MonoBehaviour"/> that will host the internal wrapping <see cref="Coroutine"/>.</param>
		/// <param name="routines">The routines to wait on for completion.</param>
		/// <param name="task">The work to execute asynchronously.</param>
		/// <param name="cancellationToken">A cancellation token that should be used to cancel the work.</param>
		public static CoreRoutine<TResult> ContinueTaskResultWhenAny<TAntecedentResult>(MonoBehaviour monoBehaviour, CoreRoutine<TAntecedentResult>[] routines, Func<CoreRoutine<TAntecedentResult>, TResult> task, RoutineCancellationToken cancellationToken)
		{
			var ret = Basic(monoBehaviour, cancellationToken);

			ret.InitializeCoroutine(ret.ContinueTaskResultWhenAnyRoutine(routines, task));

			return ret;
		}



		/// <summary>
		/// Creates a continuation that executes as a coroutine when the target <see cref="CoreRoutine{TResult}"/> completes.
		/// </summary>
		/// <param name="coroutine">The work coroutine to execute.</param>
		public CoreRoutine ContinueWith(Func<CoreRoutine<TResult>, IEnumerator> coroutine)
		{
			return ContinueWith(MonoBehaviour, this, coroutine, RoutineContinuationOptions.None, RoutineCancellationToken.None);
		}

		/// <summary>
		/// Creates a continuation that executes as a coroutine when the target <see cref="CoreRoutine{TResult}"/> completes.
		/// </summary>
		/// <param name="coroutine">The work coroutine to execute.</param>
		/// <param name="cancellationToken">A cancellation token that should be used to cancel the work.</param>
		public CoreRoutine ContinueWith(Func<CoreRoutine<TResult>, IEnumerator> coroutine, RoutineCancellationToken cancellationToken)
		{
			return ContinueWith(MonoBehaviour, this, coroutine, RoutineContinuationOptions.None, cancellationToken);
		}

		/// <summary>
		/// Creates a continuation that executes as a coroutine when the target <see cref="CoreRoutine{TResult}"/> completes.
		/// </summary>
		/// <param name="coroutine">The work coroutine to execute.</param>
		/// <param name="continuationOptions">Options for how the continuation should behave.</param>
		public CoreRoutine ContinueWith(Func<CoreRoutine<TResult>, IEnumerator> coroutine, RoutineContinuationOptions continuationOptions)
		{
			return ContinueWith(MonoBehaviour, this, coroutine, continuationOptions, RoutineCancellationToken.None);
		}

		/// <summary>
		/// Creates a continuation that executes as a coroutine when the target <see cref="CoreRoutine{TResult}"/> completes.
		/// </summary>
		/// <param name="coroutine">The work coroutine to execute.</param>
		/// <param name="continuationOptions">Options for how the continuation should behave.</param>
		/// <param name="cancellationToken">A cancellation token that should be used to cancel the work.</param>
		public CoreRoutine ContinueWith(Func<CoreRoutine<TResult>, IEnumerator> coroutine, RoutineContinuationOptions continuationOptions, RoutineCancellationToken cancellationToken)
		{
			return ContinueWith(MonoBehaviour, this, coroutine, continuationOptions, cancellationToken);
		}

		/// <summary>
		/// Creates a continuation that executes as a callback when the target <see cref="CoreRoutine{TResult}"/> completes.
		/// </summary>
		/// <param name="callback">The work callback to execute.</param>
		public CoreRoutine ContinueWithCallback(Action<CoreRoutine<TResult>> callback)
		{
			return ContinueWithCallback(MonoBehaviour, this, callback, RoutineContinuationOptions.None, RoutineCancellationToken.None);
		}

		/// <summary>
		/// Creates a continuation that executes as a callback when the target <see cref="CoreRoutine{TResult}"/> completes.
		/// </summary>
		/// <param name="callback">The work callback to execute.</param>
		/// <param name="cancellationToken">A cancellation token that should be used to cancel the work.</param>
		public CoreRoutine ContinueWithCallback(Action<CoreRoutine<TResult>> callback, RoutineCancellationToken cancellationToken)
		{
			return ContinueWithCallback(MonoBehaviour, this, callback, RoutineContinuationOptions.None, cancellationToken);
		}

		/// <summary>
		/// Creates a continuation that executes as a callback when the target <see cref="CoreRoutine{TResult}"/> completes.
		/// </summary>
		/// <param name="callback">The work callback to execute.</param>
		/// <param name="continuationOptions">Options for how the continuation should behave.</param>
		public CoreRoutine ContinueWithCallback(Action<CoreRoutine<TResult>> callback, RoutineContinuationOptions continuationOptions)
		{
			return ContinueWithCallback(MonoBehaviour, this, callback, continuationOptions, RoutineCancellationToken.None);
		}

		/// <summary>
		/// Creates a continuation that executes as a callback when the target <see cref="CoreRoutine{TResult}"/> completes.
		/// </summary>
		/// <param name="callback">The work callback to execute.</param>
		/// <param name="continuationOptions">Options for how the continuation should behave.</param>
		/// <param name="cancellationToken">A cancellation token that should be used to cancel the work.</param>
		public CoreRoutine ContinueWithCallback(Action<CoreRoutine<TResult>> callback, RoutineContinuationOptions continuationOptions, RoutineCancellationToken cancellationToken)
		{
			return ContinueWithCallback(MonoBehaviour, this, callback, continuationOptions, cancellationToken);
		}

		/// <summary>
		/// Creates a continuation that executes asynchronously when the target <see cref="CoreRoutine{TResult}"/> completes.
		/// </summary>
		/// <param name="task">The work to execute asynchronously.</param>
		public CoreRoutine ContinueWithTask(Action<CoreRoutine<TResult>> task)
		{
			return ContinueWithTask(MonoBehaviour, this, task, RoutineContinuationOptions.None, RoutineCancellationToken.None);
		}

		/// <summary>
		/// Creates a continuation that executes asynchronously when the target <see cref="CoreRoutine{TResult}"/> completes.
		/// </summary>
		/// <param name="task">The work to execute asynchronously.</param>
		/// <param name="cancellationToken">A cancellation token that should be used to cancel the work.</param>
		public CoreRoutine ContinueWithTask(Action<CoreRoutine<TResult>> task, RoutineCancellationToken cancellationToken)
		{
			return ContinueWithTask(MonoBehaviour, this, task, RoutineContinuationOptions.None, cancellationToken);
		}

		/// <summary>
		/// Creates a continuation that executes asynchronously when the target <see cref="CoreRoutine{TResult}"/> completes.
		/// </summary>
		/// <param name="task">The work to execute asynchronously.</param>
		/// <param name="continuationOptions">Options for how the continuation should behave.</param>
		public CoreRoutine ContinueWithTask(Action<CoreRoutine<TResult>> task, RoutineContinuationOptions continuationOptions)
		{
			return ContinueWithTask(MonoBehaviour, this, task, continuationOptions, RoutineCancellationToken.None);
		}

		/// <summary>
		/// Creates a continuation that executes asynchronously when the target <see cref="CoreRoutine{TResult}"/> completes.
		/// </summary>
		/// <param name="task">The work to execute asynchronously.</param>
		/// <param name="continuationOptions">Options for how the continuation should behave.</param>
		/// <param name="cancellationToken">A cancellation token that should be used to cancel the work.</param>
		public CoreRoutine ContinueWithTask(Action<CoreRoutine<TResult>> task, RoutineContinuationOptions continuationOptions, RoutineCancellationToken cancellationToken)
		{
			return ContinueWithTask(MonoBehaviour, this, task, continuationOptions, cancellationToken);
		}

		/// <summary>
		/// Creates a continuation that executes as a coroutine and returns a result when the target <see cref="CoreRoutine{TResult}"/> completes.
		/// </summary>
		/// <typeparam name="TNewResult">The type of the result produced by the continuation.</typeparam>
		/// <param name="coroutine">The work coroutine to execute.</param>
		public CoreRoutine<TNewResult> ContinueWithResult<TNewResult>(Func<CoreRoutine<TResult>, IEnumerator> coroutine)
		{
			return CoreRoutine<TNewResult>.ContinueWithResult(MonoBehaviour, this, coroutine, RoutineContinuationOptions.None, RoutineCancellationToken.None);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <typeparam name="TNewResult">The type of the result produced by the continuation.</typeparam>
		/// <param name="coroutine">The work coroutine to execute.</param>
		/// <param name="cancellationToken">A cancellation token that should be used to cancel the work.</param>
		public CoreRoutine<TNewResult> ContinueWithResult<TNewResult>(Func<CoreRoutine<TResult>, IEnumerator> coroutine, RoutineCancellationToken cancellationToken)
		{
			return CoreRoutine<TNewResult>.ContinueWithResult(MonoBehaviour, this, coroutine, RoutineContinuationOptions.None, cancellationToken);
		}

		/// <summary>
		/// Creates a continuation that executes as a coroutine and returns a result when the target <see cref="CoreRoutine{TResult}"/> completes.
		/// </summary>
		/// <typeparam name="TNewResult">The type of the result produced by the continuation.</typeparam>
		/// <param name="coroutine">The work coroutine to execute.</param>
		/// <param name="continuationOptions">Options for how the continuation should behave.</param>
		public CoreRoutine<TNewResult> ContinueWithResult<TNewResult>(Func<CoreRoutine<TResult>, IEnumerator> coroutine, RoutineContinuationOptions continuationOptions)
		{
			return CoreRoutine<TNewResult>.ContinueWithResult(MonoBehaviour, this, coroutine, continuationOptions, RoutineCancellationToken.None);
		}

		/// <summary>
		/// Creates a continuation that executes as a coroutine and returns a result when the target <see cref="CoreRoutine{TResult}"/> completes.
		/// </summary>
		/// <typeparam name="TNewResult">The type of the result produced by the continuation.</typeparam>
		/// <param name="coroutine">The work coroutine to execute.</param>
		/// <param name="continuationOptions">Options for how the continuation should behave.</param>
		/// <param name="cancellationToken">A cancellation token that should be used to cancel the work.</param>
		public CoreRoutine<TNewResult> ContinueWithResult<TNewResult>(Func<CoreRoutine<TResult>, IEnumerator> coroutine, RoutineContinuationOptions continuationOptions, RoutineCancellationToken cancellationToken)
		{
			return CoreRoutine<TNewResult>.ContinueWithResult(MonoBehaviour, this, coroutine, continuationOptions, cancellationToken);
		}

		/// <summary>
		/// Creates a continuation that executes as a callback and returns a result when the target <see cref="CoreRoutine{TResult}"/> completes.
		/// </summary>
		/// <typeparam name="TNewResult">The type of the result produced by the continuation.</typeparam>
		/// <param name="callback">The work callback to execute.</param>
		public CoreRoutine<TNewResult> ContinueWithCallbackResult<TNewResult>(Func<CoreRoutine<TResult>, TNewResult> callback)
		{
			return CoreRoutine<TNewResult>.ContinueWithCallbackResult(MonoBehaviour, this, callback, RoutineContinuationOptions.None, RoutineCancellationToken.None);
		}

		/// <summary>
		/// Creates a continuation that executes as a callback and returns a result when the target <see cref="CoreRoutine{TResult}"/> completes.
		/// </summary>
		/// <typeparam name="TNewResult">The type of the result produced by the continuation.</typeparam>
		/// <param name="callback">The work callback to execute.</param>
		/// <param name="cancellationToken">A cancellation token that should be used to cancel the work.</param>
		public CoreRoutine<TNewResult> ContinueWithCallbackResult<TNewResult>(Func<CoreRoutine<TResult>, TNewResult> callback, RoutineCancellationToken cancellationToken)
		{
			return CoreRoutine<TNewResult>.ContinueWithCallbackResult(MonoBehaviour, this, callback, RoutineContinuationOptions.None, cancellationToken);
		}

		/// <summary>
		/// Creates a continuation that executes as a callback and returns a result when the target <see cref="CoreRoutine{TResult}"/> completes.
		/// </summary>
		/// <typeparam name="TNewResult">The type of the result produced by the continuation.</typeparam>
		/// <param name="callback">The work callback to execute.</param>
		/// <param name="continuationOptions">Options for how the continuation should behave.</param>
		public CoreRoutine<TNewResult> ContinueWithCallbackResult<TNewResult>(Func<CoreRoutine<TResult>, TNewResult> callback, RoutineContinuationOptions continuationOptions)
		{
			return CoreRoutine<TNewResult>.ContinueWithCallbackResult(MonoBehaviour, this, callback, continuationOptions, RoutineCancellationToken.None);
		}

		/// <summary>
		/// Creates a continuation that executes as a callback and returns a result when the target <see cref="CoreRoutine{TResult}"/> completes.
		/// </summary>
		/// <typeparam name="TNewResult">The type of the result produced by the continuation.</typeparam>
		/// <param name="callback">The work callback to execute.</param>
		/// <param name="continuationOptions">Options for how the continuation should behave.</param>
		/// <param name="cancellationToken">A cancellation token that should be used to cancel the work.</param>
		public CoreRoutine<TNewResult> ContinueWithCallbackResult<TNewResult>(Func<CoreRoutine<TResult>, TNewResult> callback, RoutineContinuationOptions continuationOptions, RoutineCancellationToken cancellationToken)
		{
			return CoreRoutine<TNewResult>.ContinueWithCallbackResult(MonoBehaviour, this, callback, continuationOptions, cancellationToken);
		}

		/// <summary>
		/// Creates a continuation that executes asynchronously and returns a result when the target <see cref="CoreRoutine{TResult}"/> completes.
		/// </summary>
		/// <typeparam name="TNewResult">The type of the result produced by the continuation.</typeparam>
		/// <param name="task">The work to execute asynchronously.</param>
		public CoreRoutine<TNewResult> ContinueWithTaskResult<TNewResult>(Func<CoreRoutine<TResult>, TNewResult> task)
		{
			return CoreRoutine<TNewResult>.ContinueWithTaskResult(MonoBehaviour, this, task, RoutineContinuationOptions.None, RoutineCancellationToken.None);
		}

		/// <summary>
		/// Creates a continuation that executes asynchronously and returns a result when the target <see cref="CoreRoutine{TResult}"/> completes.
		/// </summary>
		/// <typeparam name="TNewResult">The type of the result produced by the continuation.</typeparam>
		/// <param name="task">The work to execute asynchronously.</param>
		/// <param name="cancellationToken">A cancellation token that should be used to cancel the work.</param>
		public CoreRoutine<TNewResult> ContinueWithTaskResult<TNewResult>(Func<CoreRoutine<TResult>, TNewResult> task, RoutineCancellationToken cancellationToken)
		{
			return CoreRoutine<TNewResult>.ContinueWithTaskResult(MonoBehaviour, this, task, RoutineContinuationOptions.None, cancellationToken);
		}

		/// <summary>
		/// Creates a continuation that executes asynchronously and returns a result when the target <see cref="CoreRoutine{TResult}"/> completes.
		/// </summary>
		/// <typeparam name="TNewResult">The type of the result produced by the continuation.</typeparam>
		/// <param name="task">The work to execute asynchronously.</param>
		/// <param name="continuationOptions">Options for how the continuation should behave.</param>
		public CoreRoutine<TNewResult> ContinueWithTaskResult<TNewResult>(Func<CoreRoutine<TResult>, TNewResult> task, RoutineContinuationOptions continuationOptions)
		{
			return CoreRoutine<TNewResult>.ContinueWithTaskResult(MonoBehaviour, this, task, continuationOptions, RoutineCancellationToken.None);
		}

		/// <summary>
		/// Creates a continuation that executes asynchronously and returns a result when the target <see cref="CoreRoutine{TResult}"/> completes.
		/// </summary>
		/// <typeparam name="TNewResult">The type of the result produced by the continuation.</typeparam>
		/// <param name="task">The work to execute asynchronously.</param>
		/// <param name="continuationOptions">Options for how the continuation should behave.</param>
		/// <param name="cancellationToken">A cancellation token that should be used to cancel the work.</param>
		public CoreRoutine<TNewResult> ContinueWithTaskResult<TNewResult>(Func<CoreRoutine<TResult>, TNewResult> task, RoutineContinuationOptions continuationOptions, RoutineCancellationToken cancellationToken)
		{
			return CoreRoutine<TNewResult>.ContinueWithTaskResult(MonoBehaviour, this, task, continuationOptions, cancellationToken);
		}




		private IEnumerator TaskRoutine(Func<TResult> func)
		{
			Exception exception = null;
			TResult value = default(TResult);

			var thread = new Thread(() =>
			{
				try
				{
					value = func();
				}
				catch (Exception e)
				{
					exception = e;
				}
			});

			thread.Start();

			while (true)
			{
				if (thread.IsAlive)
				{
					yield return null;
					continue;
				}

				if (exception != null)
				{
					throw exception;
				}

				yield return value;
			}
		}


		protected override IEnumerator BasicRoutine(IEnumerator coroutine)
		{
			while (true)
			{
				try
				{
					if (!coroutine.MoveNext())
					{
						Status = RoutineStatus.RanToCompletion;
						yield break;
					}
				}
				catch (RoutineCanceledException e)
				{
					Exception = e;

					if (e.Token == CancellationToken)
					{
						Status = RoutineStatus.Canceled;
					}
					else
					{
						Status = RoutineStatus.Faulted;
					}

					yield break;
				}
				catch (Exception e)
				{
					Debug.Logger.LogException(e, MonoBehaviour);
					Status = RoutineStatus.Faulted;
					Exception = e;
				}

				object yieldObject = coroutine.Current;

				if (yieldObject is TResult)
				{
					Status = RoutineStatus.RanToCompletion;
					Result = (TResult)yieldObject;

					yield break;
				}

				yield return coroutine.Current;
			}
		}


		protected IEnumerator ContinueCallbackResultWhenAllRoutine(CoreRoutine[] routines, Func<CoreRoutine[], TResult> callback)
		{
			CancellationToken.ThrowIfCancellationRequested();

			while (true)
			{
				for (int i = 0; i < routines.Length; i++)
				{
					if (!routines[i].IsDone)
					{
						goto NotAllRoutinesDone;
					}
				}

				break;

			NotAllRoutinesDone:
				yield return null;
				CancellationToken.ThrowIfCancellationRequested();
			}

			callback(routines);
		}


		protected IEnumerator ContinueCallbackResultWhenAllRoutine<TAntecedentResult>(CoreRoutine<TAntecedentResult>[] routines, Func<CoreRoutine<TAntecedentResult>[], TResult> callback)
		{
			CancellationToken.ThrowIfCancellationRequested();

			while (true)
			{
				for (int i = 0; i < routines.Length; i++)
				{
					if (!routines[i].IsDone)
					{
						goto NotAllRoutinesDone;
					}
				}

				break;

			NotAllRoutinesDone:
				yield return null;
				CancellationToken.ThrowIfCancellationRequested();
			}

			callback(routines);
		}


		protected IEnumerator ContinueTaskResultWhenAllRoutine(CoreRoutine[] routines, Func<CoreRoutine[], TResult> task)
		{
			CancellationToken.ThrowIfCancellationRequested();

			while (true)
			{
				for (int i = 0; i < routines.Length; i++)
				{
					if (!routines[i].IsDone)
					{
						goto NotAllRoutinesDone;
					}
				}

				break;

			NotAllRoutinesDone:
				yield return null;
				CancellationToken.ThrowIfCancellationRequested();
			}

			IEnumerator c = TaskRoutine(() => task(routines));

			while (c.MoveNext())
			{
				yield return c.Current;
			}
		}


		protected IEnumerator ContinueTaskResultWhenAllRoutine<TAntecedentResult>(CoreRoutine<TAntecedentResult>[] routines, Func<CoreRoutine<TAntecedentResult>[], TResult> task)
		{
			CancellationToken.ThrowIfCancellationRequested();

			while (true)
			{
				for (int i = 0; i < routines.Length; i++)
				{
					if (!routines[i].IsDone)
					{
						goto NotAllRoutinesDone;
					}
				}

				break;

			NotAllRoutinesDone:
				yield return null;
				CancellationToken.ThrowIfCancellationRequested();
			}

			IEnumerator c = TaskRoutine(() => task(routines));

			while (c.MoveNext())
			{
				yield return c.Current;
			}
		}


		protected IEnumerator ContinueCallbackResultWhenAnyRoutine(CoreRoutine[] routines, Func<CoreRoutine, TResult> callback)
		{
			CancellationToken.ThrowIfCancellationRequested();

			CoreRoutine doneRoutine;

			while (true)
			{
				for (int i = 0; i < routines.Length; i++)
				{
					if (routines[i].IsDone)
					{
						doneRoutine = routines[i];
						goto AnyRoutineDone;
					}
				}

				yield return null;
				CancellationToken.ThrowIfCancellationRequested();
			}

		AnyRoutineDone:
			callback(doneRoutine);
		}


		protected IEnumerator ContinueCallbackResultWhenAnyRoutine<TAntecedentResult>(CoreRoutine<TAntecedentResult>[] routines, Func<CoreRoutine<TAntecedentResult>, TResult> callback)
		{
			CancellationToken.ThrowIfCancellationRequested();

			CoreRoutine<TAntecedentResult> doneRoutine;

			while (true)
			{
				for (int i = 0; i < routines.Length; i++)
				{
					if (routines[i].IsDone)
					{
						doneRoutine = routines[i];
						goto AnyRoutineDone;
					}
				}

				yield return null;
				CancellationToken.ThrowIfCancellationRequested();
			}

		AnyRoutineDone:
			callback(doneRoutine);
		}


		protected IEnumerator ContinueTaskResultWhenAnyRoutine(CoreRoutine[] routines, Func<CoreRoutine, TResult> task)
		{
			CancellationToken.ThrowIfCancellationRequested();

			CoreRoutine doneRoutine;

			while (true)
			{
				for (int i = 0; i < routines.Length; i++)
				{
					if (routines[i].IsDone)
					{
						doneRoutine = routines[i];
						goto AnyRoutineDone;
					}
				}

				yield return null;
				CancellationToken.ThrowIfCancellationRequested();
			}

		AnyRoutineDone: 
			IEnumerator c = TaskRoutine(() => task(doneRoutine));

			while (c.MoveNext())
			{
				yield return c.Current;
			}
		}


		protected IEnumerator ContinueTaskResultWhenAnyRoutine<TAntecedentResult>(CoreRoutine<TAntecedentResult>[] routines, Func<CoreRoutine<TAntecedentResult>, TResult> task)
		{
			CancellationToken.ThrowIfCancellationRequested();

			CoreRoutine<TAntecedentResult> doneRoutine;

			while (true)
			{
				for (int i = 0; i < routines.Length; i++)
				{
					if (routines[i].IsDone)
					{
						doneRoutine = routines[i];
						goto AnyRoutineDone;
					}
				}

				yield return null;
				CancellationToken.ThrowIfCancellationRequested();
			}

		AnyRoutineDone: 
			IEnumerator c = TaskRoutine(() => task(doneRoutine));

			while (c.MoveNext())
			{
				yield return c.Current;
			}
		}


		protected IEnumerator ContinueWithCallbackResultRoutine(CoreRoutine routine, Func<CoreRoutine, TResult> callback, RoutineContinuationOptions continuationOptions)
		{
			CancellationToken.ThrowIfCancellationRequested();

			while (!routine.IsDone)
			{
				yield return null;
				CancellationToken.ThrowIfCancellationRequested();
			}

			switch (routine.Status)
			{
				case RoutineStatus.Canceled:
					if ((continuationOptions & RoutineContinuationOptions.NotOnCanceled) ==
						RoutineContinuationOptions.NotOnCanceled)
					{
						yield break;
					}
					break;
				case RoutineStatus.Faulted:
					if ((continuationOptions & RoutineContinuationOptions.NotOnFaulted) ==
						RoutineContinuationOptions.NotOnFaulted)
					{
						yield break;
					}
					break;
				case RoutineStatus.RanToCompletion:
					if ((continuationOptions & RoutineContinuationOptions.NotOnRanToCompletion) ==
						RoutineContinuationOptions.NotOnRanToCompletion)
					{
						yield break;
					}
					break;
			}

			yield return callback(routine);
		}


		protected IEnumerator ContinueWithCallbackResultRoutine<TAntecedentResult>(CoreRoutine<TAntecedentResult> routine, Func<CoreRoutine<TAntecedentResult>, TResult> callback, RoutineContinuationOptions continuationOptions)
		{
			CancellationToken.ThrowIfCancellationRequested();

			while (!routine.IsDone)
			{
				yield return null;
				CancellationToken.ThrowIfCancellationRequested();
			}

			switch (routine.Status)
			{
				case RoutineStatus.Canceled:
					if ((continuationOptions & RoutineContinuationOptions.NotOnCanceled) ==
						RoutineContinuationOptions.NotOnCanceled)
					{
						yield break;
					}
					break;
				case RoutineStatus.Faulted:
					if ((continuationOptions & RoutineContinuationOptions.NotOnFaulted) ==
						RoutineContinuationOptions.NotOnFaulted)
					{
						yield break;
					}
					break;
				case RoutineStatus.RanToCompletion:
					if ((continuationOptions & RoutineContinuationOptions.NotOnRanToCompletion) ==
						RoutineContinuationOptions.NotOnRanToCompletion)
					{
						yield break;
					}
					break;
			}

			yield return callback(routine);
		}


		protected IEnumerator ContinueWithTaskResultRoutine(CoreRoutine routine, Func<CoreRoutine, TResult> task, RoutineContinuationOptions continuationOptions)
		{
			CancellationToken.ThrowIfCancellationRequested();

			while (!routine.IsDone)
			{
				yield return null;
				CancellationToken.ThrowIfCancellationRequested();
			}

			switch (routine.Status)
			{
				case RoutineStatus.Canceled:
					if ((continuationOptions & RoutineContinuationOptions.NotOnCanceled) ==
						RoutineContinuationOptions.NotOnCanceled)
					{
						yield break;
					}
					break;
				case RoutineStatus.Faulted:
					if ((continuationOptions & RoutineContinuationOptions.NotOnFaulted) ==
						RoutineContinuationOptions.NotOnFaulted)
					{
						yield break;
					}
					break;
				case RoutineStatus.RanToCompletion:
					if ((continuationOptions & RoutineContinuationOptions.NotOnRanToCompletion) ==
						RoutineContinuationOptions.NotOnRanToCompletion)
					{
						yield break;
					}
					break;
			}

			IEnumerator c = TaskRoutine(() => task(routine));

			while (c.MoveNext())
			{
				yield return c.Current;
			}
		}


		protected IEnumerator ContinueWithTaskResultRoutine<TAntecedentResult>(CoreRoutine<TAntecedentResult> routine, Func<CoreRoutine<TAntecedentResult>, TResult> task, RoutineContinuationOptions continuationOptions)
		{
			CancellationToken.ThrowIfCancellationRequested();

			while (!routine.IsDone)
			{
				yield return null;
				CancellationToken.ThrowIfCancellationRequested();
			}

			switch (routine.Status)
			{
				case RoutineStatus.Canceled:
					if ((continuationOptions & RoutineContinuationOptions.NotOnCanceled) ==
						RoutineContinuationOptions.NotOnCanceled)
					{
						yield break;
					}
					break;
				case RoutineStatus.Faulted:
					if ((continuationOptions & RoutineContinuationOptions.NotOnFaulted) ==
						RoutineContinuationOptions.NotOnFaulted)
					{
						yield break;
					}
					break;
				case RoutineStatus.RanToCompletion:
					if ((continuationOptions & RoutineContinuationOptions.NotOnRanToCompletion) ==
						RoutineContinuationOptions.NotOnRanToCompletion)
					{
						yield break;
					}
					break;
			}

			IEnumerator c = TaskRoutine(() => task(routine));

			while (c.MoveNext())
			{
				yield return c.Current;
			}
		}
	}
}
