using System;
using System.Collections;
using Core.Debug;
using UnityEngine;


namespace Core.Routine
{
	/// <summary>
	/// A utility class for encapsulating a basic <see cref="CoreRoutine"/> with boilerplate cancelation behaviour.
	/// </summary>
	public class SimpleRoutine
	{
		protected CoreRoutine routine;
		protected RoutineCancellationSource cancellationSource;



		/// <summary>
		/// The <see cref="CoreRoutine"/> wrapped by this <see cref="SimpleRoutine"/>.
		/// </summary>
		public CoreRoutine Routine
		{
			get
			{
				return routine;
			}
		}

		/// <summary>
		/// The coroutine encapsulated by the wrapped <see cref="CoreRoutine"/>.
		/// </summary>
		public Coroutine Coroutine
		{
			get
			{
				return Routine.Coroutine;
			}
		}

		/// <summary>
		/// True if the wrapped <see cref="CoreRoutine"/> has completed, otherwise false.
		/// </summary>
		public bool IsDone
		{
			get
			{
				return Routine.IsDone;
			}
		}

		/// <summary>
		/// True if the wrapped <see cref="CoreRoutine"/> has completed without fault or cancellation, otherwise false.
		/// </summary>
		public bool IsSuccessful
		{
			get
			{
				return Routine.Status == RoutineStatus.RanToCompletion;
			}
		}

		/// <summary>
		/// The token being managed by this <see cref="SimpleRoutine"/>.
		/// </summary>
		public RoutineCancellationToken Token
		{
			get
			{
				return cancellationSource.Token;
			}
		}



		/// <summary>
		/// Starts a <see cref="SimpleRoutine"/> that encapsulates a <see cref="CoreRoutine"/> and a 
		/// <see cref="RoutineCancellationSource"/> for cancelation. The token will be monitored internally
		/// for cancelation.
		/// </summary>
		/// <param name="simpleRoutine">The <see cref="SimpleRoutine"/> to store the created routine. If the routine is not null and is running it will be canceled.</param>
		/// <param name="monoBehaviour">The <see cref="MonoBehaviour"/> that will host the internal wrapping <see cref="Coroutine"/>.</param>
		/// <param name="coroutine">The <see cref="Coroutine"/> to be wrapped.</param>
		public static SimpleRoutine Start(ref SimpleRoutine simpleRoutine, MonoBehaviour monoBehaviour, Func<IEnumerator> coroutine)
		{
			Assert.IsNotNull(monoBehaviour);
			Assert.IsNotNull(coroutine);

			if (simpleRoutine != null)
			{
				simpleRoutine.Cancel();
			}

			simpleRoutine = new SimpleRoutine();

			simpleRoutine.cancellationSource = new RoutineCancellationSource();
			simpleRoutine.routine = CoreRoutine.Start(monoBehaviour, CancelWrapper(coroutine, simpleRoutine.cancellationSource.Token), simpleRoutine.cancellationSource.Token);

			return simpleRoutine;
		}

		/// <summary>
		/// Starts a <see cref="SimpleRoutine"/> that encapsulates a <see cref="CoreRoutine"/> and a 
		/// <see cref="RoutineCancellationSource"/> for cancelation. The token will be passed to the
		/// coroutine with the caller responsible for handling cancelation.
		/// </summary>
		/// <param name="simpleRoutine">The <see cref="SimpleRoutine"/> to store the created routine. If the routine is not null and is running it will be canceled.</param>
		/// <param name="monoBehaviour">The <see cref="MonoBehaviour"/> that will host the internal wrapping <see cref="Coroutine"/>.</param>
		/// <param name="coroutine">The <see cref="Coroutine"/> to be wrapped.</param>
		public static SimpleRoutine Start(ref SimpleRoutine simpleRoutine, MonoBehaviour monoBehaviour, Func<RoutineCancellationToken, IEnumerator> coroutine)
		{
			Assert.IsNotNull(monoBehaviour);
			Assert.IsNotNull(coroutine);

			if (simpleRoutine != null)
			{
				simpleRoutine.Cancel();
			}

			simpleRoutine = new SimpleRoutine();

			simpleRoutine.cancellationSource = new RoutineCancellationSource();
			simpleRoutine.routine = CoreRoutine.Start(monoBehaviour, coroutine(simpleRoutine.cancellationSource.Token), simpleRoutine.cancellationSource.Token);

			return simpleRoutine;
		}

		/// <summary>
		/// If the <see cref="SimpleRoutine"/> is not null it will be canceled, othewise this method does nothing.
		/// </summary>
		/// <param name="simpleRoutine">The <see cref="SimpleRoutine"/> to cancel.</param>
		public static void Cancel(SimpleRoutine simpleRoutine)
		{
			if (simpleRoutine != null &&
			    !simpleRoutine.IsDone)
			{
				simpleRoutine.Cancel();
			}
		}



		/// <summary>
		/// Cancels the <see cref="SimpleRoutine"/>.
		/// </summary>
		public void Cancel()
		{
			cancellationSource.Cancel();
		}


		protected static IEnumerator CancelWrapper(Func<IEnumerator> coroutine, RoutineCancellationToken token)
		{
			token.ThrowIfCancellationRequested();
			IEnumerator coroutineEnumerator = coroutine();

			while (coroutineEnumerator.MoveNext())
			{
				yield return coroutineEnumerator.Current;
				token.ThrowIfCancellationRequested();
			}
		}
	}
}