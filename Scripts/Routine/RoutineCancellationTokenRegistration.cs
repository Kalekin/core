﻿using System;

namespace Core.Routine
{
	/// <summary>
	/// Represents a callback delegate that has been registered with a <see cref="RoutineCancellationToken"/>.
	/// </summary>
    public struct RoutineCancellationTokenRegistration : IDisposable
    {
        private RoutineCancellationSource _source;
        private Action _callback;




        internal RoutineCancellationTokenRegistration(RoutineCancellationSource source, Action callback)
        {
            _source = source;
            _callback = callback;
            _source.Callbacks += callback;
        }



		/// <summary>
		/// Releases all resources used by the current instance of the CancellationTokenRegistration class.
		/// </summary>
        public void Dispose()
		{
			_source.Callbacks -= _callback;
			_source = null;
			_callback = null;
        }
    }
}