﻿using System;
using System.Collections;
using System.Threading;
using Core.Debug;
using Core.Time;
using UnityEngine;


namespace Core.Routine
{
	/// <summary>
	/// An extension of the Unity <see cref="Coroutine"/> that provides better tools for parallelised work.
	/// </summary>
    public class CoreRoutine
    {
        private MonoBehaviour _monoBehaviour;
        private RoutineCancellationToken _cancellationToken;
		private Coroutine _coroutine;
		private Exception _exception;
		private RoutineStatus _status;
        


        /// <summary>
		/// The host <see cref="MonoBehaviour"/> the wrapping <see cref="Coroutine"/> runs on.
        /// </summary>
        public MonoBehaviour MonoBehaviour
        {
            get
            {
                return _monoBehaviour;
            }
	        protected set
	        {
		        _monoBehaviour = value;
	        }
        }

		/// <summary>
		/// An internal <see cref="Coroutine"/> that wraps and executes the user provided work unit. This <see cref="Coroutine"/> can be yielded to when waiting for the <see cref="CoreRoutine"/> to complete.
		/// </summary>
        public Coroutine Coroutine
        {
            get
            {
                return _coroutine;
			}
			protected set
			{
				_coroutine = value;
			}
        }

		/// <summary>
		/// Gets the <see cref="Exception"/> that caused the <see cref="CoreRoutine"/> to end prematurely. If the <see cref="CoreRoutine"/> completed successfully or has not yet thrown any exceptions, this will return null.
		/// </summary>
        public Exception Exception
        {
            get
            {
                return _exception;
			}
			protected set
			{
				_exception = value;
			}
        }

		/// <summary>
		/// Gets the <see cref="RoutineStatus"/> of this routine.
		/// </summary>
	    public RoutineStatus Status
	    {
		    get
		    {
			    ValidateHostBehaviour();
			    return _status;
		    }
		    protected set
		    {
			    _status = value;
		    }
	    }

		/// <summary>
		/// Gets whether this <see cref="CoreRoutine"/> has completed.
		/// </summary>
        public bool IsDone
        {
            get
            {
				return Status != RoutineStatus.Running;
            }
        }

		/// <summary>
		/// Gets whether this <see cref="CoreRoutine"/> instance has completed execution due to being canceled.
		/// </summary>
        public bool IsCanceled
        {
            get
            {
				return Status == RoutineStatus.Canceled;
            }
        }

		/// <summary>
		/// Gets whether this <see cref="CoreRoutine"/> is currently running.
		/// </summary>
        public bool IsRunning
        {
            get
            {
				return Status == RoutineStatus.Running;
            }
        }

        /// <summary>
		/// Gets whether this <see cref="CoreRoutine"/> completed due to an unhandled exception.
        /// </summary>
	    public bool IsFaulted
	    {
	        get
	        {
				return Status == RoutineStatus.Faulted;
	        }
	    }

		/// <summary>
		/// Gets whether this <see cref="CoreRoutine"/> completed without raising an exception.
		/// </summary>
		public bool IsSuccessful
		{
			get
			{
				return Status == RoutineStatus.RanToCompletion;
			}
		}


		protected RoutineCancellationToken CancellationToken
		{
			get
			{
				return _cancellationToken;
			}
			set
			{
				_cancellationToken = value;
			}
		}




        protected CoreRoutine()
        {
            
        }




	    private static CoreRoutine Basic(MonoBehaviour monoBehaviour, RoutineCancellationToken cancellationToken)
	    {
		    var ret = new CoreRoutine();

			ret.MonoBehaviour = monoBehaviour;
			ret.Exception = null;
			ret.Status = RoutineStatus.Running;
	        ret.CancellationToken = cancellationToken;

		    return ret;
	    }

		/// <summary>
		/// Starts a <see cref="CoreRoutine"/> that will wrap the provided coroutine.
		/// </summary>
		/// <param name="monoBehaviour">The <see cref="MonoBehaviour"/> that will host the internal wrapping <see cref="Coroutine"/>.</param>
		/// <param name="coroutine">The <see cref="Coroutine"/> to be wrapped.</param>
        public static CoreRoutine Start(MonoBehaviour monoBehaviour, IEnumerator coroutine)
        {
			Assert.IsNotNull(monoBehaviour, "monoBehaviour is null");
			Assert.IsNotNull(coroutine, "coroutine is null");

            return Start(monoBehaviour, coroutine, RoutineCancellationToken.None);
        }

		/// <summary>
		/// Starts a <see cref="CoreRoutine"/> that will wrap the provided coroutine.
		/// </summary>
		/// <param name="monoBehaviour">The <see cref="MonoBehaviour"/> that will host the internal wrapping <see cref="Coroutine"/>.</param>
		/// <param name="coroutine">The <see cref="Coroutine"/> to be wrapped.</param>
		/// <param name="cancellationToken">A cancellation token that should be used to cancel the work.</param>
	    public static CoreRoutine Start(MonoBehaviour monoBehaviour, IEnumerator coroutine, RoutineCancellationToken cancellationToken)
	    {
			Assert.IsNotNull(monoBehaviour, "monoBehaviour is null");
			Assert.IsNotNull(coroutine, "coroutine is null");
			Assert.IsNotNull(cancellationToken, "cancellationToken is null");

		    var ret = Basic(monoBehaviour, cancellationToken);

		    ret.InitializeCoroutine(coroutine);

		    return ret;
	    }

		/// <summary>
		/// Starts a <see cref="CoreRoutine"/> that will wrap an asynchronously executed task.
		/// </summary>
		/// <param name="monoBehaviour">The <see cref="MonoBehaviour"/> that will host the internal wrapping <see cref="Coroutine"/>.</param>
		/// <param name="task">The work to execute asynchronously.</param>
		public static CoreRoutine Task(MonoBehaviour monoBehaviour, Action task)
		{
			return Task(monoBehaviour, task, RoutineCancellationToken.None);
		}

		/// <summary>
		/// Starts a <see cref="CoreRoutine"/> that will wrap an asynchronously executed task.
		/// </summary>
		/// <param name="monoBehaviour">The <see cref="MonoBehaviour"/> that will host the internal wrapping <see cref="Coroutine"/>.</param>
		/// <param name="task">The work to execute asynchronously.</param>
		/// <param name="cancellationToken">A cancellation token that should be used to cancel the work.</param>
        public static CoreRoutine Task(MonoBehaviour monoBehaviour, Action task, RoutineCancellationToken cancellationToken)
        {
            var ret = Basic(monoBehaviour, cancellationToken);

            ret.InitializeCoroutine(ret.TaskRoutine(task));

            return ret;
        }

		/// <summary>
		/// Creates a routine that will complete when all of the <see cref="CoreRoutine"/> objects in an array have completed.
		/// </summary>
		/// <param name="monoBehaviour">The <see cref="MonoBehaviour"/> that will host the internal wrapping <see cref="Coroutine"/>.</param>
		/// <param name="routines">The routines to wait on for completion.</param>
	    public static CoreRoutine WhenAll(MonoBehaviour monoBehaviour, CoreRoutine[] routines)
        {
			Assert.IsNotNull(monoBehaviour, "monoBehaviour is null");
			Assert.IsNotNull(routines, "routines is null");

            return WhenAll(monoBehaviour, routines, RoutineCancellationToken.None);
        }

		/// <summary>
		/// Creates a routine that will complete when all of the <see cref="CoreRoutine"/> objects in an array have completed.
		/// </summary>
		/// <param name="monoBehaviour">The <see cref="MonoBehaviour"/> that will host the internal wrapping <see cref="Coroutine"/>.</param>
		/// <param name="routines">The routines to wait on for completion.</param>
		/// <param name="cancellationToken">A cancellation token that should be used to cancel the work.</param>
        public static CoreRoutine WhenAll(MonoBehaviour monoBehaviour, CoreRoutine[] routines, RoutineCancellationToken cancellationToken)
        {
            var ret = Basic(monoBehaviour, cancellationToken);
            
            ret.InitializeCoroutine(ret.WhenAllRoutine(routines));

            return ret;
        }

		/// <summary>
		/// Creates a routine that will complete when any of the <see cref="CoreRoutine"/> objects in an array has completed.
		/// </summary>
		/// <param name="monoBehaviour">The <see cref="MonoBehaviour"/> that will host the internal wrapping <see cref="Coroutine"/>.</param>
		/// <param name="routines">The routines to wait on for completion.</param>
        public static CoreRoutine WhenAny(MonoBehaviour monoBehaviour, CoreRoutine[] routines)
        {
            return WhenAny(monoBehaviour, routines, RoutineCancellationToken.None);
        }

		/// <summary>
		/// Creates a routine that will complete when any of the <see cref="CoreRoutine"/> objects in an array has completed.
		/// </summary>
		/// <param name="monoBehaviour">The <see cref="MonoBehaviour"/> that will host the internal wrapping <see cref="Coroutine"/>.</param>
		/// <param name="routines">The routines to wait on for completion.</param>
		/// <param name="cancellationToken">A cancellation token that should be used to cancel the work.</param>
        public static CoreRoutine WhenAny(MonoBehaviour monoBehaviour, CoreRoutine[] routines, RoutineCancellationToken cancellationToken)
        {
            var ret = Basic(monoBehaviour, cancellationToken);

            ret.InitializeCoroutine(ret.WhenAnyRoutine(routines));

            return ret;
        }

		/// <summary>
		/// Creates a routine that will complete when the evaluating function returns true.
		/// </summary>
		/// <param name="monoBehaviour">The <see cref="MonoBehaviour"/> that will host the internal wrapping <see cref="Coroutine"/>.</param>
		/// <param name="evaluate">The function to evaluate.</param>
		public static CoreRoutine WhenTrue(MonoBehaviour monoBehaviour, Func<bool> evaluate)
		{
			return WhenTrue(monoBehaviour, evaluate, RoutineCancellationToken.None);
		}

		/// <summary>
		/// Creates a routine that will complete when the evaluating function returns true.
		/// </summary>
		/// <param name="monoBehaviour">The <see cref="MonoBehaviour"/> that will host the internal wrapping <see cref="Coroutine"/>.</param>
		/// <param name="evaluate">The function to evaluate.</param>
		/// <param name="cancellationToken">A cancellation token that should be used to cancel the work.</param>
	    public static CoreRoutine WhenTrue(MonoBehaviour monoBehaviour, Func<bool> evaluate, RoutineCancellationToken cancellationToken)
	    {
			var ret = Basic(monoBehaviour, cancellationToken);

			ret.InitializeCoroutine(ret.WhenTrueRoutine(evaluate));

			return ret;
	    }

		/// <summary>
		/// Creates a routine that will complete when the evaluating function returns false.
		/// </summary>
		/// <param name="monoBehaviour">The <see cref="MonoBehaviour"/> that will host the internal wrapping <see cref="Coroutine"/>.</param>
		/// <param name="evaluate">The function to evaluate.</param>
		public static CoreRoutine WhenFalse(MonoBehaviour monoBehaviour, Func<bool> evaluate)
		{
			return WhenFalse(monoBehaviour, evaluate, RoutineCancellationToken.None);
		}

		/// <summary>
		/// Creates a routine that will complete when the evaluating function returns false.
		/// </summary>
		/// <param name="monoBehaviour">The <see cref="MonoBehaviour"/> that will host the internal wrapping <see cref="Coroutine"/>.</param>
		/// <param name="evaluate">The function to evaluate.</param>
		/// <param name="cancellationToken">A cancellation token that should be used to cancel the work.</param>
		public static CoreRoutine WhenFalse(MonoBehaviour monoBehaviour, Func<bool> evaluate, RoutineCancellationToken cancellationToken)
		{
			var ret = Basic(monoBehaviour, cancellationToken);

			ret.InitializeCoroutine(ret.WhenFalseRoutine(evaluate));

			return ret;
		}

		/// <summary>
		/// Creates a <see cref="CoreRoutine"/> that completes after an elapsed time.
		/// </summary>
		/// <param name="monoBehaviour">The <see cref="MonoBehaviour"/> that will host the internal wrapping <see cref="Coroutine"/>.</param>
		/// <param name="time">The number of seconds to wait before completing. Uses the default scaled Unity timer.</param>
        public static CoreRoutine Delay(MonoBehaviour monoBehaviour, float time)
		{
			return Delay(monoBehaviour, time, Time.Timer.Default, RoutineCancellationToken.None);
        }

		/// <summary>
		/// Creates a <see cref="CoreRoutine"/> that completes after an elapsed time.
		/// </summary>
		/// <param name="monoBehaviour">The <see cref="MonoBehaviour"/> that will host the internal wrapping <see cref="Coroutine"/>.</param>
		/// <param name="time">The number of seconds to wait before completing. Uses the default scaled Unity timer.</param>
		/// <param name="cancellationToken">A cancellation token that should be used to cancel the work.</param>
        public static CoreRoutine Delay(MonoBehaviour monoBehaviour, float time, RoutineCancellationToken cancellationToken)
		{
			return Delay(monoBehaviour, time, Time.Timer.Default, cancellationToken);
		}

		/// <summary>
		/// Creates a <see cref="CoreRoutine"/> that completes after an elapsed time.
		/// </summary>
		/// <param name="monoBehaviour">The <see cref="MonoBehaviour"/> that will host the internal wrapping <see cref="Coroutine"/>.</param>
		/// <param name="time">The number of seconds to wait before completing.</param>
		/// <param name="timer">The <see cref="ITimer"/> to use for tracking elapsed time.</param>
		public static CoreRoutine Delay<T>(MonoBehaviour monoBehaviour, float time, T timer)
			where T : ITimer
		{
			return Delay(monoBehaviour, time, timer, RoutineCancellationToken.None);
		}

		/// <summary>
		/// Creates a <see cref="CoreRoutine"/> that completes after an elapsed time.
		/// </summary>
		/// <param name="monoBehaviour">The <see cref="MonoBehaviour"/> that will host the internal wrapping <see cref="Coroutine"/>.</param>
		/// <param name="time">The number of seconds to wait before completing.</param>
		/// <param name="timer">The <see cref="ITimer"/> to use for tracking elapsed time.</param>
		/// <param name="cancellationToken">A cancellation token that should be used to cancel the work.</param>
		public static CoreRoutine Delay<T>(MonoBehaviour monoBehaviour, float time, T timer, RoutineCancellationToken cancellationToken)
			where T : ITimer
		{
			var ret = Basic(monoBehaviour, cancellationToken);

			ret.InitializeCoroutine(ret.DelayRoutine(time, timer));

			return ret;
		}

		/// <summary>
		/// Creates a routine that will execute when the <see cref="CoreRoutine"/> object has completed.
		/// </summary>
		/// <param name="monoBehaviour">The <see cref="MonoBehaviour"/> that will host the internal wrapping <see cref="Coroutine"/>.</param>
		/// <param name="routine">The <see cref="CoreRoutine"/> to wait on for completion.</param>
		/// <param name="coroutine">The work coroutine to execute.</param>
		/// <param name="continuationOptions">Options for how the continuation should behave.</param>
		/// <param name="cancellationToken">A cancellation token that should be used to cancel the work.</param>
        public static CoreRoutine ContinueWith(MonoBehaviour monoBehaviour, CoreRoutine routine, Func<CoreRoutine, IEnumerator> coroutine, RoutineContinuationOptions continuationOptions, RoutineCancellationToken cancellationToken)
        {
            var ret = Basic(monoBehaviour, cancellationToken);

            ret.InitializeCoroutine(ret.ContinueWithRoutine(routine, coroutine, continuationOptions));

            return ret;
        }

		/// <summary>
		/// Creates a routine that will execute when the <see cref="CoreRoutine"/> object has completed.
		/// </summary>
		/// <typeparam name="TAntecedentResult">The type of the result produced by this <see cref="CoreRoutine{TResult}"/></typeparam>
		/// <param name="monoBehaviour">The <see cref="MonoBehaviour"/> that will host the internal wrapping <see cref="Coroutine"/>.</param>
		/// <param name="routine">The <see cref="CoreRoutine"/> to wait on for completion.</param>
		/// <param name="coroutine">The work coroutine to execute.</param>
		/// <param name="continuationOptions">Options for how the continuation should behave.</param>
		/// <param name="cancellationToken">A cancellation token that should be used to cancel the work.</param>
		public static CoreRoutine ContinueWith<TAntecedentResult>(MonoBehaviour monoBehaviour, CoreRoutine<TAntecedentResult> routine, Func<CoreRoutine<TAntecedentResult>, IEnumerator> coroutine, RoutineContinuationOptions continuationOptions, RoutineCancellationToken cancellationToken)
        {
            var ret = Basic(monoBehaviour, cancellationToken);

            ret.InitializeCoroutine(ret.ContinueWithRoutine(routine, coroutine, continuationOptions));

            return ret;
        }

		/// <summary>
		/// Creates a callback routine that will execute when the <see cref="CoreRoutine"/> object has completed.
		/// </summary>
		/// <param name="monoBehaviour">The <see cref="MonoBehaviour"/> that will host the internal wrapping <see cref="Coroutine"/>.</param>
		/// <param name="routine">The <see cref="CoreRoutine"/> to wait on for completion.</param>
		/// <param name="callback">The work callback to execute.</param>
		/// <param name="continuationOptions">Options for how the continuation should behave.</param>
		/// <param name="cancellationToken">A cancellation token that should be used to cancel the work.</param>
		public static CoreRoutine ContinueWithCallback(MonoBehaviour monoBehaviour, CoreRoutine routine, Action<CoreRoutine> callback, RoutineContinuationOptions continuationOptions, RoutineCancellationToken cancellationToken)
		{
			var ret = Basic(monoBehaviour, cancellationToken);

			ret.InitializeCoroutine(ret.ContinueWithCallbackRoutine(routine, callback, continuationOptions));

			return ret;
		}

		/// <summary>
		/// Creates a callback routine that will execute when the <see cref="CoreRoutine"/> object has completed.
		/// </summary>
		/// <typeparam name="TAntecedentResult">The type of the result produced by this <see cref="CoreRoutine{TResult}"/></typeparam>
		/// <param name="monoBehaviour">The <see cref="MonoBehaviour"/> that will host the internal wrapping <see cref="Coroutine"/>.</param>
		/// <param name="routine">The <see cref="CoreRoutine"/> to wait on for completion.</param>
		/// <param name="callback">The work callback to execute.</param>
		/// <param name="continuationOptions">Options for how the continuation should behave.</param>
		/// <param name="cancellationToken">A cancellation token that should be used to cancel the work.</param>
		public static CoreRoutine ContinueWithCallback<TAntecedentResult>(MonoBehaviour monoBehaviour, CoreRoutine<TAntecedentResult> routine, Action<CoreRoutine<TAntecedentResult>> callback, RoutineContinuationOptions continuationOptions, RoutineCancellationToken cancellationToken)
		{
			var ret = Basic(monoBehaviour, cancellationToken);

			ret.InitializeCoroutine(ret.ContinueWithCallbackRoutine(routine, callback, continuationOptions));

			return ret;
		}

		/// <summary>
		/// Creates a routine that will execute asychronously when the <see cref="CoreRoutine"/> object has completed.
		/// </summary>
		/// <param name="monoBehaviour">The <see cref="MonoBehaviour"/> that will host the internal wrapping <see cref="Coroutine"/>.</param>
		/// <param name="routine">The <see cref="CoreRoutine"/> to wait on for completion.</param>
		/// <param name="task">The work to execute asynchronously.</param>
		/// <param name="continuationOptions">Options for how the continuation should behave.</param>
		/// <param name="cancellationToken">A cancellation token that should be used to cancel the work.</param>
        public static CoreRoutine ContinueWithTask(MonoBehaviour monoBehaviour, CoreRoutine routine, Action<CoreRoutine> task, RoutineContinuationOptions continuationOptions, RoutineCancellationToken cancellationToken)
        {
            var ret = Basic(monoBehaviour, cancellationToken);

            ret.InitializeCoroutine(ret.ContinueWithTaskRoutine(routine, task, continuationOptions));

            return ret;
        }

		/// <summary>
		/// Creates a routine that will execute asychronously when the <see cref="CoreRoutine"/> object has completed.
		/// </summary>
		/// <typeparam name="TAntecedentResult">The type of the result produced by this <see cref="CoreRoutine{TResult}"/></typeparam>
		/// <param name="monoBehaviour">The <see cref="MonoBehaviour"/> that will host the internal wrapping <see cref="Coroutine"/>.</param>
		/// <param name="routine">The <see cref="CoreRoutine"/> to wait on for completion.</param>
		/// <param name="task">The work to execute asynchronously.</param>
		/// <param name="continuationOptions">Options for how the continuation should behave.</param>
		/// <param name="cancellationToken">A cancellation token that should be used to cancel the work.</param>
		public static CoreRoutine ContinueWithTask<TAntecedentResult>(MonoBehaviour monoBehaviour, CoreRoutine<TAntecedentResult> routine, Action<CoreRoutine<TAntecedentResult>> task, RoutineContinuationOptions continuationOptions, RoutineCancellationToken cancellationToken)
        {
            var ret = Basic(monoBehaviour, cancellationToken);

            ret.InitializeCoroutine(ret.ContinueWithTaskRoutine(routine, task, continuationOptions));

            return ret;
        }

		/// <summary>
		/// Creates a routine that will execute when all of the <see cref="CoreRoutine"/> objects in an array have completed.
		/// </summary>
		/// <param name="monoBehaviour">The <see cref="MonoBehaviour"/> that will host the internal wrapping <see cref="Coroutine"/>.</param>
		/// <param name="routines">The routines to wait on for completion.</param>
		/// <param name="coroutine">The work coroutine to execute.</param>
		/// <param name="cancellationToken">A cancellation token that should be used to cancel the work.</param>
        public static CoreRoutine ContinueWhenAll(MonoBehaviour monoBehaviour, CoreRoutine[] routines, Func<CoreRoutine[], IEnumerator> coroutine, RoutineCancellationToken cancellationToken)
        {
            var ret = Basic(monoBehaviour, cancellationToken);

            ret.InitializeCoroutine(ret.ContinueWhenAllRoutine(routines, coroutine));

            return ret;
        }

		/// <summary>
		/// Creates a routine that will execute when all of the <see cref="CoreRoutine"/> objects in an array have completed.
		/// </summary>
		/// <typeparam name="TAntecedentResult">The type of the result produced by this <see cref="CoreRoutine{TResult}"/></typeparam>
		/// <param name="monoBehaviour">The <see cref="MonoBehaviour"/> that will host the internal wrapping <see cref="Coroutine"/>.</param>
		/// <param name="routines">The routines to wait on for completion.</param>
		/// <param name="coroutine">The work coroutine to execute.</param>
		/// <param name="cancellationToken">A cancellation token that should be used to cancel the work.</param>
		public static CoreRoutine ContinueWhenAll<TAntecedentResult>(MonoBehaviour monoBehaviour, CoreRoutine<TAntecedentResult>[] routines, Func<CoreRoutine<TAntecedentResult>[], IEnumerator> coroutine, RoutineCancellationToken cancellationToken)
        {
            var ret = Basic(monoBehaviour, cancellationToken);

            ret.InitializeCoroutine(ret.ContinueWhenAllRoutine(routines, coroutine));

            return ret;
        }
		
		/// <summary>
		/// Creates a callback routine that will execute when all of the <see cref="CoreRoutine"/> objects in an array have completed.
		/// </summary>
		/// <param name="monoBehaviour">The <see cref="MonoBehaviour"/> that will host the internal wrapping <see cref="Coroutine"/>.</param>
		/// <param name="routines">The routines to wait on for completion.</param>
		/// <param name="callback">The work callback to execute.</param>
		/// <param name="cancellationToken">A cancellation token that should be used to cancel the work.</param>
		public static CoreRoutine ContinueCallbackWhenAll(MonoBehaviour monoBehaviour, CoreRoutine[] routines, Action<CoreRoutine[]> callback, RoutineCancellationToken cancellationToken)
		{
			var ret = Basic(monoBehaviour, cancellationToken);

			ret.InitializeCoroutine(ret.ContinueCallbackWhenAllRoutine(routines, callback));

			return ret;
		}

		/// <summary>
		/// Creates a callback routine that will execute when all of the <see cref="CoreRoutine"/> objects in an array have completed.
		/// </summary>
		/// <typeparam name="TAntecedentResult">The type of the result produced by this <see cref="CoreRoutine{TResult}"/></typeparam>
		/// <param name="monoBehaviour">The <see cref="MonoBehaviour"/> that will host the internal wrapping <see cref="Coroutine"/>.</param>
		/// <param name="routines">The routines to wait on for completion.</param>
		/// <param name="callback">The work callback to execute.</param>
		/// <param name="cancellationToken">A cancellation token that should be used to cancel the work.</param>
		public static CoreRoutine ContinueCallbackWhenAll<TAntecedentResult>(MonoBehaviour monoBehaviour, CoreRoutine<TAntecedentResult>[] routines, Action<CoreRoutine<TAntecedentResult>[]> callback, RoutineCancellationToken cancellationToken)
		{
			var ret = Basic(monoBehaviour, cancellationToken);

			ret.InitializeCoroutine(ret.ContinueCallbackWhenAllRoutine(routines, callback));

			return ret;
		}

		/// <summary>
		/// Creates a routine that will execute asynchronously when all of the <see cref="CoreRoutine"/> objects in an array have completed.
		/// </summary>
		/// <param name="monoBehaviour">The <see cref="MonoBehaviour"/> that will host the internal wrapping <see cref="Coroutine"/>.</param>
		/// <param name="routines">The routines to wait on for completion.</param>
		/// <param name="task">The work to execute asynchronously.</param>
		/// <param name="cancellationToken">A cancellation token that should be used to cancel the work.</param>
        public static CoreRoutine ContinueTaskWhenAll(MonoBehaviour monoBehaviour, CoreRoutine[] routines, Action<CoreRoutine[]> task, RoutineCancellationToken cancellationToken)
        {
            var ret = Basic(monoBehaviour, cancellationToken);

            ret.InitializeCoroutine(ret.ContinueTaskWhenAllRoutine(routines, task));

            return ret;
        }

		/// <summary>
		/// Creates a routine that will execute asynchronously when all of the <see cref="CoreRoutine"/> objects in an array have completed.
		/// </summary>
		/// <typeparam name="TAntecedentResult">The type of the result produced by this <see cref="CoreRoutine{TResult}"/></typeparam>
		/// <param name="monoBehaviour">The <see cref="MonoBehaviour"/> that will host the internal wrapping <see cref="Coroutine"/>.</param>
		/// <param name="routines">The routines to wait on for completion.</param>
		/// <param name="task">The work to execute asynchronously.</param>
		/// <param name="cancellationToken">A cancellation token that should be used to cancel the work.</param>
		public static CoreRoutine ContinueTaskWhenAll<TAntecedentResult>(MonoBehaviour monoBehaviour, CoreRoutine<TAntecedentResult>[] routines, Action<CoreRoutine<TAntecedentResult>[]> task, RoutineCancellationToken cancellationToken)
        {
            var ret = Basic(monoBehaviour, cancellationToken);

            ret.InitializeCoroutine(ret.ContinueTaskWhenAllRoutine(routines, task));

            return ret;
        }

		/// <summary>
		/// Creates a routine that will execute when any of the <see cref="CoreRoutine"/> objects in an array has completed.
		/// </summary>
		/// <param name="monoBehaviour">The <see cref="MonoBehaviour"/> that will host the internal wrapping <see cref="Coroutine"/>.</param>
		/// <param name="routines">The routines to wait on for completion.</param>
		/// <param name="coroutine">The work coroutine to execute.</param>
		/// <param name="cancellationToken">A cancellation token that should be used to cancel the work.</param>
        public static CoreRoutine ContinueWhenAny(MonoBehaviour monoBehaviour, CoreRoutine[] routines, Func<CoreRoutine, IEnumerator> coroutine, RoutineCancellationToken cancellationToken)
        {
            var ret = Basic(monoBehaviour, cancellationToken);

            ret.InitializeCoroutine(ret.ContinueWhenAnyRoutine(routines, coroutine));

            return ret;
        }

		/// <summary>
		/// Creates a routine that will execute when any of the <see cref="CoreRoutine"/> objects in an array has completed.
		/// </summary>
		/// <typeparam name="TAntecedentResult">The type of the result produced by this <see cref="CoreRoutine{TResult}"/></typeparam>
		/// <param name="monoBehaviour">The <see cref="MonoBehaviour"/> that will host the internal wrapping <see cref="Coroutine"/>.</param>
		/// <param name="routines">The routines to wait on for completion.</param>
		/// <param name="coroutine">The work coroutine to execute.</param>
		/// <param name="cancellationToken">A cancellation token that should be used to cancel the work.</param>
		public static CoreRoutine ContinueWhenAny<TAntecedentResult>(MonoBehaviour monoBehaviour, CoreRoutine<TAntecedentResult>[] routines, Func<CoreRoutine<TAntecedentResult>, IEnumerator> coroutine, RoutineCancellationToken cancellationToken)
        {
            var ret = Basic(monoBehaviour, cancellationToken);

            ret.InitializeCoroutine(ret.ContinueWhenAnyRoutine(routines, coroutine));

            return ret;
        }
		
		/// <summary>
		/// Creates a routine that will execute when any of the <see cref="CoreRoutine"/> objects in an array has completed.
		/// </summary>
		/// <param name="monoBehaviour">The <see cref="MonoBehaviour"/> that will host the internal wrapping <see cref="Coroutine"/>.</param>
		/// <param name="routines">The routines to wait on for completion.</param>
		/// <param name="callback">The work callback to execute.</param>
		/// <param name="cancellationToken">A cancellation token that should be used to cancel the work.</param>
		/// <returns></returns>
		public static CoreRoutine ContinueCallbackWhenAny(MonoBehaviour monoBehaviour, CoreRoutine[] routines, Action<CoreRoutine> callback, RoutineCancellationToken cancellationToken)
		{
			var ret = Basic(monoBehaviour, cancellationToken);

			ret.InitializeCoroutine(ret.ContinueCallbackWhenAnyRoutine(routines, callback));

			return ret;
		}

		/// <summary>
		/// Creates a routine that will execute when any of the <see cref="CoreRoutine"/> objects in an array has completed.
		/// </summary>
		/// <typeparam name="TAntecedentResult">The type of the result produced by this <see cref="CoreRoutine{TResult}"/></typeparam>
		/// <param name="monoBehaviour">The <see cref="MonoBehaviour"/> that will host the internal wrapping <see cref="Coroutine"/>.</param>
		/// <param name="routines">The routines to wait on for completion.</param>
		/// <param name="callback">The work callback to execute.</param>
		/// <param name="cancellationToken">A cancellation token that should be used to cancel the work.</param>
		public static CoreRoutine ContinueCallbackWhenAny<TAntecedentResult>(MonoBehaviour monoBehaviour, CoreRoutine<TAntecedentResult>[] routines, Action<CoreRoutine<TAntecedentResult>> callback, RoutineCancellationToken cancellationToken)
		{
			var ret = Basic(monoBehaviour, cancellationToken);

			ret.InitializeCoroutine(ret.ContinueCallbackWhenAnyRoutine(routines, callback));

			return ret;
		}

		/// <summary>
		/// Creates a routine that will execute asynchronously when any of the <see cref="CoreRoutine"/> objects in an array has completed.
		/// </summary>
		/// <param name="monoBehaviour">The <see cref="MonoBehaviour"/> that will host the internal wrapping <see cref="Coroutine"/>.</param>
		/// <param name="routines">The routines to wait on for completion.</param>
		/// <param name="task">The work to execute asynchronously.</param>
		/// <param name="cancellationToken">A cancellation token that should be used to cancel the work.</param>
        public static CoreRoutine ContinueTaskWhenAny(MonoBehaviour monoBehaviour, CoreRoutine[] routines, Action<CoreRoutine> task, RoutineCancellationToken cancellationToken)
        {
            var ret = Basic(monoBehaviour, cancellationToken);

            ret.InitializeCoroutine(ret.ContinueTaskWhenAnyRoutine(routines, task));

            return ret;
        }

		/// <summary>
		/// Creates a routine that will execute asynchronously when any of the <see cref="CoreRoutine"/> objects in an array has completed.
		/// </summary>
		/// <typeparam name="TAntecedentResult">The type of the result produced by this <see cref="CoreRoutine{TResult}"/></typeparam>
		/// <param name="monoBehaviour">The <see cref="MonoBehaviour"/> that will host the internal wrapping <see cref="Coroutine"/>.</param>
		/// <param name="routines">The routines to wait on for completion.</param>
		/// <param name="task">The work to execute asynchronously.</param>
		/// <param name="cancellationToken">A cancellation token that should be used to cancel the work.</param>
		public static CoreRoutine ContinueTaskWhenAny<TAntecedentResult>(MonoBehaviour monoBehaviour, CoreRoutine<TAntecedentResult>[] routines, Action<CoreRoutine<TAntecedentResult>> task, RoutineCancellationToken cancellationToken)
        {
            var ret = Basic(monoBehaviour, cancellationToken);

            ret.InitializeCoroutine(ret.ContinueTaskWhenAnyRoutine(routines, task));

            return ret;
        }



		/// <summary>
		/// Creates a continuation that executes as a coroutine when the target <see cref="CoreRoutine"/> completes.
		/// </summary>
		/// <param name="coroutine">The work coroutine to execute.</param>
        public CoreRoutine ContinueWith(Func<CoreRoutine, IEnumerator> coroutine)
        {
            return ContinueWith(MonoBehaviour, this, coroutine, RoutineContinuationOptions.None, RoutineCancellationToken.None);
        }

		/// <summary>
		/// Creates a continuation that executes as a coroutine when the target <see cref="CoreRoutine"/> completes.
		/// </summary>
		/// <param name="coroutine">The work coroutine to execute.</param>
		/// <param name="cancellationToken">A cancellation token that should be used to cancel the work.</param>
        public CoreRoutine ContinueWith(Func<CoreRoutine, IEnumerator> coroutine, RoutineCancellationToken cancellationToken)
        {
            return ContinueWith(MonoBehaviour, this, coroutine, RoutineContinuationOptions.None, cancellationToken);
        }

		/// <summary>
		/// Creates a continuation that executes as a coroutine when the target <see cref="CoreRoutine"/> completes.
		/// </summary>
		/// <param name="coroutine">The work coroutine to execute.</param>
		/// <param name="continuationOptions">Options for how the continuation should behave.</param>
        public CoreRoutine ContinueWith(Func<CoreRoutine, IEnumerator> coroutine, RoutineContinuationOptions continuationOptions)
        {
            return ContinueWith(MonoBehaviour, this, coroutine, continuationOptions, RoutineCancellationToken.None);
        }

		/// <summary>
		/// Creates a continuation that executes as a coroutine when the target <see cref="CoreRoutine"/> completes.
		/// </summary>
		/// <param name="coroutine">The work coroutine to execute.</param>
		/// <param name="continuationOptions">Options for how the continuation should behave.</param>
		/// <param name="cancellationToken">A cancellation token that should be used to cancel the work.</param>
		public CoreRoutine ContinueWith(Func<CoreRoutine, IEnumerator> coroutine, RoutineContinuationOptions continuationOptions, RoutineCancellationToken cancellationToken)
		{
			return ContinueWith(MonoBehaviour, this, coroutine, continuationOptions, cancellationToken);
		}

		/// <summary>
		/// Creates a continuation that executes as a callback when the target <see cref="CoreRoutine"/> completes.
		/// </summary>
		/// <param name="callback">The work callback to execute.</param>
		public CoreRoutine ContinueWithCallback(Action<CoreRoutine> callback)
		{
			return ContinueWithCallback(MonoBehaviour, this, callback, RoutineContinuationOptions.None, RoutineCancellationToken.None);
		}

		/// <summary>
		/// Creates a continuation that executes as a callback when the target <see cref="CoreRoutine"/> completes.
		/// </summary>
		/// <param name="callback">The work callback to execute.</param>
		/// <param name="cancellationToken">A cancellation token that should be used to cancel the work.</param>
		public CoreRoutine ContinueWithCallback(Action<CoreRoutine> callback, RoutineCancellationToken cancellationToken)
		{
			return ContinueWithCallback(MonoBehaviour, this, callback, RoutineContinuationOptions.None, cancellationToken);
		}

		/// <summary>
		/// Creates a continuation that executes as a callback when the target <see cref="CoreRoutine"/> completes.
		/// </summary>
		/// <param name="callback">The work callback to execute.</param>
		/// <param name="continuationOptions">Options for how the continuation should behave.</param>
		public CoreRoutine ContinueWithCallback(Action<CoreRoutine> callback, RoutineContinuationOptions continuationOptions)
		{
			return ContinueWithCallback(MonoBehaviour, this, callback, continuationOptions, RoutineCancellationToken.None);
		}

		/// <summary>
		/// Creates a continuation that executes as a callback when the target <see cref="CoreRoutine"/> completes.
		/// </summary>
		/// <param name="callback">The work callback to execute.</param>
		/// <param name="continuationOptions">Options for how the continuation should behave.</param>
		/// <param name="cancellationToken">A cancellation token that should be used to cancel the work.</param>
		public CoreRoutine ContinueWithCallback(Action<CoreRoutine> callback, RoutineContinuationOptions continuationOptions, RoutineCancellationToken cancellationToken)
		{
			return ContinueWithCallback(MonoBehaviour, this, callback, continuationOptions, cancellationToken);
		}

		/// <summary>
		/// Creates a continuation that executes asynchronously when the target <see cref="CoreRoutine"/> completes.
		/// </summary>
		/// <param name="task">The work to execute asynchronously.</param>
        public CoreRoutine ContinueWithTask(Action<CoreRoutine> task)
        {
            return ContinueWithTask(MonoBehaviour, this, task, RoutineContinuationOptions.None, RoutineCancellationToken.None);
        }

		/// <summary>
		/// Creates a continuation that executes asynchronously when the target <see cref="CoreRoutine"/> completes.
		/// </summary>
		/// <param name="task">The work to execute asynchronously.</param>
		/// <param name="cancellationToken">A cancellation token that should be used to cancel the work.</param>
        public CoreRoutine ContinueWithTask(Action<CoreRoutine> task, RoutineCancellationToken cancellationToken)
        {
            return ContinueWithTask(MonoBehaviour, this, task, RoutineContinuationOptions.None, cancellationToken);
        }

		/// <summary>
		/// Creates a continuation that executes asynchronously when the target <see cref="CoreRoutine"/> completes.
		/// </summary>
		/// <param name="task">The work to execute asynchronously.</param>
		/// <param name="continuationOptions">Options for how the continuation should behave.</param>
        public CoreRoutine ContinueWithTask(Action<CoreRoutine> task, RoutineContinuationOptions continuationOptions)
        {
            return ContinueWithTask(MonoBehaviour, this, task, continuationOptions, RoutineCancellationToken.None);
        }

		/// <summary>
		/// Creates a continuation that executes asynchronously when the target <see cref="CoreRoutine"/> completes.
		/// </summary>
		/// <param name="task">The work to execute asynchronously.</param>
		/// <param name="continuationOptions">Options for how the continuation should behave.</param>
		/// <param name="cancellationToken">A cancellation token that should be used to cancel the work.</param>
		public CoreRoutine ContinueWithTask(Action<CoreRoutine> task, RoutineContinuationOptions continuationOptions, RoutineCancellationToken cancellationToken)
		{
			return ContinueWithTask(MonoBehaviour, this, task, continuationOptions, cancellationToken);
		}




		protected void ValidateHostBehaviour()
		{
			if (_status == RoutineStatus.Running &&
			    _monoBehaviour == null)
			{
				_status = RoutineStatus.Faulted;
				_exception = new RoutineHostDestroyedException();
			}
		}


        protected void InitializeCoroutine(IEnumerator coroutine)
        {
            Coroutine = MonoBehaviour.StartCoroutine(BasicRoutine(coroutine));
        }


        protected virtual IEnumerator BasicRoutine(IEnumerator coroutine)
        {
            while (true)
            {
                try
                {
                    if (!coroutine.MoveNext())
                    {
                        Status = RoutineStatus.RanToCompletion;

                        yield break;
                    }
                }
                catch (RoutineCanceledException e)
				{
					Exception = e;

	                if (e.Token == CancellationToken)
	                {
		                Status = RoutineStatus.Canceled;
	                }
	                else
	                {
		                Status = RoutineStatus.Faulted;
	                }

                    yield break;
                }
                catch (Exception e)
                {
					Debug.Logger.LogException(e, MonoBehaviour);
                    Status = RoutineStatus.Faulted;
                    Exception = e;

                    yield break;
                }

                yield return coroutine.Current;
            }
        }


		protected IEnumerator WhenAllRoutine(CoreRoutine[] routines)
        {
            CancellationToken.ThrowIfCancellationRequested();

	        while (true)
			{
				for (int i = 0; i < routines.Length; i++)
				{
					if (!routines[i].IsDone)
					{
						goto NotAllRoutinesDone;
					}
				}

				break;

			NotAllRoutinesDone:
				yield return null;
				CancellationToken.ThrowIfCancellationRequested();
	        }
        }


		protected IEnumerator WhenAnyRoutine(CoreRoutine[] routines)
        {
            CancellationToken.ThrowIfCancellationRequested();

            while (true)
            {
	            for (int i = 0; i < routines.Length; i++)
	            {
					if (routines[i].IsDone)
					{
						yield break;
					}
	            }

                yield return null;
                CancellationToken.ThrowIfCancellationRequested();
            }
        }


		protected IEnumerator WhenTrueRoutine(Func<bool> evaluate)
		{
			CancellationToken.ThrowIfCancellationRequested();

			while (!evaluate())
			{
				yield return null;
				CancellationToken.ThrowIfCancellationRequested();
			}
		}


		protected IEnumerator WhenFalseRoutine(Func<bool> evaluate)
		{
			CancellationToken.ThrowIfCancellationRequested();

			while (evaluate())
			{
				yield return null;
				CancellationToken.ThrowIfCancellationRequested();
			}
		}


		protected IEnumerator DelayRoutine<T>(float time, T timer)
			where T : ITimer
        {
            CancellationToken.ThrowIfCancellationRequested();

			float endTime = timer.Time + time;

			while (timer.Time < endTime)
            {
                yield return null;
                CancellationToken.ThrowIfCancellationRequested();
            }
        }


        protected IEnumerator TaskRoutine(Action action)
        {
            Exception exception = null;

            var thread = new Thread(() =>
            {
                try
                {
                    action();
                }
                catch (Exception e)
                {
                    exception = e;
                }
            });

            thread.Start();

            while (true)
            {
                if (thread.IsAlive)
                {
                    yield return null;
                    continue;
                }

                if (exception != null)
                {
                    throw exception;
                }

                yield break;
            }
        }


        protected IEnumerator ContinueWhenAllRoutine(CoreRoutine[] routines, Func<CoreRoutine[], IEnumerator> coroutine)
        {
            CancellationToken.ThrowIfCancellationRequested();

            while (true)
            {
	            for (int i = 0; i < routines.Length; i++)
	            {
		            if (!routines[i].IsDone)
		            {
			            goto NotAllRoutinesDone;
		            }       
	            }

	            break;

            NotAllRoutinesDone:
                yield return null;
                CancellationToken.ThrowIfCancellationRequested();
            }
 
			IEnumerator c = coroutine(routines);

			while (c.MoveNext())
			{
				yield return c.Current;
			}
        }


		protected IEnumerator ContinueWhenAllRoutine<TAntecedentResult>(CoreRoutine<TAntecedentResult>[] routines, Func<CoreRoutine<TAntecedentResult>[], IEnumerator> coroutine)
        {
            CancellationToken.ThrowIfCancellationRequested();

            while (true)
			{
				for (int i = 0; i < routines.Length; i++)
				{
					if (!routines[i].IsDone)
					{
						goto NotAllRoutinesDone;
					}
				}

				break;

            NotAllRoutinesDone:
                yield return null;
                CancellationToken.ThrowIfCancellationRequested();
            }

	        IEnumerator c = coroutine(routines);

	        while (c.MoveNext())
	        {
		        yield return c.Current;
	        }
		}


		protected IEnumerator ContinueCallbackWhenAllRoutine(CoreRoutine[] routines, Action<CoreRoutine[]> callback)
		{
			CancellationToken.ThrowIfCancellationRequested();

			while (true)
			{
				for (int i = 0; i < routines.Length; i++)
				{
					if (!routines[i].IsDone)
					{
						goto NotAllRoutinesDone;
					}
				}

				break;

			NotAllRoutinesDone:
				yield return null;
				CancellationToken.ThrowIfCancellationRequested();
			}

			callback(routines);
		}


		protected IEnumerator ContinueCallbackWhenAllRoutine<TAntecedentResult>(CoreRoutine<TAntecedentResult>[] routines, Action<CoreRoutine<TAntecedentResult>[]> callback)
		{
			CancellationToken.ThrowIfCancellationRequested();

			while (true)
			{
				for (int i = 0; i < routines.Length; i++)
				{
					if (!routines[i].IsDone)
					{
						goto NotAllRoutinesDone;
					}
				}

				break;

			NotAllRoutinesDone:
				yield return null;
				CancellationToken.ThrowIfCancellationRequested();
			}

			callback(routines);
		}


        protected IEnumerator ContinueTaskWhenAllRoutine(CoreRoutine[] routines, Action<CoreRoutine[]> task)
        {
             CancellationToken.ThrowIfCancellationRequested();

            while (true)
			{
				for (int i = 0; i < routines.Length; i++)
				{
					if (!routines[i].IsDone)
					{
						goto NotAllRoutinesDone;
					}
				}

				break;

            NotAllRoutinesDone:
                yield return null;
                CancellationToken.ThrowIfCancellationRequested();
            }

			IEnumerator c = TaskRoutine(() => task(routines));

			while (c.MoveNext())
			{
				yield return c.Current;
			}
        }


		protected IEnumerator ContinueTaskWhenAllRoutine<TAntecedentResult>(CoreRoutine<TAntecedentResult>[] routines, Action<CoreRoutine<TAntecedentResult>[]> task)
        {
             CancellationToken.ThrowIfCancellationRequested();

            while (true)
			{
				for (int i = 0; i < routines.Length; i++)
				{
					if (!routines[i].IsDone)
					{
						goto NotAllRoutinesDone;
					}
				}

                break;

            NotAllRoutinesDone:
                yield return null;
                CancellationToken.ThrowIfCancellationRequested();
            }

			IEnumerator c = TaskRoutine(() => task(routines));

			while (c.MoveNext())
			{
				yield return c.Current;
			}
        }


        protected IEnumerator ContinueWhenAnyRoutine(CoreRoutine[] routines, Func<CoreRoutine, IEnumerator> coroutine)
        {
            CancellationToken.ThrowIfCancellationRequested();

            CoreRoutine doneRoutine;

            while (true)
			{
				for (int i = 0; i < routines.Length; i++)
				{
					if (routines[i].IsDone)
					{
						doneRoutine = routines[i];
						goto AnyRoutineDone;
					}
				}

                yield return null;
                CancellationToken.ThrowIfCancellationRequested();
            }

		AnyRoutineDone: 
			IEnumerator c = coroutine(doneRoutine);

			while (c.MoveNext())
			{
				yield return c.Current;
			}
        }


		protected IEnumerator ContinueWhenAnyRoutine<TAntecedentResult>(CoreRoutine<TAntecedentResult>[] routines, Func<CoreRoutine<TAntecedentResult>, IEnumerator> coroutine)
        {
            CancellationToken.ThrowIfCancellationRequested();

			CoreRoutine<TAntecedentResult> doneRoutine;

            while (true)
			{
				for (int i = 0; i < routines.Length; i++)
				{
					if (routines[i].IsDone)
					{
						doneRoutine = routines[i];
						goto AnyRoutineDone;
					}
				}

                yield return null;
                CancellationToken.ThrowIfCancellationRequested();
            }

        AnyRoutineDone:
			IEnumerator c = coroutine(doneRoutine);

			while (c.MoveNext())
			{
				yield return c.Current;
			}
		}


		protected IEnumerator ContinueCallbackWhenAnyRoutine(CoreRoutine[] routines, Action<CoreRoutine> callback)
		{
			CancellationToken.ThrowIfCancellationRequested();

			CoreRoutine doneRoutine;

			while (true)
			{
				for (int i = 0; i < routines.Length; i++)
				{
					if (routines[i].IsDone)
					{
						doneRoutine = routines[i];
						goto AnyRoutineDone;
					}
				}

				yield return null;
				CancellationToken.ThrowIfCancellationRequested();
			}

		AnyRoutineDone:
			callback(doneRoutine);
		}


		protected IEnumerator ContinueCallbackWhenAnyRoutine<TAntecedentResult>(CoreRoutine<TAntecedentResult>[] routines, Action<CoreRoutine<TAntecedentResult>> callback)
		{
			CancellationToken.ThrowIfCancellationRequested();

			CoreRoutine<TAntecedentResult> doneRoutine;

			while (true)
			{
				for (int i = 0; i < routines.Length; i++)
				{
					if (routines[i].IsDone)
					{
						doneRoutine = routines[i];
						goto AnyRoutineDone;
					}
				}

				yield return null;
				CancellationToken.ThrowIfCancellationRequested();
			}

		AnyRoutineDone:
			callback(doneRoutine);
		}


        protected IEnumerator ContinueTaskWhenAnyRoutine(CoreRoutine[] routines, Action<CoreRoutine> task)
        {
            CancellationToken.ThrowIfCancellationRequested();

            CoreRoutine doneRoutine;

            while (true)
			{
				for (int i = 0; i < routines.Length; i++)
				{
					if (routines[i].IsDone)
					{
						doneRoutine = routines[i];
						goto AnyRoutineDone;
					}
				}

                yield return null;
                CancellationToken.ThrowIfCancellationRequested();
            }

        AnyRoutineDone:
			IEnumerator c = TaskRoutine(() => task(doneRoutine));

			while (c.MoveNext())
			{
				yield return c.Current;
			}
        }


		protected IEnumerator ContinueTaskWhenAnyRoutine<TAntecedentResult>(CoreRoutine<TAntecedentResult>[] routines, Action<CoreRoutine<TAntecedentResult>> task)
        {
            CancellationToken.ThrowIfCancellationRequested();

			CoreRoutine<TAntecedentResult> doneRoutine;

            while (true)
			{
				for (int i = 0; i < routines.Length; i++)
				{
					if (routines[i].IsDone)
					{
						doneRoutine = routines[i];
						goto AnyRoutineDone;
					}
				}

                yield return null;
                CancellationToken.ThrowIfCancellationRequested();
            }

        AnyRoutineDone:
			IEnumerator c = TaskRoutine(() => task(doneRoutine));

			while (c.MoveNext())
			{
				yield return c.Current;
			}
        }


        protected IEnumerator ContinueWithRoutine(CoreRoutine routine, Func<CoreRoutine, IEnumerator> coroutine, RoutineContinuationOptions continuationOptions)
        {
            CancellationToken.ThrowIfCancellationRequested();

            while (!routine.IsDone)
            {
                yield return null;
                CancellationToken.ThrowIfCancellationRequested();
            }

            switch (routine.Status)
            {
                case RoutineStatus.Canceled:
                    if ((continuationOptions & RoutineContinuationOptions.NotOnCanceled) ==
                        RoutineContinuationOptions.NotOnCanceled)
                    {
                        yield break;
                    }
                    break;
                case RoutineStatus.Faulted:
                    if ((continuationOptions & RoutineContinuationOptions.NotOnFaulted) ==
                        RoutineContinuationOptions.NotOnFaulted)
                    {
                        yield break;
                    }
                    break;
                case RoutineStatus.RanToCompletion:
                    if ((continuationOptions & RoutineContinuationOptions.NotOnRanToCompletion) ==
                        RoutineContinuationOptions.NotOnRanToCompletion)
                    {
                        yield break;
                    }
                    break;
            }

			IEnumerator c = coroutine(routine);

			while (c.MoveNext())
			{
				yield return c.Current;
			}
        }


		protected IEnumerator ContinueWithRoutine<TAntecedentResult>(CoreRoutine<TAntecedentResult> routine, Func<CoreRoutine<TAntecedentResult>, IEnumerator> coroutine, RoutineContinuationOptions continuationOptions)
        {
            CancellationToken.ThrowIfCancellationRequested();

            while (!routine.IsDone)
            {
                yield return null;
                CancellationToken.ThrowIfCancellationRequested();
            }

            switch (routine.Status)
            {
                case RoutineStatus.Canceled:
                    if ((continuationOptions & RoutineContinuationOptions.NotOnCanceled) ==
                        RoutineContinuationOptions.NotOnCanceled)
                    {
                        yield break;
                    }
                    break;
                case RoutineStatus.Faulted:
                    if ((continuationOptions & RoutineContinuationOptions.NotOnFaulted) ==
                        RoutineContinuationOptions.NotOnFaulted)
                    {
                        yield break;
                    }
                    break;
                case RoutineStatus.RanToCompletion:
                    if ((continuationOptions & RoutineContinuationOptions.NotOnRanToCompletion) ==
                        RoutineContinuationOptions.NotOnRanToCompletion)
                    {
                        yield break;
                    }
                    break;
            }

			IEnumerator c = coroutine(routine);

			while (c.MoveNext())
			{
				yield return c.Current;
			}
        }


		protected IEnumerator ContinueWithCallbackRoutine(CoreRoutine routine, Action<CoreRoutine> callback, RoutineContinuationOptions continuationOptions)
		{
			CancellationToken.ThrowIfCancellationRequested();

			while (!routine.IsDone)
			{
				yield return null;
				CancellationToken.ThrowIfCancellationRequested();
			}

			switch (routine.Status)
			{
				case RoutineStatus.Canceled:
					if ((continuationOptions & RoutineContinuationOptions.NotOnCanceled) ==
						RoutineContinuationOptions.NotOnCanceled)
					{
						yield break;
					}
					break;
				case RoutineStatus.Faulted:
					if ((continuationOptions & RoutineContinuationOptions.NotOnFaulted) ==
						RoutineContinuationOptions.NotOnFaulted)
					{
						yield break;
					}
					break;
				case RoutineStatus.RanToCompletion:
					if ((continuationOptions & RoutineContinuationOptions.NotOnRanToCompletion) ==
						RoutineContinuationOptions.NotOnRanToCompletion)
					{
						yield break;
					}
					break;
			}

			callback(routine);
		}


		protected IEnumerator ContinueWithCallbackRoutine<TAntecedentResult>(CoreRoutine<TAntecedentResult> routine, Action<CoreRoutine<TAntecedentResult>> callback, RoutineContinuationOptions continuationOptions)
		{
			CancellationToken.ThrowIfCancellationRequested();

			while (!routine.IsDone)
			{
				yield return null;
				CancellationToken.ThrowIfCancellationRequested();
			}

			switch (routine.Status)
			{
				case RoutineStatus.Canceled:
					if ((continuationOptions & RoutineContinuationOptions.NotOnCanceled) ==
						RoutineContinuationOptions.NotOnCanceled)
					{
						yield break;
					}
					break;
				case RoutineStatus.Faulted:
					if ((continuationOptions & RoutineContinuationOptions.NotOnFaulted) ==
						RoutineContinuationOptions.NotOnFaulted)
					{
						yield break;
					}
					break;
				case RoutineStatus.RanToCompletion:
					if ((continuationOptions & RoutineContinuationOptions.NotOnRanToCompletion) ==
						RoutineContinuationOptions.NotOnRanToCompletion)
					{
						yield break;
					}
					break;
			}

			callback(routine);
		}


        protected IEnumerator ContinueWithTaskRoutine(CoreRoutine routine, Action<CoreRoutine> task, RoutineContinuationOptions continuationOptions)
        {
            CancellationToken.ThrowIfCancellationRequested();

            while (!routine.IsDone)
            {
                yield return null;
                CancellationToken.ThrowIfCancellationRequested();
            }

            switch (routine.Status)
            {
                case RoutineStatus.Canceled:
                    if ((continuationOptions & RoutineContinuationOptions.NotOnCanceled) ==
                        RoutineContinuationOptions.NotOnCanceled)
                    {
                        yield break;
                    }
                    break;
                case RoutineStatus.Faulted:
                    if ((continuationOptions & RoutineContinuationOptions.NotOnFaulted) ==
                        RoutineContinuationOptions.NotOnFaulted)
                    {
                        yield break;
                    }
                    break;
                case RoutineStatus.RanToCompletion:
                    if ((continuationOptions & RoutineContinuationOptions.NotOnRanToCompletion) ==
                        RoutineContinuationOptions.NotOnRanToCompletion)
                    {
                        yield break;
                    }
                    break;
            }

			IEnumerator c = TaskRoutine(() => task(routine));

			while (c.MoveNext())
			{
				yield return c.Current;
			}
        }


		protected IEnumerator ContinueWithTaskRoutine<TAntecedentResult>(CoreRoutine<TAntecedentResult> routine, Action<CoreRoutine<TAntecedentResult>> task, RoutineContinuationOptions continuationOptions)
        {
            CancellationToken.ThrowIfCancellationRequested();

            while (!routine.IsDone)
            {
                yield return null;
                CancellationToken.ThrowIfCancellationRequested();
            }

            switch (routine.Status)
            {
                case RoutineStatus.Canceled:
                    if ((continuationOptions & RoutineContinuationOptions.NotOnCanceled) ==
                        RoutineContinuationOptions.NotOnCanceled)
                    {
                        yield break;
                    }
                    break;
                case RoutineStatus.Faulted:
                    if ((continuationOptions & RoutineContinuationOptions.NotOnFaulted) ==
                        RoutineContinuationOptions.NotOnFaulted)
                    {
                        yield break;
                    }
                    break;
                case RoutineStatus.RanToCompletion:
                    if ((continuationOptions & RoutineContinuationOptions.NotOnRanToCompletion) ==
                        RoutineContinuationOptions.NotOnRanToCompletion)
                    {
                        yield break;
                    }
                    break;
            }

			IEnumerator c = TaskRoutine(() => task(routine));

			while (c.MoveNext())
			{
				yield return c.Current;
			}
        }
    }
}