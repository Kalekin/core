﻿using System;


namespace Core.Routine
{
	/// <summary>
	/// Signals to a <see cref="RoutineCancellationToken"/> that it should be canceled.
	/// </summary>
    public class RoutineCancellationSource
    {
        private bool _isCancellationRequested;
        private RoutineCancellationToken _token;
        private Action _callbacks;



		/// <summary>
		/// Gets whether cancellation has been requested for this RoutineCancellationSource.
		/// </summary>
        public bool IsCancellationRequested
        {
            get
            {
                return _isCancellationRequested;
            }
            private set
            {
                _isCancellationRequested = value;
            }
        }

		/// <summary>
		/// Gets the <see cref="RoutineCancellationToken"/> associated with this RoutineCancellationSource.
		/// </summary>
        public RoutineCancellationToken Token
        {
            get
            {
                return _token;
            }
        }


        internal Action Callbacks
        {
            get
            {
                return _callbacks;
            }
            set
            {
                _callbacks = value;
            }
        }



		/// <summary>
		/// Initializes a new instance of the <see cref="RoutineCancellationSource"/> class.
		/// </summary>
		public RoutineCancellationSource()
		{
			IsCancellationRequested = false;
			_token = new RoutineCancellationToken();
        }



		/// <summary>
		/// Creates a CancellationTokenSource that will be in the canceled state when any of the source tokens 
		/// in the specified array are in the canceled state.
		/// </summary>
		/// <param name="tokens"></param>
		/// <returns></returns>
        public static RoutineCancellationSource CreateLinkedTokenSource(params RoutineCancellationToken[] tokens)
        {
            var ret = new RoutineCancellationSource();

            ret.IsCancellationRequested = false;
			ret._token = new RoutineCancellationToken(ret);

            for (int i = 0; i < tokens.Length; ++i)
            {
                tokens[i].Register(ret.ParentTokenCancelledHandler);
            }

            return ret;
        }



		/// <summary>
		/// Communicates a request for cancellation.
		/// </summary>
        public void Cancel()
        {
            if (IsCancellationRequested)
            {
                return;
            }

            IsCancellationRequested = true;

	        if (Callbacks != null)
	        {
		        Callbacks();
	        }
        }


        private void ParentTokenCancelledHandler()
        {
            if (IsCancellationRequested)
            {
                return;
            }

            Cancel();
        }
    }
}
