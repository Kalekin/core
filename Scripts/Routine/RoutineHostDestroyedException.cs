﻿using System;


namespace Core.Routine
{
	/// <summary>
	/// Represents an exception indicating the destruction of a <see cref="CoreRoutine"/>'s hosting behaviour..
	/// </summary>
	public class RoutineHostDestroyedException : Exception
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="RoutineCanceledException"/> class.
		/// </summary>
		public RoutineHostDestroyedException()
			:base("The hosting behaviour for this routine was destroyed before completion.")
		{
		}
	}
}