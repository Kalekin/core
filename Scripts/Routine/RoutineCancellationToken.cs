﻿using System;


namespace Core.Routine
{
	/// <summary>
	/// Propagates notification that operations should be canceled.
	/// </summary>
    public struct RoutineCancellationToken : IEquatable<RoutineCancellationToken>
	{
		public static readonly RoutineCancellationToken None = new RoutineCancellationToken();
        private readonly RoutineCancellationSource _source;



		/// <summary>
		/// Gets whether this token is capable of being in the canceled state.
		/// </summary>
        public bool CanBeCanceled
        {
            get
            {
                return _source != null;
            }
        }

		/// <summary>
		/// Gets whether cancellation has been requested for this token.
		/// </summary>
        public bool IsCancellationRequested
        {
            get
            {
                if (!CanBeCanceled)
                {
                    return false;
                }

                return _source.IsCancellationRequested;
            }
        }



		/// <summary>
		/// Initializes the <see cref="RoutineCancellationToken"/> from a specified <see cref="RoutineCancellationSource"/>.
		/// </summary>
		/// <param name="source"></param>
		/// <returns></returns>
        public RoutineCancellationToken(RoutineCancellationSource source)
		{
			_source = source;
		}



		/// <summary>
		/// Determines whether two <see cref="RoutineCancellationToken"/> instances are equal.
		/// </summary>
		/// <param name="left"></param>
		/// <param name="right"></param>
		/// <returns></returns>
		public static bool operator ==(RoutineCancellationToken left, RoutineCancellationToken right)
		{
			return left.Equals(right);
		}

		/// <summary>
		/// Determines whether two <see cref="RoutineCancellationToken"/> instances are not equal.
		/// </summary>
		/// <param name="left"></param>
		/// <param name="right"></param>
		/// <returns></returns>
		public static bool operator !=(RoutineCancellationToken left, RoutineCancellationToken right)
		{
			return !left.Equals(right);
		}



		/// <summary>
		/// Indicates whether this instance and a specified object are equal.
		/// </summary>
		/// <returns>
		/// true if <paramref name="obj"/> and this instance are the same type and represent the same value; otherwise, false.
		/// </returns>
		/// <param name="obj">Another object to compare to. </param><filterpriority>2</filterpriority>
		public override bool Equals(object obj)
		{
			return base.Equals(obj);
		}

		/// <summary>
		/// Indicates whether this instance and a specified <see cref="RoutineCancellationToken"/> are equal.
		/// </summary>
		/// <returns>
		/// true if <paramref name="other"/> and this represent the same value; otherwise, false.
		/// </returns>
		/// <param name="other">Another <see cref="RoutineCancellationToken"/> to compare to. </param><filterpriority>2</filterpriority>
		public bool Equals(RoutineCancellationToken other)
		{
			return Equals(_source, other._source);
		}

		/// <summary>
		/// Returns the hash code for this instance.
		/// </summary>
		/// <returns>
		/// A 32-bit signed integer that is the hash code for this instance.
		/// </returns>
		/// <filterpriority>2</filterpriority>
		public override int GetHashCode()
		{
			return (_source != null ? _source.GetHashCode() : 0);
		}

		/// <summary>
		/// Registers a delegate that will be called when this <see cref="RoutineCancellationToken"/> is canceled.
        /// </summary>
        /// <param name="callback"></param>
        /// <param name="state"></param>
        /// <returns></returns>
        public RoutineCancellationTokenRegistration Register(Action<object> callback, object state)
        {
            return Register(() => callback(state));
        }

		/// <summary>
		/// Registers a delegate that will be called when this <see cref="RoutineCancellationToken"/> is canceled.
		/// </summary>
		/// <param name="callback"></param>
		/// <returns></returns>
        public RoutineCancellationTokenRegistration Register(Action callback)
        {
            return new RoutineCancellationTokenRegistration(_source, callback);
        }

		/// <summary>
		/// Throws a <see cref="RoutineCanceledException"/> if this token has had cancellation requested.
		/// </summary>
		/// <exception cref="RoutineCanceledException">The token has had cancellation requested.</exception>
        public void ThrowIfCancellationRequested()
        {
            if (IsCancellationRequested)
            {
                throw new RoutineCanceledException(this);
            }
        }
    }
}
