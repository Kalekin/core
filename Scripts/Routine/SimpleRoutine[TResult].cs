﻿using System;
using System.Collections;
using Core.Debug;
using UnityEngine;


namespace Core.Routine
{
	/// <summary>
	/// A utility class for encapsulating a basic <see cref="CoreRoutine{TResult}"/> with boilerplate cancelation behaviour.
	/// </summary>
	/// <typeparam name="TResult"></typeparam>
	public class SimpleRoutine<TResult> : SimpleRoutine
	{
		/// <summary>
		/// The <see cref="CoreRoutine{TResult}"/> wrapped by this <see cref="SimpleRoutine{TResult}"/>.
		/// </summary>
		public new CoreRoutine<TResult> Routine
		{
			get
			{
				return (CoreRoutine<TResult>)base.Routine;
			}
		}



		/// <summary>
		/// The result value of the wrapped <see cref="CoreRoutine{TResult}"/>.
		/// </summary>
		public TResult Value
		{
			get
			{
				return Routine.Result;
			}
		}



		/// <summary>
		/// Starts a <see cref="SimpleRoutine{TResult}"/> that encapsulates a <see cref="CoreRoutine"/> and a 
		/// <see cref="RoutineCancellationSource"/> for cancelation. The token will be monitored internally
		/// for cancelation.
		/// </summary>
		/// <param name="simpleRoutine">The <see cref="SimpleRoutine{TResult}"/> to store the created routine. If the routine is not null and is running it will be canceled.</param>
		/// <param name="monoBehaviour">The <see cref="MonoBehaviour"/> that will host the internal wrapping <see cref="Coroutine"/>.</param>
		/// <param name="coroutine">The <see cref="Coroutine"/> to be wrapped.</param>
		public static SimpleRoutine<TResult> StartResult(ref SimpleRoutine<TResult> simpleRoutine, MonoBehaviour monoBehaviour, Func<IEnumerator> coroutine)
		{
			Assert.IsNotNull(monoBehaviour);
			Assert.IsNotNull(coroutine);

			if (simpleRoutine != null)
			{
				simpleRoutine.Cancel();
			}

			simpleRoutine = new SimpleRoutine<TResult>();

			simpleRoutine.cancellationSource = new RoutineCancellationSource();
			simpleRoutine.routine = CoreRoutine<TResult>.StartResult(monoBehaviour, CancelWrapper(coroutine, simpleRoutine.cancellationSource.Token), simpleRoutine.cancellationSource.Token);

			return simpleRoutine;
		}

		/// <summary>
		/// Starts a <see cref="SimpleRoutine{TResult}"/> that encapsulates a <see cref="CoreRoutine"/> and a 
		/// <see cref="RoutineCancellationSource"/> for cancelation. The token will be passed to the
		/// coroutine with the caller responsible for handling cancelation.
		/// </summary>
		/// <param name="simpleRoutine">The <see cref="SimpleRoutine{TResult}"/> to store the created routine. If the routine is not null and is running it will be canceled.</param>
		/// <param name="monoBehaviour">The <see cref="MonoBehaviour"/> that will host the internal wrapping <see cref="Coroutine"/>.</param>
		/// <param name="coroutine">The <see cref="Coroutine"/> to be wrapped.</param>
		public static SimpleRoutine<TResult> StartResult(ref SimpleRoutine<TResult> simpleRoutine, MonoBehaviour monoBehaviour, Func<RoutineCancellationToken, IEnumerator> coroutine)
		{
			Assert.IsNotNull(monoBehaviour);
			Assert.IsNotNull(coroutine);

			if (simpleRoutine != null)
			{
				simpleRoutine.Cancel();
			}

			simpleRoutine = new SimpleRoutine<TResult>();

			simpleRoutine.cancellationSource = new RoutineCancellationSource();
			simpleRoutine.routine = CoreRoutine<TResult>.StartResult(monoBehaviour, coroutine(simpleRoutine.cancellationSource.Token), simpleRoutine.cancellationSource.Token);

			return simpleRoutine;
		}
	}
}
