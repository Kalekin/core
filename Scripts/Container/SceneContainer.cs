using System.Collections.Generic;
using System.Linq;
using Core.Container.Internal;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;


namespace Core.Container
{
#if UNITY_EDITOR
	[ExecuteInEditMode, ScriptExecutionOrder(-2000)]
#endif
	public class SceneContainer : MonoBehaviour
	{
		[SerializeField] private MonoBehaviour[] m_ContainerBehaviours = default(MonoBehaviour[]);
		[SerializeField] private Component[] m_LinkedBehaviours = default(Component[]);




		private void Awake()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
			{
				return;
			}
#endif
			Container container = Container.Get();
			var containerInternal = (IContainerInternal) container;

			for (int i = 0; i < m_LinkedBehaviours.Length; i++)
			{
				if (m_LinkedBehaviours[i] == null)
				{
					Debug.Logger.LogWarning(
						"SceneContainer contains a null ContainerBehaviourLink entry. Check that this behaviour isn't being destroyed during initialization.", this);
					continue;
				}

				containerInternal.Register(m_LinkedBehaviours[i]);
			}

			for (int i = 0; i < m_ContainerBehaviours.Length; i++)
			{
				if (m_ContainerBehaviours[i] == null)
				{
					Debug.Logger.LogWarning(
						"SceneContainer contains a null IContainerBehaviour entry. Check that this behaviour isn't being destroyed during initialization.", this);
					continue;
				}

				containerInternal.Register(m_ContainerBehaviours[i]);
			}

			for (int i = 0; i < m_ContainerBehaviours.Length; i++)
			{
				if (m_ContainerBehaviours[i] == null)
				{
					continue;
				}

				((IContainerBehaviour)m_ContainerBehaviours[i]).OnConstruct(container);
			}

			for (int i = 0; i < m_ContainerBehaviours.Length; i++)
			{
				if (m_ContainerBehaviours[i] == null)
				{
					continue;
				}

				((IContainerBehaviour)m_ContainerBehaviours[i]).OnLinked();
			}

			Destroy(gameObject);
		}


		 

#if UNITY_EDITOR
		private void Update()
		{
			if (Application.isPlaying)
			{
				return;
			}

			RebuildCollection();
		}



		[ContextMenu("Rebuild")]
		public void RebuildCollection()
		{
			var allLoadedBehaviours = Resources.FindObjectsOfTypeAll<MonoBehaviour>();
			var sceneBehaviours = new List<MonoBehaviour>();
			var linkedBehaviours = new List<Component>();

			for (int i = 0; i < allLoadedBehaviours.Length; i++)
			{
				var behaviour = allLoadedBehaviours[i];

				string assetPath = AssetDatabase.GetAssetPath(behaviour);

				if (string.IsNullOrEmpty(assetPath))
				{
					if (behaviour is IContainerBehaviour)
					{
						sceneBehaviours.Add(behaviour);
					}
					else
					{
						var behaviourLink = behaviour as ContainerBehaviourLink;
						if (behaviourLink != null)
						{
							var link = behaviourLink;

							if (link.Links == null ||
							    link.Links.Length == 0)
							{
								continue;
							}

							linkedBehaviours.AddRange(behaviourLink.Links.Where(l => l != null));
						}
					}
				}
			}

			m_ContainerBehaviours = sceneBehaviours.ToArray();
			m_LinkedBehaviours = linkedBehaviours.ToArray();
		}
#endif
	}
}
