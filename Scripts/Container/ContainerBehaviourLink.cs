﻿using System.Linq;
using UnityEngine;


namespace Core.Container
{
	public class ContainerBehaviourLink : MonoBehaviour
	{
		[SerializeField] private Component[] m_Links = default(Component[]);




		public Component[] Links
		{
			get { return m_Links; }
		}



#if UNITY_EDITOR
		private void OnValidate()
		{
			for (int i = 0; i < m_Links.Length; i++)
			{
				Component monoBehaviour = m_Links[i];
				if (monoBehaviour is IContainerBehaviour)
				{
					UnityEngine.Debug.LogError(
						string.Format(
							"Removing MonoBehaviour {0} from Links array. This behaviour already inherits from IContainerBehaviour and will be automatically tracked for locating by the SceneContainer.",
							monoBehaviour), this);

					m_Links = m_Links.Where(m => !(m is IContainerBehaviour)).ToArray();
					return;
				}
			}
		}
#endif
	}
}
