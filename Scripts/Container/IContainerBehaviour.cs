namespace Core.Container
{
	public interface IContainerBehaviour
	{
		void OnConstruct(Container container);


		void OnLinked();
	}
}