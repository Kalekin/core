﻿using UnityEngine;


namespace Core.Container.Internal
{
	public interface IContainerInternal
	{
		void Register(object monoBehaviour);
	}
}