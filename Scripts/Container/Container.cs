﻿using System;
using System.Collections.Generic;
using Core.Container.Internal;
using Core.Debug;
using Core.Extensions;
using UnityEngine;


namespace Core.Container
{
	public class Container : MonoBehaviour, IContainerInternal
	{
		public const string TAG = "Container";
		private const int _MAP_START_SIZE = 201;
		private const int _DEFAULT_CONTAINER_SIZE = 4;
		private static bool _Initialized;
		private TypeCollection[] _map;




		public static Container Get()
		{
			GameObject containerObject = GameObject.FindWithTag(TAG);
			Container ret;

			if (containerObject == null)
			{

				Assert.IsFalse(_Initialized, "Failed to locate the container after it has been initialized. Has the GameObject the container behaviour was sitting on been destroyed?");
				containerObject = new GameObject("Container") { tag = TAG };
				DontDestroyOnLoad(containerObject);
				ret = containerObject.AddComponent<Container>();
				ret._map = new TypeCollection[_MAP_START_SIZE];
			}
			else
			{
				ret = containerObject.GetComponent<Container>();

				if (ret == null)
				{
					Assert.IsFalse(_Initialized, "Failed to locate the container after it has been initialized. Has the GameObject the container behaviour was sitting on been destroyed?");
					ret = containerObject.AddComponent<Container>();
					ret._map = new TypeCollection[_MAP_START_SIZE];
				}
			}

			_Initialized = true;
			return ret;
		}


		public void Locate<T>(out T behaviour)
			where T : class
		{
			behaviour = Locate<T>();
		}


		public T Locate<T>()
			where T : class
		{
			int index = GetIndexForTypeOrNull(typeof(T));

			if (_map[index].Type == null)
			{
				return null;
			}

			List<object> collection = _map[index].Collection;

			for (int i = collection.Count - 1; i >= 0; i--)
			{
				object o = collection[i];

				if (o == null)
				{
					collection.RemoveAtUnsorted(i);
					i--;
					continue;
				}

				return (T)o;
			}

			return null;
		}


		public void LocateWithTag<T>(out T behaviour, string t)
			where T : class
		{
			behaviour = LocateWithTag<T>(t);
		}


		public T LocateWithTag<T>(string t)
			where T : class
		{
			Assert.IsFalse(string.IsNullOrEmpty(t), "An object tag cannot be null or empty.");
			Assert.IsTrue(typeof(Component).IsAssignableFrom(typeof(T)) ||
						  typeof(T).IsInterface, "Locating objects with tags is restricted to MonoBehaviours or inerfaces");


			int index = GetIndexForTypeOrNull(typeof(T));

			if (_map[index].Type == null)
			{
				return null;
			}

			List<object> collection = _map[index].Collection;

			for (int i = collection.Count - 1; i >= 0; i--)
			{
				object o = collection[i];

				if (o == null)
				{
					collection.RemoveAtUnsorted(i);
					i--;
					continue;
				}

				var c = o as Component;

				if (c == null ||
				    !c.CompareTag(t))
				{
					continue;
				}

				return (T)o;
			}

			return null;
		}


		public void LocateAll<T>(List<T> outList)
			where T : class
		{
			Assert.IsNotNull(outList, "Provided argument \"outList\" cannot be null.");

			int index = GetIndexForTypeOrNull(typeof(T));

			if (_map[index].Type == null)
			{
				return;
			}

			List<object> collection = _map[index].Collection;

			for (int i = collection.Count - 1; i >= 0; i--)
			{
				object o = collection[i];

				if (o == null)
				{
					collection.RemoveAtUnsorted(i);
					continue;
				}

				outList.Add((T)o);
			}
		}


		public void LocateAllWithTag<T>(List<T> outList, string t)
			where T : class
		{
			Assert.IsNotNull(outList, "Provided argument \"outList\" cannot be null.");
			Assert.IsFalse(string.IsNullOrEmpty(t), "An object tag cannot be null or empty.");
			Assert.IsTrue(typeof(Component).IsAssignableFrom(typeof(T)) ||
						  typeof(T).IsInterface, "Locating objects with tags is restricted to MonoBehaviours or inerfaces");

			int index = GetIndexForTypeOrNull(typeof(T));

			if (_map[index].Type == null)
			{
				return;
			}


			List<object> collection = _map[index].Collection;

			for (int i = collection.Count - 1; i >= 0; i--)
			{
				object o = collection[i];

				if (o == null)
				{
					collection.RemoveAtUnsorted(i);
					continue;
				}

				var c = o as Component;

				if (c == null ||
					!c.CompareTag(t))
				{
					continue;
				}

				outList.Add((T)o);
			}
		}


		void IContainerInternal.Register(object behaviour)
		{
			Assert.IsNotNull(behaviour, "Attempting to register a null behaviour. Was this behaviour destroyed during initialization?");

			Type type = behaviour.GetType();
			Type[] interfaces = type.GetInterfaces();

			for (int i = 0; i < interfaces.Length; i++)
			{
				var inter = interfaces[i];
				RegisterBehaviourForType(behaviour, inter);
			}

			while (type != null &&
				   type != typeof (object))
			{
				RegisterBehaviourForType(behaviour, type);
				type = type.BaseType;
			}

			RegisterBehaviourForType(behaviour, type);
		}


		private void RegisterBehaviourForType(object behaviour, Type type)
		{
			int index = GetIndexForTypeOrNull(type);

			if (_map[index].Type != null &&
				_map[index].Type != type)
			{
				GrowMap();
				index = GetIndexForTypeOrNull(type);
			}

			if (_map[index].Type == null)
			{
				_map[index] = new TypeCollection
				{
					Type = type,
					Collection = new List<object>(_DEFAULT_CONTAINER_SIZE)
				};
			}

			_map[index].Collection.Add(behaviour);
		}


		private void GrowMap()
		{
			int size = _map.Length*2;

			Debug.Logger.LogWarning(
				string.Format(
					"Growing Container map size to {0}. Consider initializaing the container to this size to prevent runtime resizes.",
					size));

			TypeCollection[] temp = _map;
			_map = new TypeCollection[size];

			for (int i = 0; i < temp.Length; i++)
			{
				int index = GetIndexForTypeOrNull(temp[i].Type);
				_map[index] = temp[i];
			}
		}


		private int GetIndexForTypeOrNull(Type type)
		{
			int hash = type.GetHashCode();
			int index = (hash % _map.Length + _map.Length) % _map.Length;
			int typesChecked = 0;

			while (typesChecked < _map.Length &&
				   _map[index].Type != null &&
				   _map[index].Type != type)
			{
				typesChecked++;
				index++;

				if (index >= _map.Length)
				{
					index = 0;
				}
			}

			if (typesChecked > 0)
			{
				Debug.Logger.LogWarning(string.Format("{0} map collisions indexing type {1}", typesChecked, type));
			}

			return index;
		}


		

		private struct TypeCollection
		{
			public Type Type;
			public List<object> Collection;
		}
	}
}
