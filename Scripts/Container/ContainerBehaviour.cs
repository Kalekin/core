﻿using UnityEngine;


namespace Core.Container
{
	public class ContainerBehaviour : MonoBehaviour, IContainerBehaviour
	{
		void IContainerBehaviour.OnConstruct(Container container)
		{
			OnConstruct(container);
		}


		void IContainerBehaviour.OnLinked()
		{
			OnLinked();
		}


		protected virtual void OnConstruct(Container container)
		{
			
		}


		protected virtual void OnLinked()
		{
			
		}
	}
}
