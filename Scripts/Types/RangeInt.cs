﻿using System;
using Core.Debug;
using Core.Serialization;
using UnityEngine;


namespace Core.Types
{
	[Serializable]
	public struct RangeInt : IJsonSerializable
	{
		[SerializeField]
		private int m_Minimum;
		[SerializeField]
		private int m_Maximum;




		public int Minimum
		{
			get
			{
				return m_Minimum;
			}
			set
			{
				m_Minimum = value;

				if (Maximum < Minimum)
				{
					Maximum = Minimum;
				}
			}
		}


		public int Maximum
		{
			get
			{
				return m_Maximum;
			}
			set
			{
				m_Maximum = value;

				if (Minimum > Maximum)
				{
					Minimum = Maximum;
				}
			}
		}


		public int Length
		{
			get
			{
				return Maximum - Minimum;
			}
		}




		public RangeInt(int minimum, int maximum)
		{
			m_Minimum = minimum;
			m_Maximum = maximum;
		}




		public bool Contains(int value)
		{
			return value >= Minimum && value < Maximum;
		}


		public int Repeat(int value)
		{

			return (int)Mathf.Repeat(value - Minimum, Length) + Minimum;
		}


		public int Clamp(int value)
		{
			return Mathf.Clamp(value, Minimum, Maximum);
		}


		public int PingPong(int value)
		{
			return (int)Mathf.PingPong(value - Minimum, Length) + Minimum;
		}


		public override string ToString()
		{
			return "Min: " + Minimum + ", Max: " + Maximum;
		}


		void IJsonSerializable.FromJsonElement(JsonElement element)
		{
			Assert.IsNotNull(element);

			JsonObject obj = element.AsJsonObject();
			Minimum = obj["min"].AsInt();
			Maximum = obj["max"].AsInt();
		}


		JsonElement IJsonSerializable.ToJsonElement(JsonPool pool)
		{
			Assert.IsNotNull(pool);

			JsonObject ret = pool.GetObject();
			JsonPrimitive min = pool.GetPrimitive();
			JsonPrimitive max = pool.GetPrimitive();

			min.SetValue(Minimum);
			max.SetValue(Maximum);

			ret.Add("min", min);
			ret.Add("max", max);

			return ret;
		}
	}

}