using System;
using Core.Debug;
using Core.Serialization;
using UnityEngine;


namespace Core.Types
{
	[Serializable]
	public struct PointInt : IEquatable<PointInt>, IJsonSerializable
	{
		public static readonly PointInt Zero = new PointInt(0, 0);
		public static readonly PointInt Up = new PointInt(0, 1);
		public static readonly PointInt Down = new PointInt(0, -1);
		public static readonly PointInt Left = new PointInt(-1, 0);
		public static readonly PointInt Right = new PointInt(1, 0);
		[SerializeField]
		private int m_X;
		[SerializeField]
		private int m_Y;




		public int X
		{
			get { return m_X; }
			set { m_X = value; }
		}


		public int Y
		{
			get { return m_Y; }
			set { m_Y = value; }
		}




		public PointInt(int x, int y)
		{
			m_X = x;
			m_Y = y;
		}




		public static PointInt operator +(PointInt a, PointInt b)
		{
			return new PointInt(a.m_X + b.m_X, a.m_Y + b.m_Y);
		}


		public static PointInt operator -(PointInt a, PointInt b)
		{
			return new PointInt(a.m_X - b.m_X, a.m_Y - b.m_Y);
		}


		public static PointInt operator *(PointInt a, int i)
		{
			return new PointInt(a.m_X * i, a.m_Y * i);
		}


		public static PointInt operator /(PointInt a, int i)
		{
			return new PointInt(a.m_X / i, a.m_Y / i);
		}


		public static PointInt operator *(int i, PointInt a)
		{
			return new PointInt(a.m_X * i, a.m_Y * i);
		}




		public static PointInt Max(PointInt a, PointInt b)
		{
			return new PointInt(Mathf.Max(a.m_X, b.m_X), Math.Max(a.m_Y, b.m_Y));
		}


		public static PointInt Min(PointInt a, PointInt b)
		{
			return new PointInt(Mathf.Min(a.m_X, b.m_X), Math.Min(a.m_Y, b.m_Y));
		}


		public override string ToString()
		{
			return string.Format("x: {0}, y: {1}", m_X, m_Y);
		}


		public bool Equals(PointInt other)
		{
			return m_X == other.m_X && m_Y == other.m_Y;
		}


		public override int GetHashCode()
		{
			unchecked
			{
				return (m_X.GetHashCode() * 397) ^ m_Y.GetHashCode();
			}
		}


		void IJsonSerializable.FromJsonElement(JsonElement element)
		{
			Assert.IsNotNull(element);

			JsonObject obj = element.AsJsonObject();

			m_X = obj["x"].AsInt();
			m_Y = obj["y"].AsInt();
		}


		JsonElement IJsonSerializable.ToJsonElement(JsonPool pool)
		{
			Assert.IsNotNull(pool);

			JsonObject ret = pool.GetObject();
			JsonPrimitive x = pool.GetPrimitive();
			JsonPrimitive y = pool.GetPrimitive();

			x.SetValue(m_X);
			y.SetValue(m_Y);

			ret.Add("x", x);
			ret.Add("y", y);

			return ret;
		}
	}
}