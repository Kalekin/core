using System;
using Core.Debug;
using Core.Serialization;
using UnityEngine;


namespace Core.Types
{
	[Serializable]
	public struct Point : IEquatable<Point>, IJsonSerializable
	{
		public static readonly Point Zero = new Point(0f, 0f);
		public static readonly Point Up = new Point(0f, 1f);
		public static readonly Point Down = new Point(0f, -1f);
		public static readonly Point Left = new Point(-1f, 0f);
		public static readonly Point Right = new Point(1f, 0f);
		[SerializeField] private float m_X;
		[SerializeField] private float m_Y;




		public float X
		{
			get { return m_X; }
			set { m_X = value; }
		}


		public float Y
		{
			get { return m_Y; }
			set { m_Y = value; }
		}


		public float Magnitude
		{
			get { return Mathf.Sqrt(m_X*m_X + m_Y*m_Y); }
		}


		public float SqrMagnitude
		{
			get { return m_X*m_X + m_Y*m_Y; }
		}




		public Point(float x, float y)
		{
			m_X = x;
			m_Y = y;
		}




		public static implicit operator Vector3(Point point)
		{
			return new Vector3(point.X, 0f, point.Y);
		}


		public static implicit operator Point(Vector3 point)
		{
			return new Point(point.x, point.z);
		}


		public static Point operator +(Point a, Point b)
		{
			return new Point(a.m_X + b.m_X, a.m_Y + b.m_Y);
		}


		public static Point operator -(Point a, Point b)
		{
			return new Point(a.m_X - b.m_X, a.m_Y - b.m_Y);
		}


		public static Point operator -(Point p)
		{
			return new Point(-p.m_X, -p.m_Y);
		}


		public static Point operator +(Point a, Vector3 b)
		{
			return new Point(a.m_X + b.x, a.m_Y + b.z);
		}


		public static Point operator -(Point a, Vector3 b)
		{
			return new Point(a.m_X - b.x, a.m_Y - b.z);
		}


		public static Point operator +(Vector3 a, Point b)
		{
			return new Point(a.x + b.m_X, a.z+ b.m_Y);
		}


		public static Point operator -(Vector3 a, Point b)
		{
			return new Point(a.x - b.m_X, a.z - b.m_Y);
		}


		public static Point operator *(Point a, float f)
		{
			return new Point(a.m_X*f, a.m_Y*f);
		}


		public static Point operator /(Point a, float f)
		{
			return new Point(a.m_X/f, a.m_Y/f);
		}


		public static Point operator *(float f, Point a)
		{
			return new Point(a.m_X*f, a.m_Y*f);
		}


		public static Point operator /(float f, Point a)
		{
			return new Point(a.m_X/f, a.m_Y/f);
		}




		public void Normalize()
		{
			float magnitude = Magnitude;
			m_X /= magnitude;
			m_Y /= magnitude;
		}


		public Point Normalized()
		{
			Point ret = this;
			ret.Normalize();
			return ret;
		}


		public static Point Lerp(Point a, Point b, float t)
		{
			return new Point(Mathf.Lerp(a.m_X, b.m_X, t), Mathf.Lerp(a.m_Y, b.m_Y, t));
		}


		public static Point Max(Point a, Point b)
		{
			return new Point(Mathf.Max(a.m_X, b.m_X), Math.Max(a.m_Y, b.m_Y));
		}


		public static Point Min(Point a, Point b)
		{
			return new Point(Mathf.Min(a.m_X, b.m_X), Math.Min(a.m_Y, b.m_Y));
		}


		public static Point Normalize(Point value)
		{
			value.Normalize();
			return value;
		}


		public static Point MoveTowards(Point current, Point target, float maxDelta)
		{
			Point delta = target - current;

			if (delta.Magnitude <= maxDelta)
			{
				return target;
			}

			return current + delta.Normalized() * maxDelta;
		}


		public static float Distance(Point a, Point b)
		{
			return (b - a).Magnitude;
		}


		public override string ToString()
		{
			return string.Format("x: {0}, y: {1}", m_X, m_Y);
		}


		public bool Equals(Point other)
		{
			return m_X == other.m_X && m_Y == other.m_Y;
		}


		public override int GetHashCode()
		{
			unchecked
			{
				return (m_X.GetHashCode() * 397) ^ m_Y.GetHashCode();
			}
		}


		void IJsonSerializable.FromJsonElement(JsonElement element)
		{
			Assert.IsNotNull(element);

			JsonObject obj = element.AsJsonObject();

			m_X = obj["x"].AsFloat();
			m_Y = obj["y"].AsFloat();
		}


		JsonElement IJsonSerializable.ToJsonElement(JsonPool pool)
		{
			Assert.IsNotNull(pool);

			JsonObject ret = pool.GetObject();
			JsonPrimitive x = pool.GetPrimitive();
			JsonPrimitive y = pool.GetPrimitive();

			x.SetValue(m_X);
			y.SetValue(m_Y);

			ret.Add("x", x);
			ret.Add("y", y);

			return ret;
		}
	}
}