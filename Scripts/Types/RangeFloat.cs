﻿using System;
using Core.Debug;
using Core.Serialization;
using UnityEngine;


namespace Core.Types
{
	[Serializable]
	public struct RangeFloat : IJsonSerializable
	{
		[SerializeField] private float m_Minimum;
		[SerializeField] private float m_Maximum;




		public float Minimum
		{
			get { return m_Minimum; }
			set
			{
				m_Minimum = value;

				if (Maximum < Minimum)
				{
					Maximum = Minimum;
				}
			}
		}


		public float Maximum
		{
			get { return m_Maximum; }
			set
			{
				m_Maximum = value;

				if (Minimum > Maximum)
				{
					Minimum = Maximum;
				}
			}
		}


		public float Length
		{
			get { return Maximum - Minimum; }
		}




		public RangeFloat(float minimum, float maximum)
		{
			m_Minimum = minimum;
			m_Maximum = maximum;
		}




		public bool Contains(float value)
		{
			return value >= Minimum && value <= Maximum;
		}


		public float Repeat(float value)
		{
			return Mathf.Repeat(value - Minimum, Length) + Minimum;
		}


		public float Clamp(float value)
		{
			return Mathf.Clamp(value, Minimum, Maximum);
		}


		public float PingPong(float value)
		{
			return Mathf.PingPong(value - Minimum, Length) + Minimum;
		}


		public float ValueAtTime(float time)
		{
			return Mathf.Lerp(Minimum, Maximum, time);
		}


		public float ValueAtStep(int step, int stepCount)
		{
			if (stepCount <= 1)
			{
				return ValueAtTime(0.5f);
			}

			step = Mathf.Clamp(step, 0, stepCount);

			return ValueAtTime((float) step/(stepCount - 1));
		}


		public override string ToString()
		{
			return "Min: " + Minimum + ", Max: " + Maximum;
		}


		void IJsonSerializable.FromJsonElement(JsonElement element)
		{
			Assert.IsNotNull(element);

			JsonObject obj = element.AsJsonObject();
			Minimum = obj["min"].AsFloat();
			Maximum = obj["max"].AsFloat();
		}


		JsonElement IJsonSerializable.ToJsonElement(JsonPool pool)
		{
			Assert.IsNotNull(pool);

			JsonObject ret = pool.GetObject();
			JsonPrimitive min = pool.GetPrimitive();
			JsonPrimitive max = pool.GetPrimitive();

			min.SetValue(Minimum);
			max.SetValue(Maximum);

			ret.Add("min", min);
			ret.Add("max", max);

			return ret;
		}
	}
}