using System;
using Core.Debug;
using Core.Serialization;
using UnityEngine;
using Random = UnityEngine.Random;


namespace Core.Types
{
	[Serializable]
	public class RetroRandom : IJsonSerializable
    {
        [SerializeField] private int m_Seed;




        public int Seed
        {
            get
            {
                return m_Seed;
            }
            set
            {
                m_Seed = value;
            }
        }


        public float Value
        {
            get
            {
                int temp = Random.seed;
                Random.seed = m_Seed;

                float ret = Random.value;

                m_Seed = Random.seed;
                Random.seed = temp;

                return ret;
            }
        }


		public Vector2 OnUnitCircle
		{
			get
			{
				return PointOnCircle(1f);
			}
		}


		public Vector3 OnUnitSphere
		{
			get
			{
				return PointOnSphere(1f);
			}
		}


		public Vector2 InUnitCircle
		{
			get
			{
				return PointInCircle(0f, 1f);
			}
		}


		public Vector3 InUnitSphere
		{
			get
			{
				return PointInSphere(0f, 1f);
			}
		}




        public RetroRandom()
            : this(Random.Range(int.MinValue, int.MaxValue))
        {
        }


        public RetroRandom(int seed)
        {
            m_Seed = seed;
        }




		public int Range(RangeInt range)
		{
			return Range(range.Minimum, range.Maximum);
		}


        public int Range(int min, int max)
        {
            int temp = Random.seed;
            Random.seed = m_Seed;

            int ret = Random.Range(min, max);

            m_Seed = Random.seed;
            Random.seed = temp;

            return ret;
        }


		public float Range(RangeFloat range)
		{
			return Range(range.Minimum, range.Maximum);
		}


        public float Range(float min, float max)
        {
            int temp = Random.seed;
            Random.seed = m_Seed;

            float ret = Random.Range(min, max);

            m_Seed = Random.seed;
            Random.seed = temp;

            return ret;
        }


		public Vector2 PointInCircle(RangeFloat range)
		{
			return PointInCircle(range.Minimum, range.Maximum);
		}


        public Vector2 PointInCircle(float inner, float outer)
        {
            int temp = Random.seed;
            Random.seed = m_Seed;

            float radius = Random.Range(inner, outer);
            float angleRadians = Random.Range(0, Mathf.PI * 2);

            m_Seed = Random.seed;
            Random.seed = temp;

            return new Vector2(radius * Mathf.Cos(angleRadians), radius * Mathf.Sin(angleRadians));
        }
		
		
		public Vector2 PointOnCircle(float radius)
		{
			int temp = Random.seed;
			Random.seed = m_Seed;

			float angleRadians = Random.Range(0, Mathf.PI * 2);

			m_Seed = Random.seed;
			Random.seed = temp;

			return new Vector2(radius * Mathf.Cos(angleRadians), radius * Mathf.Sin(angleRadians));
		}


		public Vector3 PointInSphere(RangeFloat range)
		{
			return PointInSphere(range.Minimum, range.Maximum);
		}


        public Vector3 PointInSphere(float inner, float outer)
        {
            int temp = Random.seed;
            Random.seed = m_Seed;

            float radius = Random.Range(inner, outer);
            Quaternion rotation = Random.rotation;

            m_Seed = Random.seed;
            Random.seed = temp;

            return (rotation * Vector3.one) * radius;
        }


		public Vector3 PointOnSphere(float radius)
		{
			int temp = Random.seed;
			Random.seed = m_Seed;

			Quaternion rotation = Random.rotation;

			m_Seed = Random.seed;
			Random.seed = temp;

			return (rotation * Vector3.one) * radius;
		}


        public override string ToString()
        {
            return "Seed: " + m_Seed;
        }


		void IJsonSerializable.FromJsonElement(JsonElement element)
		{
			Assert.IsNotNull(element);

			m_Seed = element.AsInt();
		}


		JsonElement IJsonSerializable.ToJsonElement(JsonPool pool)
		{
			Assert.IsNotNull(pool);

			JsonPrimitive ret = pool.GetPrimitive();
			ret.SetValue(m_Seed);

			return ret;
		}
    }   
}
