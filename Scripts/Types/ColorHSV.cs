﻿using System;
using Core.Debug;
using Core.Serialization;
using UnityEngine;


namespace Core.Types
{
	[Serializable]
	public struct ColorHSV : IJsonSerializable
	{
		[SerializeField] private float m_H;
		[SerializeField] private float m_S;
		[SerializeField] private float m_V;
		[SerializeField] private float m_A;




		public float H
		{
			get
			{
				return m_H;
			}
			set
			{
				m_H = Mathf.Repeat(value, 360f);
			}
		}


		public float S
		{
			get
			{
				return m_S;
			}
			set
			{
				m_S = Mathf.Clamp01(value);
			}
		}


		public float V
		{
			get
			{
				return m_V;
			}
			set
			{
				m_V = Mathf.Clamp01(value);
			}
		}


		public float A
		{
			get
			{
				return m_A;
			}
			set
			{
				m_A = Mathf.Clamp01(value);
			}
		}


		public float this[int index]
		{
			get
			{
				switch (index)
				{
					case 0:
						return m_H;
					case 1:
						return m_S;
					case 2:
						return m_V;
					case 3:
						return m_A;
					default:
						throw new ArgumentOutOfRangeException("index");
				}
			}
		}




		public ColorHSV(float h, float s, float v)
			: this(h, s, v, 1f)
		{
		}


		public ColorHSV(float h, float s, float v, float a)
		{
			m_H = Mathf.Repeat(h, 360f);
			m_S = Mathf.Clamp01(s);
			m_V = Mathf.Clamp01(v);
			m_A = Mathf.Clamp01(a);
		}




		public static implicit operator ColorHSV(Color color)
		{
			var ret = new ColorHSV();
			ret.FromColor(color);
			return ret;
		}


		public static implicit operator Color(ColorHSV color)
		{
			return color.ToColor();
		}


		public static implicit operator ColorHSV(Vector4 vector)
		{
			return new ColorHSV(vector.x, vector.y, vector.z, vector.w);
		}


		public static implicit operator Vector4(ColorHSV color)
		{
			return new Vector4(color.H, color.S, color.V, color.A);
		}




		public void FromColor(Color color)
		{
			float min = Mathf.Min(Mathf.Min(color.r, color.g), color.b);
			float max = Mathf.Max(Mathf.Max(color.r, color.g), color.b);
			float delta = max - min;
			float v;
			float s;
			float h;

			v = max;

			if (Mathf.Approximately(max, 0) ||
				Mathf.Approximately(min, max))
			{
				H = -1;
				S = 0;
				V = v;
				A = color.a;
				return;
			}

			s = delta / max;

			if (color.r == max)
			{
				h = (color.g - color.b) / delta;
			}
			else if (color.g == max)
			{
				h = 2 + (color.b - color.r) / delta;
			}
			else
			{
				h = 4 + (color.r - color.g) / delta;
			}

			h *= 60;

			H = h;
			S = s;
			V = v;
			A = color.a;
		}


		public Color ToColor()
		{
			if (S == 0)
			{
				return new Color(V, V, V, A);
			}

			float sector = H / 60;
			int i = (int)sector;
			float f = sector - i;
			var color = new Color(0, 0, 0, A);

			switch (i)
			{
				case 0:
					color.r = V;
					color.g = V * (1 - S * (1 - f));
					color.b = V * (1 - S);
					break;

				case 1:
					color.r = V * (1 - S * f);
					color.g = V;
					color.b = V * (1 - S);
					break;

				case 2:
					color.r = V * (1 - S);
					color.g = V;
					color.b = V * (1 - S * (1 - f));
					break;

				case 3:
					color.r = V * (1 - S);
					color.g = V * (1 - S * f);
					color.b = V;
					break;

				case 4:
					color.r = V * (1 - S * (1 - f));
					color.g = V * (1 - S);
					color.b = V;
					break;

				default:
					color.r = V;
					color.g = V * (1 - S);
					color.b = V * (1 - S * f);
					break;
			}

			return color;
		}


		void IJsonSerializable.FromJsonElement(JsonElement element)
		{
			Assert.IsNotNull(element);
			Assert.IsTrue(element.IsJsonObject);

			JsonObject obj = element.AsJsonObject();
			m_H = obj["h"].AsFloat();
			m_S = obj["s"].AsFloat();
			m_V = obj["v"].AsFloat();
			m_A = obj["a"].AsFloat();;
		}


		JsonElement IJsonSerializable.ToJsonElement(JsonPool pool)
		{
			JsonObject obj = pool.GetObject();
			JsonPrimitive h = pool.GetPrimitive();
			JsonPrimitive s = pool.GetPrimitive();
			JsonPrimitive v = pool.GetPrimitive();
			JsonPrimitive a = pool.GetPrimitive();

			h.SetValue(m_H);
			s.SetValue(m_S);
			v.SetValue(m_V);
			a.SetValue(m_A);

			obj["h"] = h;
			obj["s"] = s;
			obj["v"] = v;
			obj["a"] = a;

			return obj;
		}
	}
}
