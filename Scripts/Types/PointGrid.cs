﻿using System;
using Core.Debug;
using Core.Serialization;
using UnityEngine;


namespace Core.Types
{
	[Serializable]
	public struct PointGrid : IJsonSerializable
	{
		[SerializeField] private float m_CellWidth;
		[SerializeField] private float m_CellHeight;
		[SerializeField] private Point m_Origin;
		private float _halfCellWidth;
		private float _halfCellHeight;




		public float CellWidth
		{
			get { return m_CellWidth; }
			set
			{
				m_CellWidth = value;
				_halfCellWidth = m_CellWidth*0.5f;
			}
		}


		public float CellHeight
		{
			get { return m_CellHeight; }
			set
			{
				m_CellHeight = value;
				_halfCellHeight = m_CellHeight*0.5f;
			}
		}


		public Point Origin
		{
			get { return m_Origin; }
			set { m_Origin = value; }
		}




		public PointGrid(float cellWidth, float cellHeight)
		{
			m_CellWidth = cellWidth;
			m_CellHeight = cellHeight;
			m_Origin = Point.Zero;
			_halfCellWidth = cellWidth*0.5f;
			_halfCellHeight = cellHeight*0.5f;
		}


		public PointGrid(float cellWidth, float cellHeight, Point origin)
		{
			m_CellWidth = cellWidth;
			m_CellHeight = cellHeight;
			m_Origin = origin;
			_halfCellWidth = cellWidth * 0.5f;
			_halfCellHeight = cellHeight * 0.5f;
		}




		private void OnValidate()
		{
			CellWidth = CellWidth;
			CellHeight = CellHeight;
		}




		public PointRect GetCell(int x, int y)
		{
			float xOffset = x*m_CellWidth;
			float yOffset = y*m_CellHeight;

			return new PointRect(-_halfCellWidth + xOffset, -_halfCellHeight + yOffset, m_CellWidth, m_CellHeight);
		}


		public PointRect GetRect(int startX, int startY, int width, int height)
		{
			Assert.IsTrue(width > 0);
			Assert.IsTrue(height > 0);

			float xOffset = startX * m_CellWidth;
			float yOffset = startY * m_CellHeight;

			return new PointRect(-_halfCellWidth + xOffset, -_halfCellHeight + yOffset, m_CellWidth*width, m_CellHeight*height);
		}


		public void GetCells(int startX, int startY, PointRect[,] outGrid)
		{
			Assert.IsNotNull(outGrid);

			int endX = startX + outGrid.GetLength(0);
			int endY = startY + outGrid.GetLength(1);

			for (int x = startX; x < endX; x++)
			{
				for (int y = startY; y < endY; y++)
				{
					float xOffset = x*m_CellWidth;
					float yOffset = y*m_CellHeight;

					outGrid[x, y] = new PointRect(-_halfCellWidth + xOffset, -_halfCellHeight + yOffset, m_CellWidth, m_CellHeight);
				}
			}
		}


		public void GetCellsFast(int startX, int startY, PointRect[,] outGrid)
		{
			Assert.IsNotNull(outGrid);

			int endX = startX + outGrid.GetLength(0);
			int endY = startY + outGrid.GetLength(1);
			float xOffset = startX * m_CellWidth - _halfCellWidth;
			float yOffset = startY * m_CellHeight - _halfCellHeight;

			for (int x = startX; x < endX; x++)
			{
				for (int y = startY; y < endY; y++)
				{
					outGrid[x, y] = new PointRect(xOffset, yOffset, m_CellWidth, m_CellHeight);

					xOffset += m_CellWidth;
					yOffset += m_CellHeight;
				}
			}
		}


		void IJsonSerializable.FromJsonElement(JsonElement element)
		{
			Assert.IsNotNull(element);

			JsonObject obj = element.AsJsonObject();

			Serialization.JsonUtility.ValueFromJsonElement(ref m_Origin, obj["o"]);
			CellWidth = obj["cw"].AsFloat();
			CellHeight = obj["ch"].AsFloat();
		}


		JsonElement IJsonSerializable.ToJsonElement(JsonPool pool)
		{
			Assert.IsNotNull(pool);

			JsonObject ret = pool.GetObject();
			JsonPrimitive cellWidth = pool.GetPrimitive();
			JsonPrimitive cellHeight = pool.GetPrimitive();

			cellWidth.SetValue(m_CellWidth);
			cellHeight.SetValue(m_CellHeight);

			ret.Add("o", Serialization.JsonUtility.ValueToJsonElement(ref m_Origin, pool));
			ret.Add("cw", cellWidth);
			ret.Add("ch", cellHeight);

			return ret;
		}
	}
}
