using System;
using Core.Debug;
using Core.Serialization;
using UnityEngine;


namespace Core.Types
{
	[Serializable]
	public struct PointRect : IJsonSerializable
	{
		[SerializeField] private Point m_Min;
		[SerializeField] private float m_Width;
		[SerializeField] private float m_Height;
		private Point _max;




		public Point Min
		{
			get { return m_Min; }
		}


		public Point Max
		{
			get { return _max; }
		}


		public float Left
		{
			get { return m_Min.X; }
			set
			{
				m_Min.X = value;
				_max.X = value + m_Width;
			}
		}


		public float Bottom
		{
			get { return m_Min.Y; }
			set
			{
				m_Min.Y = value;
				_max.Y = value + m_Height;
			}
		}


		public float Width
		{
			get { return m_Width; }
			set
			{
				m_Width = value;
				_max.X = m_Min.X + value;
			}
		}


		public float Height
		{
			get { return m_Height; }
			set
			{
				m_Height = value;
				_max.Y = m_Min.Y + value;
			}
		}




		public PointRect(float left, float bottom, float width, float height)
		{
			m_Min = new Point(left, bottom);
			m_Width = width;
			m_Height = height;
			_max = new Point(left + width, bottom + height);
		}




		private void OnValidate()
		{
			_max.X = m_Min.X + m_Width;
			_max.X = m_Min.Y + m_Height;
		}




		public bool Contains(Point point)
		{
			if (point.X < m_Min.X ||
			    point.Y < m_Min.Y ||
			    point.X > _max.X ||
			    point.Y > _max.Y)
			{
				return false;
			}

			return true;
		}


		public Point Constrain(Point point)
		{
			point.X = Mathf.Clamp(point.X, m_Min.X, _max.X);
			point.Y = Mathf.Clamp(point.Y, m_Min.Y, _max.Y);

			return point;
		}


		public override string ToString()
		{
			return string.Format("min: {0}, max: {1}", m_Min, _max);
		}


		void IJsonSerializable.FromJsonElement(JsonElement element)
		{
			Assert.IsNotNull(element);

			JsonObject obj = element.AsJsonObject();

			m_Min.X = obj["l"].AsFloat();
			m_Min.Y = obj["b"].AsFloat();
			m_Width = obj["w"].AsFloat();
			m_Height = obj["h"].AsFloat();
		}


		JsonElement IJsonSerializable.ToJsonElement(JsonPool pool)
		{
			Assert.IsNotNull(pool);

			JsonObject ret = pool.GetObject();
			JsonPrimitive left = pool.GetPrimitive();
			JsonPrimitive bottom = pool.GetPrimitive();
			JsonPrimitive width = pool.GetPrimitive();
			JsonPrimitive height = pool.GetPrimitive();

			left.SetValue(m_Min.X);
			bottom.SetValue(m_Min.Y);
			width.SetValue(m_Width);
			height.SetValue(m_Height);

			ret.Add("l", left);
			ret.Add("b", bottom);
			ret.Add("w", width);
			ret.Add("h", height);

			return ret;
		}
	}
}