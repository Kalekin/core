﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


namespace Core.UI
{
	public class EmptyGraphic : Graphic
	{
		[Obsolete("Use OnPopulateMesh instead.", true)]
		protected override void OnFillVBO(List<UIVertex> vbo)
		{
		}
	}
}
