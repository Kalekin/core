﻿using System;
using System.Collections.Generic;
using Core.Debug;
using UnityEngine;
using UnityEngine.UI;


namespace Core.UI
{
	public class Line : MaskableGraphic
	{
		private const float _EPSILON = 0.0001f;
		[SerializeField] private float m_Width = 5f;
		[SerializeField] private Sprite m_Sprite = default(Sprite);
		[SerializeField] private float m_Tiling = 1f;
		[SerializeField] private Vector3[] m_Positions = default(Vector3[]);
		[SerializeField, HideInInspector] private int m_VertexCount;




		public override Texture mainTexture
		{
			get
			{
				if (m_Sprite == null)
				{
					return s_WhiteTexture;
				}

				return m_Sprite.texture;
			}
		}


		public float Width
		{
			get { return m_Width; }
			set
			{
				m_Width = value;
				SetVerticesDirty();
			}
		}


		public int VertexCount
		{
			get { return m_VertexCount; }
		}




#if UNITY_EDITOR
		protected override void OnValidate()
		{
			m_VertexCount = m_Positions == null ? 0 : m_Positions.Length;
			base.OnValidate();
		}
#endif


		[Obsolete("Use OnPopulateMesh instead.", true)]
		protected override void OnFillVBO(List<UIVertex> vbo)
		{
			if (m_Positions == null || VertexCount < 2)
			{
				//base.OnFillVBO(vbo);
				return;
			}

			float uvXmin;
			float uvXMax;
			float uvYmin;
			float uvYMax;
			float uvLength;

			if (m_Sprite.packed &&
				m_Sprite.texture != null)
			{
				uvXmin = m_Sprite.textureRect.x / m_Sprite.texture.width;
				uvXMax = uvXmin + (m_Sprite.textureRect.width / m_Sprite.texture.width);
				uvYmin = m_Sprite.textureRect.y / m_Sprite.texture.height;
				uvYMax = uvYmin + (m_Sprite.textureRect.height / m_Sprite.texture.height);
				uvLength = uvYMax - uvYmin;
			}
			else
			{
				uvXmin = 0f;
				uvXMax = 1f;
				uvYmin = 0f;
				uvYMax = 1f;
				uvLength = 1f;
			}

			float halfWidth = Width * 0.5f;
			float uvStep = 1f/(VertexCount-1)*m_Tiling;
			Vector3 direction = m_Positions[0] - m_Positions[1];
			direction.Normalize();
			var leftDirection = new Vector3(-direction.y, direction.x);
			var rightDirection = new Vector3(direction.y, -direction.x);

			UIVertex lastLeft = UIVertex.simpleVert;
			UIVertex lastRight = UIVertex.simpleVert;
			UIVertex nextLeft = UIVertex.simpleVert;
			UIVertex nextRight = UIVertex.simpleVert;

			lastLeft.position = m_Positions[0] + leftDirection * halfWidth;
			lastLeft.uv0 = new Vector2(uvXmin, uvYMax);

			lastRight.position = m_Positions[0] + rightDirection * halfWidth;
			lastRight.uv0 = new Vector2(uvXMax, uvYMax);

			float nextUv = uvYmin + Mathf.Repeat(uvYMax - uvStep, uvLength);
			for (int i = 1; i < VertexCount - 1; i++)
			{
				direction = m_Positions[i] - m_Positions[i + 1];
				direction.Normalize();

				leftDirection.x = -direction.y;
				leftDirection.y = direction.x;
				rightDirection.x = direction.y;
				rightDirection.y = -direction.x;


				nextLeft.position = m_Positions[i] + leftDirection * halfWidth;
				nextLeft.uv0 = new Vector2(uvXmin, nextUv);

				nextRight.position = m_Positions[i] + rightDirection * halfWidth;
				nextRight.uv0 = new Vector2(uvXMax, nextUv);

				vbo.Add(lastLeft);
				vbo.Add(lastRight);
				vbo.Add(nextRight);
				vbo.Add(nextLeft);

				nextUv = uvYmin + Mathf.Repeat(nextUv - uvStep, uvLength);
				lastLeft.position = nextLeft.position;
				lastLeft.uv0.y = Math.Abs(nextLeft.uv0.y) < _EPSILON ? uvYMax : nextLeft.uv0.y;
				lastRight.position = nextRight.position;
				lastRight.uv0.y = Math.Abs(nextRight.uv0.y) < _EPSILON ? uvYMax : nextRight.uv0.y;
			}

			nextLeft.position = m_Positions[VertexCount - 1] + leftDirection * halfWidth;
			nextLeft.uv0 = new Vector2(uvXmin, nextUv);

			nextRight.position = m_Positions[VertexCount - 1] + rightDirection * halfWidth;
			nextRight.uv0 = new Vector2(uvXMax, nextUv);

			vbo.Add(lastLeft);
			vbo.Add(lastRight);
			vbo.Add(nextRight);
			vbo.Add(nextLeft);
		}


		public void SetVertexCount(int count)
		{
			Assert.IsTrue(count >= 0);

			if (m_VertexCount == count)
			{
				return;
			}

			m_VertexCount = count;

#if UNITY_EDITOR
			if (Application.isPlaying)
			{
				CheckVertexCount();
			}
			else
			{
				Vector3[] temp = m_Positions; 
				m_Positions = new Vector3[VertexCount];
				Array.Copy(temp, m_Positions, Mathf.Min(temp.Length, m_Positions.Length));
			}
#else
			CheckVertexCount();
#endif

			SetVerticesDirty();
		}


		private void CheckVertexCount()
		{
			if (VertexCount > m_Positions.Length)
			{
				Vector3[] temp = m_Positions;
				//TODO: Improve this growth pattern. May want use a larger buffer if we're likely to grow a bit more.
				m_Positions = new Vector3[VertexCount];
				Array.Copy(temp, m_Positions, temp.Length);
			}
		}


		public void SetPosition(int index, Vector3 position)
		{
			Assert.IsTrue(index >= 0);
			Assert.IsTrue(index < VertexCount);

			if (m_Positions[index] == position)
			{
				return;
			}

			m_Positions[index] = position;
			SetVerticesDirty();
		}
	}
}
