﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;


namespace Core.UI
{
	public class TassleHandleElement : MonoBehaviour,
		IPointerClickHandler,
		IBeginDragHandler,
		IDragHandler,
		IEndDragHandler
	{
		[SerializeField, HideInInspector] private Rigidbody2D m_Rigidbody2D = default(Rigidbody2D);
		private Vector2 _dragDelta;
		private bool _isDragging;





		public bool InputLocked
		{
			get; set;
		}


		public bool IsDragging
		{
			get { return _isDragging; }
		}




		public event Action<PointerEventData> OnClicked;


		public event Action<PointerEventData> OnDrag;


		public event Action<PointerEventData> OnDragStateChanged; 
		



		public void Setup(Rigidbody2D body)
		{
			m_Rigidbody2D = body;
		}


		private void EndDrag()
		{
			m_Rigidbody2D.isKinematic = false;
			_isDragging = false;
		}


		void IBeginDragHandler.OnBeginDrag(PointerEventData eventData)
		{
			if (InputLocked)
			{
				return;
			}

			m_Rigidbody2D.isKinematic = true;
			_isDragging = true;
			_dragDelta = (Vector2)m_Rigidbody2D.transform.position - eventData.position;
			eventData.Use();

			if (OnDragStateChanged != null)
			{
				OnDragStateChanged(eventData);
			}
		}


		void IDragHandler.OnDrag(PointerEventData eventData)
		{
			if (InputLocked)
			{
				EndDrag();
				return;
			}

			if (!IsDragging)
			{
				return;
			}

			if (eventData.used)
			{
				return;
			}

			eventData.Use();

			if (OnDrag != null)
			{
				OnDrag(eventData);
			}

			m_Rigidbody2D.position = eventData.position + _dragDelta;
		}


		void IEndDragHandler.OnEndDrag(PointerEventData eventData)
		{
			if (InputLocked ||
				!IsDragging)
			{
				return;
			}

			if (eventData.used)
			{
				return;
			}

			EndDrag();
			eventData.Use();

			if (OnClicked != null)
			{
				OnClicked(eventData);
			}

			if (OnDragStateChanged != null)
			{
				OnDragStateChanged(eventData);
			}
		}


		void IPointerClickHandler.OnPointerClick(PointerEventData eventData)
		{
			if (InputLocked)
			{
				return;
			}

			if (IsDragging)
			{
				return;
			}

			if (OnClicked != null)
			{
				OnClicked(eventData);
			}
		}
	}
}