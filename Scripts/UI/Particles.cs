﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


namespace Core.UI
{
	[ExecuteInEditMode]
	public class Particles : MaskableGraphic
	{
		[SerializeField] private Sprite m_Sprite = default(Sprite);
		private Canvas _root;
		private Transform _transform;
		private ParticleEmitter _emitter;




		public override Texture mainTexture
		{
			get
			{

				if (m_Sprite != null)
				{
					return m_Sprite.texture;
				}

				return base.mainTexture;
			}
		}




		private void Update()
		{
			if (_emitter != null &&
			    _emitter.enabled)
			{
				SetVerticesDirty();
			}
		}


		protected override void OnEnable()
		{
			if (_emitter == null)
			{
				_emitter = GetComponent<ParticleEmitter>();
			}

			if (_transform == null)
			{
				_transform = GetComponent<Transform>();
			}

			if (_root == null)
			{
				Transform t = _transform;

				while (t.parent != null)
				{
					t = t.parent;
				}

				_root = t.GetComponent<Canvas>();
			}


			base.OnEnable();
		}




		[Obsolete("Use OnPopulateMesh instead.", true)]
		protected override void OnFillVBO(List<UIVertex> vbo)
		{
			if (_emitter.useWorldSpace)
			{
				OnFillVBOWorld(vbo);
			}
			else
			{
				OnFillVBOLocal(vbo);
			}
		}


		[Obsolete("Use OnPopulateMesh instead.", true)]
		private void OnFillVBOLocal(List<UIVertex> vbo)
		{
			int particleCount = _emitter.particleCount;

			UIVertex topLeft = UIVertex.simpleVert;
			UIVertex topRight = UIVertex.simpleVert;
			UIVertex bottomLeft = UIVertex.simpleVert;
			UIVertex bottomRight = UIVertex.simpleVert;

			topLeft.uv0 = new Vector2(0f, 1f);
			topRight.uv0 = new Vector2(1f, 1f);
			bottomRight.uv0 = new Vector2(1f, 0f);
			bottomLeft.uv0 = new Vector2(0f, 0f);

			for (int i = 0; i < particleCount; i++)
			{
				var particle = _emitter.particles[i];
				float halfSize = particle.size * 0.5f;
				Quaternion rotation = Quaternion.Euler(0f, 0f, particle.rotation);

				topLeft.color = particle.color;
				topRight.color = particle.color;
				bottomLeft.color = particle.color;
				bottomRight.color = particle.color;

				Vector3 pos = Vector3.zero;
				pos.x -= halfSize;
				pos.y += halfSize;
				topLeft.position = rotation*pos;
				topLeft.position += particle.position;
				vbo.Add(topLeft);

				pos = Vector3.zero;
				pos.x += halfSize;
				pos.y += halfSize;
				topRight.position = rotation * pos;
				topRight.position += particle.position;
				vbo.Add(topRight);

				pos = Vector3.zero;
				pos.x += halfSize;
				pos.y -= halfSize;
				bottomRight.position = rotation * pos;
				bottomRight.position += particle.position;
				vbo.Add(bottomRight);

				pos = Vector3.zero;
				pos.x -= halfSize;
				pos.y -= halfSize;
				bottomLeft.position = rotation * pos;
				bottomLeft.position += particle.position;
				vbo.Add(bottomLeft);
			}
		}


		private void OnFillVBOWorld(List<UIVertex> vbo)
		{
			Vector3 scale = _transform.lossyScale;

			scale.x = _root.scaleFactor/scale.x;
			scale.y = _root.scaleFactor/scale.y;
			scale.z = _root.scaleFactor/scale.z;
			int particleCount = _emitter.particleCount;

			UIVertex topLeft = UIVertex.simpleVert;
			UIVertex topRight = UIVertex.simpleVert;
			UIVertex bottomLeft = UIVertex.simpleVert;
			UIVertex bottomRight = UIVertex.simpleVert;

			topLeft.uv0 = new Vector2(0f, 1f);
			topRight.uv0 = new Vector2(1f, 1f);
			bottomRight.uv0 = new Vector2(1f, 0f);
			bottomLeft.uv0 = new Vector2(0f, 0f);

			for (int i = 0; i < particleCount; i++)
			{
				var particle = _emitter.particles[i];
				float halfSize = particle.size * 0.5f;
				Quaternion rotation = Quaternion.Euler(0f, 0f, particle.rotation);
				Quaternion reverseLocal = Quaternion.Inverse(_transform.rotation);
				rotation *= reverseLocal;
				Vector3 position = reverseLocal*(particle.position - _transform.position);

				topLeft.color = particle.color;
				topRight.color = particle.color;
				bottomLeft.color = particle.color;
				bottomRight.color = particle.color;

				Vector3 pos = Vector3.zero;
				pos.x -= halfSize;
				pos.y += halfSize;
				topLeft.position = rotation * pos;
				topLeft.position += position;
				topLeft.position = Vector3.Scale(topLeft.position, scale);
				vbo.Add(topLeft);

				pos = Vector3.zero;
				pos.x += halfSize;
				pos.y += halfSize;
				topRight.position = rotation * pos;
				topRight.position += position;
				topRight.position = Vector3.Scale(topRight.position, scale);
				vbo.Add(topRight);

				pos = Vector3.zero;
				pos.x += halfSize;
				pos.y -= halfSize;
				bottomRight.position = rotation * pos;
				bottomRight.position += position;
				bottomRight.position = Vector3.Scale(bottomRight.position, scale);
				vbo.Add(bottomRight);

				pos = Vector3.zero;
				pos.x -= halfSize;
				pos.y -= halfSize;
				bottomLeft.position = rotation * pos;
				bottomLeft.position += position;
				bottomLeft.position = Vector3.Scale(bottomLeft.position, scale);
				vbo.Add(bottomLeft);
			}
		}
	}
}
