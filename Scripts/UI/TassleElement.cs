﻿using UnityEngine;
using UnityEngine.UI;


namespace Core.UI
{
	public class TassleElement : MonoBehaviour
	{
		[SerializeField] private float m_Length = 50f;
		[SerializeField, Range(1, 20)] private int m_Segments = 10;
#if UNITY_EDITOR
		[SerializeField] private float m_BodyMass = 1f;
		[SerializeField] private float m_BodyDrag = 1f;
		[SerializeField] private float m_JointDampenRatio = 1f;
		[SerializeField] private Vector2 m_HandleOffset = new Vector2(0, -13f);
		[SerializeField] private float m_JointFrequency = 10f;
		[SerializeField] private float m_HandleSize = 15f;
		[SerializeField] private Sprite m_HandleSprite = default(Sprite);
#endif
		[SerializeField] private Line m_Line = default(Line);
		[SerializeField] private Vector2 m_Gravity = new Vector2(0, -400f);
		[SerializeField, HideInInspector] private Rigidbody2D[] m_TassleBody = default(Rigidbody2D[]);
		[SerializeField, HideInInspector] private Transform m_Anchor = default(Transform);
		[SerializeField, HideInInspector] private TassleHandleElement m_Handle = default(TassleHandleElement);
		private Canvas _canvas;
		private Vector2 _gravity;




		public TassleHandleElement Handle
		{
			get { return m_Handle; }
		}




#if UNITY_EDITOR
		private void Update()
		{
			CheckTassleSettings();
		}
#endif

		private void Start()
		{
			CheckTassleSettings();

			for (int i = 0; i < m_TassleBody.Length; i++)
			{
				var r = m_TassleBody[i].GetComponent<Rigidbody2D>();
				r.position = r.transform.position;
			}
		}


		private void FixedUpdate()
		{
			for (int i = 1; i < m_TassleBody.Length; i++)
			{
				var body = m_TassleBody[i];

				if (body.isKinematic ||
					body.IsSleeping())
				{
					continue;
				}

				body.velocity += _gravity*UnityEngine.Time.deltaTime;
			}

			UpdateLine();
		}




		public void AwakenRope()
		{
			for (int i = 0; i < m_TassleBody.Length; i++)
			{
				var d = m_TassleBody[i];
				d.WakeUp();
			}
		}


		private void UpdateLine()
		{
			m_Line.SetPosition(0, m_Anchor.localPosition);
			for (int i = 0; i < m_TassleBody.Length; i++)
			{
				m_Line.SetPosition(i + 1, m_TassleBody[i].transform.localPosition);
			}
		}


		private void CheckTassleSettings()
		{
			Transform t = transform;

			while (t.parent != null)
			{
				t = t.parent;
			}

			_canvas = t.GetComponent<Canvas>();
			_gravity = m_Gravity * _canvas.scaleFactor;
			float segmentLength = m_Length / m_Segments * _canvas.scaleFactor;

			for (int i = 0; i < m_TassleBody.Length; i++)
			{
				var body = m_TassleBody[i];
				body.GetComponent<SpringJoint2D>().distance = segmentLength;
			}
		}



#if UNITY_EDITOR
		[ContextMenu("Rebuild")]
		private void Rebuild()
		{
			if (Handle != null)
			{
				DestroyImmediate(Handle.gameObject);
				m_Handle = null;
			}

			if (m_Anchor != null)
			{
				DestroyImmediate(m_Anchor.gameObject);
				m_Anchor = null;
			}

			if (m_TassleBody != null)
			{
				for (int i = 0; i < m_TassleBody.Length; i++)
				{
					var j = m_TassleBody[i];

					if (j != null)
					{
						DestroyImmediate(j.gameObject);
					}
				}	
			}

			m_TassleBody = new Rigidbody2D[m_Segments];

			var anchorObj = new GameObject("Anchor");

			var trans = anchorObj.AddComponent<RectTransform>();
			var anchor = anchorObj.AddComponent<Rigidbody2D>();

			trans.SetParent(transform, false);
			trans.localScale = Vector3.one;
			anchor.isKinematic = true;

			m_Anchor = trans;

			float segmentLength = m_Length/m_Segments;

			for (int i = 0; i < m_TassleBody.Length; i++)
			{
				var obj = new GameObject(string.Format("Joint {0}", i));

				var t = obj.GetComponent<Transform>();
				var r = obj.AddComponent<Rigidbody2D>();
				var j = obj.AddComponent<SpringJoint2D>();

				t.SetParent(transform, false);
				t.localPosition = new Vector3(0f, -segmentLength * (i + 1));
				t.localScale = Vector3.one;

				r.mass = m_BodyMass;
				r.drag = m_BodyDrag;
				r.gravityScale = 0f;
				j.connectedBody = anchor;
				j.distance = segmentLength;
				j.dampingRatio = m_JointDampenRatio;
				j.frequency = m_JointFrequency;
				//j.anchor = t.position;

				anchor = r;
				m_TassleBody[i] = r;
			}
			
			Rigidbody2D lastBody = m_TassleBody[m_TassleBody.Length - 1];
			var c = lastBody.gameObject.AddComponent<CircleCollider2D>();
			c.offset = m_HandleOffset;
			c.radius = m_HandleSize;

			var handleObject = new GameObject("Handle");
			var handleGraphic = handleObject.AddComponent<Image>();
			handleGraphic.sprite = m_HandleSprite;
			handleGraphic.SetNativeSize();
			m_Handle = handleObject.AddComponent<TassleHandleElement>();
			Handle.Setup(lastBody);


			handleObject.transform.SetParent(lastBody.transform);
			handleObject.transform.localPosition = m_HandleOffset;

			
			m_Line.SetVertexCount(m_Segments + 1);
			UpdateLine();
		}
#endif
	}
}
