﻿using System;
using System.Diagnostics;
using System.Globalization;


namespace Core.Debug
{
	public static class Assert
	{
		[Conditional(Conditionals.UNITY_ASERTIONS), Conditional(Conditionals.UNITY_EDITOR), Conditional(Conditionals.DEBUG), Conditional(Conditionals.DEVELOPMENT_BUILD)]
		public static void AreEqual(object expected, object actual)
		{
			AreEqual(expected, actual, "Expected is not equal to actual");
		}


		[Conditional(Conditionals.UNITY_ASERTIONS), Conditional(Conditionals.UNITY_EDITOR), Conditional(Conditionals.DEBUG), Conditional(Conditionals.DEVELOPMENT_BUILD)]
		public static void AreEqual(object expected, object actual, string message, params object[] parameters)
		{
			AreEqual(expected, actual, string.Format(message, parameters));
		}


		[Conditional(Conditionals.UNITY_ASERTIONS), Conditional(Conditionals.UNITY_EDITOR), Conditional(Conditionals.DEBUG), Conditional(Conditionals.DEVELOPMENT_BUILD)]
		public static void AreEqual(object expected, object actual, string message)
		{
			if (!(expected.Equals(actual)))
			{
				throw new AssertFailedException(message);
			}
		}


		[Conditional(Conditionals.UNITY_ASERTIONS), Conditional(Conditionals.UNITY_EDITOR), Conditional(Conditionals.DEBUG), Conditional(Conditionals.DEVELOPMENT_BUILD)]
		public static void AreEqual(float expected, float actual, float delta)
		{
			AreEqual(expected, actual, delta, string.Format("Expected value {0} is different from actual value {1} by more than delta value {2}", expected, actual, delta));
		}


		[Conditional(Conditionals.UNITY_ASERTIONS), Conditional(Conditionals.UNITY_EDITOR), Conditional(Conditionals.DEBUG), Conditional(Conditionals.DEVELOPMENT_BUILD)]
		public static void AreEqual(float expected, float actual, float delta, string message, params object[] parameters)
		{
			AreEqual(expected, actual, delta, string.Format(message, parameters));
		}


		[Conditional(Conditionals.UNITY_ASERTIONS), Conditional(Conditionals.UNITY_EDITOR), Conditional(Conditionals.DEBUG), Conditional(Conditionals.DEVELOPMENT_BUILD)]
		public static void AreEqual(float expected, float actual, float delta, string message)
		{
			if (Math.Abs(expected - actual) > delta)
			{
				throw new Exception(message);
			}
		}


		[Conditional(Conditionals.UNITY_ASERTIONS), Conditional(Conditionals.UNITY_EDITOR), Conditional(Conditionals.DEBUG), Conditional(Conditionals.DEVELOPMENT_BUILD)]
		public static void AreEqual(double expected, double actual, double delta)
		{
			AreEqual(expected, actual, delta, string.Format("Expected value {0} is different from actual value {1} by more than delta value {2}", expected, actual, delta));
		}


		[Conditional(Conditionals.UNITY_ASERTIONS), Conditional(Conditionals.UNITY_EDITOR), Conditional(Conditionals.DEBUG), Conditional(Conditionals.DEVELOPMENT_BUILD)]
		public static void AreEqual(double expected, double actual, double delta, string message, params object[] parameters)
		{
			AreEqual(expected, actual, delta, string.Format(message, parameters));
		}


		[Conditional(Conditionals.UNITY_ASERTIONS), Conditional(Conditionals.UNITY_EDITOR), Conditional(Conditionals.DEBUG), Conditional(Conditionals.DEVELOPMENT_BUILD)]
		public static void AreEqual(double expected, double actual, double delta, string message)
		{
			if (Math.Abs(expected - actual) > delta)
			{
				throw new Exception(message);
			}
		}


		[Conditional(Conditionals.UNITY_ASERTIONS), Conditional(Conditionals.UNITY_EDITOR), Conditional(Conditionals.DEBUG), Conditional(Conditionals.DEVELOPMENT_BUILD)]
		public static void AreEqual(string expected, string actual, bool ignoreCase)
		{
			AreEqual(expected, actual, ignoreCase, CultureInfo.InvariantCulture, string.Format("Expected value \"{0}\" is not equal to actual value \"{1}\"", expected, actual));
		}


		[Conditional(Conditionals.UNITY_ASERTIONS), Conditional(Conditionals.UNITY_EDITOR), Conditional(Conditionals.DEBUG), Conditional(Conditionals.DEVELOPMENT_BUILD)]
		public static void AreEqual(string expected, string actual, bool ignoreCase, CultureInfo culture)
		{
			AreEqual(expected, actual, ignoreCase, culture, string.Format("Expected value \"{0}\" is not equal to actual value \"{1}\"", expected, actual));
		}


		[Conditional(Conditionals.UNITY_ASERTIONS), Conditional(Conditionals.UNITY_EDITOR), Conditional(Conditionals.DEBUG), Conditional(Conditionals.DEVELOPMENT_BUILD)]
		public static void AreEqual(string expected, string actual, bool ignoreCase, string message,
			params object[] parameters)
		{
			AreEqual(expected, actual, ignoreCase, CultureInfo.InvariantCulture, string.Format(message, parameters));
		}


		[Conditional(Conditionals.UNITY_ASERTIONS), Conditional(Conditionals.UNITY_EDITOR), Conditional(Conditionals.DEBUG), Conditional(Conditionals.DEVELOPMENT_BUILD)]
		public static void AreEqual(string expected, string actual, bool ignoreCase, string message)
		{
			AreEqual(expected, actual, ignoreCase, CultureInfo.InvariantCulture, message);
		}


		[Conditional(Conditionals.UNITY_ASERTIONS), Conditional(Conditionals.UNITY_EDITOR), Conditional(Conditionals.DEBUG), Conditional(Conditionals.DEVELOPMENT_BUILD)]
		public static void AreEqual(string expected, string actual, bool ignoreCase, CultureInfo culture, string message,
			params object[] parameters)
		{
			AreEqual(expected, actual, ignoreCase, culture, string.Format(message, parameters));
		}


		[Conditional(Conditionals.UNITY_ASERTIONS), Conditional(Conditionals.UNITY_EDITOR), Conditional(Conditionals.DEBUG), Conditional(Conditionals.DEVELOPMENT_BUILD)]
		public static void AreEqual(string expected, string actual, bool ignoreCase, CultureInfo culture, string message)
		{
			if (culture.CompareInfo.Compare(expected, actual, ignoreCase ? CompareOptions.IgnoreCase : CompareOptions.None) != 0)
			{
				throw new AssertFailedException(message);
			}
		}


		[Conditional(Conditionals.UNITY_ASERTIONS), Conditional(Conditionals.UNITY_EDITOR), Conditional(Conditionals.DEBUG), Conditional(Conditionals.DEVELOPMENT_BUILD)]
		public static void AreEqual<T>(T expected, T actual)
		{
			AreEqual(expected, actual, "Expected is not equal to actual");
		}


		[Conditional(Conditionals.UNITY_ASERTIONS), Conditional(Conditionals.UNITY_EDITOR), Conditional(Conditionals.DEBUG), Conditional(Conditionals.DEVELOPMENT_BUILD)]
		public static void AreEqual<T>(T expected, T actual, string message, params object[] parameters)
		{
			AreEqual(expected, actual, string.Format(message, parameters));
		}


		[Conditional(Conditionals.UNITY_ASERTIONS), Conditional(Conditionals.UNITY_EDITOR), Conditional(Conditionals.DEBUG), Conditional(Conditionals.DEVELOPMENT_BUILD)]
		public static void AreEqual<T>(T expected, T actual, string message)
		{
			if (!(expected.Equals(actual)))
			{
				throw new AssertFailedException(message);
			}
		}


		[Conditional(Conditionals.UNITY_ASERTIONS), Conditional(Conditionals.UNITY_EDITOR), Conditional(Conditionals.DEBUG), Conditional(Conditionals.DEVELOPMENT_BUILD)]
		public static void AreNotEqual(object notExpected, object actual)
		{
			AreNotEqual(notExpected, actual, "Not expected is equal to actual");
		}


		[Conditional(Conditionals.UNITY_ASERTIONS), Conditional(Conditionals.UNITY_EDITOR), Conditional(Conditionals.DEBUG), Conditional(Conditionals.DEVELOPMENT_BUILD)]
		public static void AreNotEqual(object notExpected, object actual, string message, params object[] parameters)
		{
			AreNotEqual(notExpected, actual, string.Format(message, parameters));
		}


		[Conditional(Conditionals.UNITY_ASERTIONS), Conditional(Conditionals.UNITY_EDITOR), Conditional(Conditionals.DEBUG), Conditional(Conditionals.DEVELOPMENT_BUILD)]
		public static void AreNotEqual(object notExpected, object actual, string message)
		{
			if (notExpected.Equals(actual))
			{
				throw new AssertFailedException(message);
			}
		}


		[Conditional(Conditionals.UNITY_ASERTIONS), Conditional(Conditionals.UNITY_EDITOR), Conditional(Conditionals.DEBUG), Conditional(Conditionals.DEVELOPMENT_BUILD)]
		public static void AreNotEqual(float notExpected, float actual, float delta)
		{
			AreNotEqual(notExpected, actual, delta, "Not expected is equal to actual or different by less than delta");
		}


		[Conditional(Conditionals.UNITY_ASERTIONS), Conditional(Conditionals.UNITY_EDITOR), Conditional(Conditionals.DEBUG), Conditional(Conditionals.DEVELOPMENT_BUILD)]
		public static void AreNotEqual(float notExpected, float actual, float delta, string message,
			params object[] parameters)
		{
			AreNotEqual(notExpected, actual, delta, string.Format(message, parameters));
		}


		[Conditional(Conditionals.UNITY_ASERTIONS), Conditional(Conditionals.UNITY_EDITOR), Conditional(Conditionals.DEBUG), Conditional(Conditionals.DEVELOPMENT_BUILD)]
		public static void AreNotEqual(float notExpected, float actual, float delta, string message)
		{
			if (Math.Abs(notExpected - actual) < delta)
			{
				throw new Exception(message);
			}
		}


		[Conditional(Conditionals.UNITY_ASERTIONS), Conditional(Conditionals.UNITY_EDITOR), Conditional(Conditionals.DEBUG), Conditional(Conditionals.DEVELOPMENT_BUILD)]
		public static void AreNotEqual(double notExpected, double actual, double delta)
		{
			AreNotEqual(notExpected, actual, delta, "Not expected is equal to actual or different by less than delta");
		}


		[Conditional(Conditionals.UNITY_ASERTIONS), Conditional(Conditionals.UNITY_EDITOR), Conditional(Conditionals.DEBUG), Conditional(Conditionals.DEVELOPMENT_BUILD)]
		public static void AreNotEqual(double notExpected, double actual, double delta, string message,
			params object[] parameters)
		{
			AreNotEqual(notExpected, actual, delta, string.Format(message, parameters));
		}


		[Conditional(Conditionals.UNITY_ASERTIONS), Conditional(Conditionals.UNITY_EDITOR), Conditional(Conditionals.DEBUG), Conditional(Conditionals.DEVELOPMENT_BUILD)]
		public static void AreNotEqual(double notExpected, double actual, double delta, string message)
		{
			if (Math.Abs(notExpected - actual) < delta)
			{
				throw new Exception(message);
			}
		}


		[Conditional(Conditionals.UNITY_ASERTIONS), Conditional(Conditionals.UNITY_EDITOR), Conditional(Conditionals.DEBUG), Conditional(Conditionals.DEVELOPMENT_BUILD)]
		public static void AreNotEqual(string notExpected, string actual, bool ignoreCase)
		{
			AreNotEqual(notExpected, actual, ignoreCase, CultureInfo.InvariantCulture, "Not expected is equal to actual");
		}


		[Conditional(Conditionals.UNITY_ASERTIONS), Conditional(Conditionals.UNITY_EDITOR), Conditional(Conditionals.DEBUG), Conditional(Conditionals.DEVELOPMENT_BUILD)]
		public static void AreNotEqual(string notExpected, string actual, bool ignoreCase, CultureInfo culture)
		{
			AreNotEqual(notExpected, actual, ignoreCase, culture, "Not expected is equal to actual");
		}


		[Conditional(Conditionals.UNITY_ASERTIONS), Conditional(Conditionals.UNITY_EDITOR), Conditional(Conditionals.DEBUG), Conditional(Conditionals.DEVELOPMENT_BUILD)]
		public static void AreNotEqual(string notExpected, string actual, bool ignoreCase, string message,
			params object[] parameters)
		{
			AreNotEqual(notExpected, actual, ignoreCase, CultureInfo.InvariantCulture, string.Format(message, parameters));
		}


		[Conditional(Conditionals.UNITY_ASERTIONS), Conditional(Conditionals.UNITY_EDITOR), Conditional(Conditionals.DEBUG), Conditional(Conditionals.DEVELOPMENT_BUILD)]
		public static void AreNotEqual(string notExpected, string actual, bool ignoreCase, string message)
		{
			AreNotEqual(notExpected, actual, ignoreCase, CultureInfo.InvariantCulture, message);
		}


		[Conditional(Conditionals.UNITY_ASERTIONS), Conditional(Conditionals.UNITY_EDITOR), Conditional(Conditionals.DEBUG), Conditional(Conditionals.DEVELOPMENT_BUILD)]
		public static void AreNotEqual(string notExpected, string actual, bool ignoreCase, CultureInfo culture, string message,
			params object[] parameters)
		{
			AreNotEqual(notExpected, actual, ignoreCase, culture, string.Format(message, parameters));
		}


		[Conditional(Conditionals.UNITY_ASERTIONS), Conditional(Conditionals.UNITY_EDITOR), Conditional(Conditionals.DEBUG), Conditional(Conditionals.DEVELOPMENT_BUILD)]
		public static void AreNotEqual(string notExpected, string actual, bool ignoreCase, CultureInfo culture, string message)
		{
			if (culture.CompareInfo.Compare(notExpected, actual, ignoreCase ? CompareOptions.IgnoreCase : CompareOptions.None) ==
			    0)
			{
				throw new AssertFailedException(message);
			}
		}


		[Conditional(Conditionals.UNITY_ASERTIONS), Conditional(Conditionals.UNITY_EDITOR), Conditional(Conditionals.DEBUG), Conditional(Conditionals.DEVELOPMENT_BUILD)]
		public static void AreNotEqual<T>(T notExpected, T actual)
		{
			AreNotEqual(notExpected, actual, "Not expected is equal to actual");
		}


		[Conditional(Conditionals.UNITY_ASERTIONS), Conditional(Conditionals.UNITY_EDITOR), Conditional(Conditionals.DEBUG), Conditional(Conditionals.DEVELOPMENT_BUILD)]
		public static void AreNotEqual<T>(T notExpected, T actual, string message, params object[] parameters)
		{
			AreNotEqual(notExpected, actual, string.Format(message, parameters));
		}


		[Conditional(Conditionals.UNITY_ASERTIONS), Conditional(Conditionals.UNITY_EDITOR), Conditional(Conditionals.DEBUG), Conditional(Conditionals.DEVELOPMENT_BUILD)]
		public static void AreNotEqual<T>(T notExpected, T actual, string message)
		{
			if (notExpected.Equals(actual))
			{
				throw new AssertFailedException(message);
			}
		}


		[Conditional(Conditionals.UNITY_ASERTIONS), Conditional(Conditionals.UNITY_EDITOR), Conditional(Conditionals.DEBUG), Conditional(Conditionals.DEVELOPMENT_BUILD)]
		public static void AreSame(object expected, object actual)
		{
			AreSame(expected, actual, "Expected does not refer to the same object as actual");
		}


		[Conditional(Conditionals.UNITY_ASERTIONS), Conditional(Conditionals.UNITY_EDITOR), Conditional(Conditionals.DEBUG), Conditional(Conditionals.DEVELOPMENT_BUILD)]
		public static void AreSame(object expected, object actual, string message, params object[] parameters)
		{
			AreSame(expected, actual, string.Format(message, parameters));
		}


		[Conditional(Conditionals.UNITY_ASERTIONS), Conditional(Conditionals.UNITY_EDITOR), Conditional(Conditionals.DEBUG), Conditional(Conditionals.DEVELOPMENT_BUILD)]
		public static void AreSame(object expected, object actual, string message)
		{
			if (!(ReferenceEquals(expected, actual)))
			{
				throw new AssertFailedException(message);
			}
		}


		[Conditional(Conditionals.UNITY_ASERTIONS), Conditional(Conditionals.UNITY_EDITOR), Conditional(Conditionals.DEBUG), Conditional(Conditionals.DEVELOPMENT_BUILD)]
		public static void AreNotSame(object notExpected, object actual)
		{
			AreNotSame(notExpected, actual, "Not expected refers to the same object as actual");
		}


		[Conditional(Conditionals.UNITY_ASERTIONS), Conditional(Conditionals.UNITY_EDITOR), Conditional(Conditionals.DEBUG), Conditional(Conditionals.DEVELOPMENT_BUILD)]
		public static void AreNotSame(object notExpected, object actual, string message, params object[] parameters)
		{
			AreNotSame(notExpected, actual, string.Format(message, parameters));
		}


		[Conditional(Conditionals.UNITY_ASERTIONS), Conditional(Conditionals.UNITY_EDITOR), Conditional(Conditionals.DEBUG), Conditional(Conditionals.DEVELOPMENT_BUILD)]
		public static void AreNotSame(object notExpected, object actual, string message)
		{
			if (ReferenceEquals(notExpected, actual))
			{
				throw new AssertFailedException(message);
			}
		}


		[Conditional(Conditionals.UNITY_ASERTIONS), Conditional(Conditionals.UNITY_EDITOR), Conditional(Conditionals.DEBUG), Conditional(Conditionals.DEVELOPMENT_BUILD)]
		public static void IsTrue(bool condition)
		{
			IsTrue(condition, "Condition evaluates to false");
		}


		[Conditional(Conditionals.UNITY_ASERTIONS), Conditional(Conditionals.UNITY_EDITOR), Conditional(Conditionals.DEBUG), Conditional(Conditionals.DEVELOPMENT_BUILD)]
		public static void IsTrue(bool condition, string message, params object[] paramaters)
		{
			IsTrue(condition, string.Format(message, paramaters));
		}


		[Conditional(Conditionals.UNITY_ASERTIONS), Conditional(Conditionals.UNITY_EDITOR), Conditional(Conditionals.DEBUG), Conditional(Conditionals.DEVELOPMENT_BUILD)]
		public static void IsTrue(bool condition, string message)
		{
			if (!condition)
			{
				throw new AssertFailedException(message);
			}
		}


		[Conditional(Conditionals.UNITY_ASERTIONS), Conditional(Conditionals.UNITY_EDITOR), Conditional(Conditionals.DEBUG), Conditional(Conditionals.DEVELOPMENT_BUILD)]
		public static void IsFalse(bool condition)
		{
			IsFalse(condition, "Condition evaluates to true");
		}


		[Conditional(Conditionals.UNITY_ASERTIONS), Conditional(Conditionals.UNITY_EDITOR), Conditional(Conditionals.DEBUG), Conditional(Conditionals.DEVELOPMENT_BUILD)]
		public static void IsFalse(bool condition, string message, params object[] paramaters)
		{
			IsFalse(condition, string.Format(message, paramaters));
		}


		[Conditional(Conditionals.UNITY_ASERTIONS), Conditional(Conditionals.UNITY_EDITOR), Conditional(Conditionals.DEBUG), Conditional(Conditionals.DEVELOPMENT_BUILD)]
		public static void IsFalse(bool condition, string message)
		{
			if (condition)
			{
				throw new AssertFailedException(message);
			}
		}


		[Conditional(Conditionals.UNITY_ASERTIONS), Conditional(Conditionals.UNITY_EDITOR), Conditional(Conditionals.DEBUG), Conditional(Conditionals.DEVELOPMENT_BUILD)]
		public static void IsNull(object value)
		{
			IsNull(value, "Value is not null");
		}


		[Conditional(Conditionals.UNITY_ASERTIONS), Conditional(Conditionals.UNITY_EDITOR), Conditional(Conditionals.DEBUG), Conditional(Conditionals.DEVELOPMENT_BUILD)]
		public static void IsNull(object value, string message, params object[] paramaters)
		{
			IsNull(value, string.Format(message, paramaters));
		}


		[Conditional(Conditionals.UNITY_ASERTIONS), Conditional(Conditionals.UNITY_EDITOR), Conditional(Conditionals.DEBUG), Conditional(Conditionals.DEVELOPMENT_BUILD)]
		public static void IsNull(object value, string message)
		{
			if (value != null)
			{
				throw new AssertFailedException(message);
			}
		}


		[Conditional(Conditionals.UNITY_ASERTIONS), Conditional(Conditionals.UNITY_EDITOR), Conditional(Conditionals.DEBUG), Conditional(Conditionals.DEVELOPMENT_BUILD)]
		public static void IsNotNull(object value)
		{
			IsNotNull(value, "Value is null");
		}


		[Conditional(Conditionals.UNITY_ASERTIONS), Conditional(Conditionals.UNITY_EDITOR), Conditional(Conditionals.DEBUG), Conditional(Conditionals.DEVELOPMENT_BUILD)]
		public static void IsNotNull(object value, string message, params object[] paramaters)
		{
			IsNotNull(value, string.Format(message, paramaters));
		}


		[Conditional(Conditionals.UNITY_ASERTIONS), Conditional(Conditionals.UNITY_EDITOR), Conditional(Conditionals.DEBUG), Conditional(Conditionals.DEVELOPMENT_BUILD)]
		public static void IsNotNull(object value, string message)
		{
			if (value == null)
			{
				throw new AssertFailedException(message);
			}
		}


		[Conditional(Conditionals.UNITY_ASERTIONS), Conditional(Conditionals.UNITY_EDITOR), Conditional(Conditionals.DEBUG), Conditional(Conditionals.DEVELOPMENT_BUILD)]
		public static void IsInstanceOfType(object value, Type expectedType)
		{
			IsInstanceOfType(value, expectedType,
				"Value is null or expected type is not found in the inheritance hierarchy of value");
		}


		[Conditional(Conditionals.UNITY_ASERTIONS), Conditional(Conditionals.UNITY_EDITOR), Conditional(Conditionals.DEBUG), Conditional(Conditionals.DEVELOPMENT_BUILD)]
		public static void IsInstanceOfType(object value, Type expectedType, string message, params object[] paramaters)
		{
			IsInstanceOfType(value, expectedType, string.Format(message, paramaters));
		}


		[Conditional(Conditionals.UNITY_ASERTIONS), Conditional(Conditionals.UNITY_EDITOR), Conditional(Conditionals.DEBUG), Conditional(Conditionals.DEVELOPMENT_BUILD)]
		public static void IsInstanceOfType(object value, Type expectedType, string message)
		{
			if (!expectedType.IsInstanceOfType(value))
			{
				throw new AssertFailedException(message);
			}
		}


		[Conditional(Conditionals.UNITY_ASERTIONS), Conditional(Conditionals.UNITY_EDITOR), Conditional(Conditionals.DEBUG), Conditional(Conditionals.DEVELOPMENT_BUILD)]
		public static void IsNotInstanceOfType(object value, Type wrongType)
		{
			IsNotInstanceOfType(value, wrongType,
				"Value is not null and expected type is found in the inheritance hierarchy of value");
		}


		[Conditional(Conditionals.UNITY_ASERTIONS), Conditional(Conditionals.UNITY_EDITOR), Conditional(Conditionals.DEBUG), Conditional(Conditionals.DEVELOPMENT_BUILD)]
		public static void IsNotInstanceOfType(object value, Type wrongType, string message, params object[] paramaters)
		{
			IsNotInstanceOfType(value, wrongType, string.Format(message, paramaters));
		}


		[Conditional(Conditionals.UNITY_ASERTIONS), Conditional(Conditionals.UNITY_EDITOR), Conditional(Conditionals.DEBUG), Conditional(Conditionals.DEVELOPMENT_BUILD)]
		public static void IsNotInstanceOfType(object value, Type wrongType, string message)
		{
			if (wrongType.IsInstanceOfType(value))
			{
				throw new AssertFailedException(message);
			}
		}


		[Conditional(Conditionals.UNITY_ASERTIONS), Conditional(Conditionals.UNITY_EDITOR), Conditional(Conditionals.DEBUG), Conditional(Conditionals.DEVELOPMENT_BUILD)]
		public static void Fail()
		{
			Fail("Line assertion");
		}


		[Conditional(Conditionals.UNITY_ASERTIONS), Conditional(Conditionals.UNITY_EDITOR), Conditional(Conditionals.DEBUG), Conditional(Conditionals.DEVELOPMENT_BUILD)]
		public static void Fail(string message, params object[] paramaters)
		{
			Fail(string.Format(message, paramaters));
		}


		[Conditional(Conditionals.UNITY_ASERTIONS), Conditional(Conditionals.UNITY_EDITOR), Conditional(Conditionals.DEBUG), Conditional(Conditionals.DEVELOPMENT_BUILD)]
		public static void Fail(string message)
		{
			throw new AssertFailedException(message);
		}
	}
}