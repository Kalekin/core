﻿namespace Core.Debug
{
	public static class Conditionals
	{
		public const string UNITY_EDITOR = "UNITY_EDITOR";
		public const string UNITY_LOGS = "UNITY_LOGS";
		public const string UNITY_ASERTIONS = "UNITY_ASSERTIONS";
		public const string DEBUG = "DEBUG";
		public const string DEVELOPMENT_BUILD = "DEVELOPMENT_BUILD";
	}
}
