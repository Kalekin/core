﻿using System;
using System.Diagnostics;
using Object = UnityEngine.Object;


namespace Core.Debug
{
	public static class Logger
	{
		[Conditional(Conditionals.UNITY_LOGS), Conditional(Conditionals.UNITY_EDITOR), Conditional(Conditionals.DEBUG), Conditional(Conditionals.DEVELOPMENT_BUILD)]
		public static void Log(string message)
		{
			UnityEngine.Debug.Log(message);
		}


		[Conditional(Conditionals.UNITY_LOGS), Conditional(Conditionals.UNITY_EDITOR), Conditional(Conditionals.DEBUG), Conditional(Conditionals.DEVELOPMENT_BUILD)]
		public static void Log(string message, Object context)
		{
			UnityEngine.Debug.Log(message, context);
		}


		[Conditional(Conditionals.UNITY_LOGS), Conditional(Conditionals.UNITY_EDITOR), Conditional(Conditionals.DEBUG), Conditional(Conditionals.DEVELOPMENT_BUILD)]
		public static void LogWarning(string message)
		{
			UnityEngine.Debug.LogWarning(message);
		}


		[Conditional(Conditionals.UNITY_LOGS), Conditional(Conditionals.UNITY_EDITOR), Conditional(Conditionals.DEBUG), Conditional(Conditionals.DEVELOPMENT_BUILD)]
		public static void LogWarning(string message, Object context)
		{
			UnityEngine.Debug.LogWarning(message, context);
		}


		[Conditional(Conditionals.UNITY_LOGS), Conditional(Conditionals.UNITY_EDITOR), Conditional(Conditionals.DEBUG), Conditional(Conditionals.DEVELOPMENT_BUILD)]
		public static void LogError(string message)
		{
			UnityEngine.Debug.LogError(message);
		}


		[Conditional(Conditionals.UNITY_LOGS), Conditional(Conditionals.UNITY_EDITOR), Conditional(Conditionals.DEBUG), Conditional(Conditionals.DEVELOPMENT_BUILD)]
		public static void LogError(string message, Object context)
		{
			UnityEngine.Debug.LogError(message, context);
		}


		[Conditional(Conditionals.UNITY_LOGS), Conditional(Conditionals.UNITY_EDITOR), Conditional(Conditionals.DEBUG), Conditional(Conditionals.DEVELOPMENT_BUILD)]
		public static void LogException(Exception exception)
		{
			UnityEngine.Debug.LogException(exception);
		}


		[Conditional(Conditionals.UNITY_LOGS), Conditional(Conditionals.UNITY_EDITOR), Conditional(Conditionals.DEBUG), Conditional(Conditionals.DEVELOPMENT_BUILD)]
		public static void LogException(Exception exception, Object context)
		{
			UnityEngine.Debug.LogException(exception, context);
		}
	}
}
