﻿using System;
using System.Runtime.Serialization;


namespace Core.Debug
{
	public class AssertFailedException : Exception
	{
		public AssertFailedException()
		{

		}


		public AssertFailedException(string message)
			: base(message)
		{

		}


		public AssertFailedException(SerializationInfo info, StreamingContext context)
			: base(info, context)
		{

		}


		public AssertFailedException(string message, Exception innerException)
			: base(message, innerException)
		{

		}
	}
}