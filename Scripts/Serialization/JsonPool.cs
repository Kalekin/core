﻿using Core.Pooling;


namespace Core.Serialization
{
	public class JsonPool
	{
		private ObjectPool<JsonPrimitive> _primitivePool;
		private ObjectPool<JsonArray> _arrayPool;
		private ObjectPool<JsonObject> _objectPool;




		public JsonPool()
		{
			_primitivePool = new ObjectPool<JsonPrimitive>(OnConstructJsonPrimitive, null, OnRecycleJsonPrimitive);
			_arrayPool = new ObjectPool<JsonArray>(OnConstructJsonArray, null, OnRecycleJsonArray);
			_objectPool = new ObjectPool<JsonObject>(OnConstructJsonObject, null, OnRecycleJsonObject);
		}




		public JsonNull GetNull()
		{
			return JsonNull.Instance;
		}


		public JsonPrimitive GetPrimitive()
		{
			return _primitivePool.Get();
		}


		public JsonArray GetArray()
		{
			return _arrayPool.Get();
		}


		public JsonObject GetObject()
		{
			return _objectPool.Get();
		}


		public void Recycle(JsonElement element)
		{
			switch (element.Type)
			{
				case JsonElementType.Primitive:
					_primitivePool.Release((JsonPrimitive)element);
					break;
				case JsonElementType.Array:
					var array = (JsonArray)element;

					// ReSharper disable once PossibleNullReferenceException
					for (int i = 0; i < array.Count; i++)
					{
						Recycle(array[i]);
					}

					_arrayPool.Release(array);
					break;
				case JsonElementType.Object: 
					var obj = (JsonObject)element;

					// ReSharper disable once PossibleNullReferenceException
					foreach (var keyValuePair in obj)
					{
						Recycle(keyValuePair.Value);
					}

					_objectPool.Release((JsonObject)element);
					break;
			}
		}


		private static JsonPrimitive OnConstructJsonPrimitive()
		{
			return new JsonPrimitive();
		}


		private static JsonArray OnConstructJsonArray()
		{
			return new JsonArray();
		}


		private static JsonObject OnConstructJsonObject()
		{
			return new JsonObject();
		}


		private static void OnRecycleJsonPrimitive(JsonPrimitive element)
		{
			element.ClearValue();
		}


		private static void OnRecycleJsonArray(JsonArray element)
		{
			element.Clear();
		}


		private static void OnRecycleJsonObject(JsonObject element)
		{
			element.Clear();
		}
	}
}