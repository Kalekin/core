﻿using UnityEngine;
using Core.Debug;


namespace Core.Serialization
{
	public struct JsonVector2 : IJsonSerializable
	{
		public static readonly JsonVector2 Zero = Vector2.zero;
		public static readonly JsonVector2 One = Vector2.one;
		public static readonly JsonVector2 Left = -Vector2.right;
		public static readonly JsonVector2 Right = Vector2.right;
		public static readonly JsonVector2 Up = Vector2.up;
		public static readonly JsonVector2 Down = -Vector2.up;
		private Vector2 _value;




		public static implicit operator Vector2(JsonVector2 vector3)
		{
			return vector3._value;
		}


		public static implicit operator JsonVector2(Vector2 vector3)
		{
			return new JsonVector2
			{
				_value = vector3
			};
		}




		public void FromJsonElement(JsonElement element)
		{
			Assert.IsNotNull(element);
			Assert.IsTrue(element.IsJsonObject);

			JsonObject obj = element.AsJsonObject();
			_value.x = obj["x"].AsFloat();
			_value.y = obj["y"].AsFloat();
		}


		public JsonElement ToJsonElement(JsonPool pool)
		{
			JsonObject obj = pool.GetObject();
			JsonPrimitive x = pool.GetPrimitive();
			JsonPrimitive y = pool.GetPrimitive();

			x.SetValue(_value.x);
			y.SetValue(_value.y);

			obj["x"] = x;
			obj["y"] = y;

			return obj;
		}
	}
}