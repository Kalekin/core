﻿using System;
using Core.Debug;


namespace Core.Serialization
{
	[Serializable]
	public struct JsonDateTime : IJsonSerializable
	{
		public static readonly JsonDateTime Now = DateTime.Now;
		public static readonly JsonDateTime UtcNow = DateTime.UtcNow;
		private DateTime _value;




		public static implicit operator DateTime(JsonDateTime dateTime)
		{
			return dateTime._value;
		}


		public static implicit operator JsonDateTime(DateTime dateTime)
		{
			return new JsonDateTime
			{
				_value = dateTime
			};
		}




		public void FromJsonElement(JsonElement element)
		{
			Assert.IsNotNull(element);
			Assert.IsTrue(element.IsJsonPrimitive);

			JsonPrimitive p = element.AsJsonPrimitive();

			Assert.IsTrue(p.IsString);

			_value = DateTime.Parse(p.AsString()).ToUniversalTime();
		}


		public JsonElement ToJsonElement(JsonPool pool)
		{
			JsonPrimitive ret = pool.GetPrimitive();

			ret.SetValue(DateTime.SpecifyKind(_value, DateTimeKind.Utc).ToString("o"));

			return ret;
		}
	}
}
