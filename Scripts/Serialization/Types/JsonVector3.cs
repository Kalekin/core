﻿using UnityEngine;
using Core.Debug;


namespace Core.Serialization
{
	public struct JsonVector3 : IJsonSerializable
	{
		public static readonly JsonVector3 Zero = Vector3.zero;
		public static readonly JsonVector3 One = Vector3.one;
		public static readonly JsonVector3 Left = Vector3.left;
		public static readonly JsonVector3 Right = Vector3.right;
		public static readonly JsonVector3 Up = Vector3.up;
		public static readonly JsonVector3 Down = Vector3.down;
		public static readonly JsonVector3 Forward = Vector3.forward;
		public static readonly JsonVector3 Backw= Vector3.back;
		private Vector3 _value;




		public static implicit operator Vector3(JsonVector3 vector3)
		{
			return vector3._value;
		}


		public static implicit operator JsonVector3(Vector3 vector3)
		{
			return new JsonVector3
			{
				_value = vector3
			};
		}




		void IJsonSerializable.FromJsonElement(JsonElement element)
		{
			Assert.IsNotNull(element);
			Assert.IsTrue(element.IsJsonObject);

			JsonObject obj = element.AsJsonObject();
			_value.x = obj["x"].AsFloat();
			_value.y = obj["y"].AsFloat();
			_value.z = obj["z"].AsFloat();
		}


		JsonElement IJsonSerializable.ToJsonElement(JsonPool pool)
		{
			JsonObject obj = pool.GetObject();
			JsonPrimitive x = pool.GetPrimitive();
			JsonPrimitive y = pool.GetPrimitive();
			JsonPrimitive z = pool.GetPrimitive();

			x.SetValue(_value.x);
			y.SetValue(_value.y);
			z.SetValue(_value.z);

			obj["x"] = x;
			obj["y"] = y;
			obj["z"] = z;

			return obj;
		}
	}
}
