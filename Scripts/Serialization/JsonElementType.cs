﻿namespace Core.Serialization
{
	public enum JsonElementType
	{
		Null,
		Primitive,
		Array,
		Object
	}
}