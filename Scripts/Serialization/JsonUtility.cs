﻿using System;
using System.Collections.Generic;
using System.Text;
using Core.Debug;


namespace Core.Serialization
{
	public static class JsonUtility
	{
		public static string Format(string json)
		{
			const string indentString = "     ";

			var indent = 0;
			var quoted = false;
			var sb = new StringBuilder();
			for (var i = 0; i < json.Length; i++)
			{
				var ch = json[i];
				switch (ch)
				{
					case '{':
					case '[':
						sb.Append(ch);
						if (!quoted)
						{
							sb.AppendLine();

							indent++;
							for (int c = 0; c < indent; c++)
							{
								sb.Append(indentString);
							}
						}
						break;
					case '}':
					case ']':
						if (!quoted)
						{
							sb.AppendLine();

							indent--;
							for (int c = 0; c < indent; c++)
							{
								sb.Append(indentString);
							}
						}
						sb.Append(ch);
						break;
					case '"':
						sb.Append(ch);
						bool escaped = false;
						var index = i;
						while (index > 0 && json[--index] == '\\')
							escaped = !escaped;
						if (!escaped)
							quoted = !quoted;
						break;
					case ',':
						sb.Append(ch);
						if (!quoted)
						{
							sb.AppendLine();

							for (int c = 0; c < indent; c++)
							{
								sb.Append(indentString);
							}
						}
						break;
					case ':':
						sb.Append(ch);
						if (!quoted)
							sb.Append("");
						break;
					default:
						sb.Append(ch);
						break;
				}
			}
			return sb.ToString();
		}


		public static void ValueFromJsonElement<T>(ref T obj, JsonElement element)
			where T : struct, IJsonSerializable
		{
			obj.FromJsonElement(element);
		}


		public static JsonElement ValueToJsonElement<T>(ref T obj, JsonPool pool)
			where T : struct, IJsonSerializable
		{
			return obj.ToJsonElement(pool);
		}


		public static object ConvertToObject(JsonElement element)
		{
			if (element.IsJsonPrimitive)
			{
				return element.AsJsonPrimitive().Value;
			}
		
			if (element.IsJsonObject)
			{
				JsonObject obj = element.AsJsonObject();
				var ret = new Dictionary<string, object>(obj.Count);

				foreach (var pair in obj)
				{
					ret.Add(pair.Key, ConvertToObject(pair.Value));
				}

				return ret;
			}
			
			if (element.IsJsonArray)
			{
				JsonArray array = element.AsJsonArray();
				var ret = new List<object>(array.Count);

				for (int i = 0; i < array.Count; i++)
				{
					ret.Add(ConvertToObject(array[i]));
				}

				return ret;
			}

			return null;
		}


		public static bool GetFromObject(JsonObject obj, string key, bool defaultValue)
		{
			Assert.IsNotNull(obj);
			Assert.IsNotNull(key);

			JsonElement e;

			if (obj.TryGetValue(key, out e) &&
				e.IsJsonPrimitive)
			{
				JsonPrimitive p = e.AsJsonPrimitive();

				if (p.IsBoolean)
				{
					return p.AsBool();
				}
			}

			return defaultValue;
		}


		public static int GetFromObject(JsonObject obj, string key, int defaultValue)
		{
			Assert.IsNotNull(obj);
			Assert.IsNotNull(key);

			JsonElement e;

			if (obj.TryGetValue(key, out e) &&
				e.IsJsonPrimitive)
			{
				JsonPrimitive p = e.AsJsonPrimitive();

				if (p.IsInteger)
				{
					return p.AsInt();
				}
			}

			return defaultValue;
		}


		public static float GetFromObject(JsonObject obj, string key, float defaultValue)
		{
			Assert.IsNotNull(obj);
			Assert.IsNotNull(key);

			JsonElement e;

			if (obj.TryGetValue(key, out e) &&
				e.IsJsonPrimitive)
			{
				JsonPrimitive p = e.AsJsonPrimitive();

				if (p.IsFloatingPoint)
				{
					return p.AsFloat();
				}
			}

			return defaultValue;
		}


		public static string GetFromObject(JsonObject obj, string key, string defaultValue)
		{
			Assert.IsNotNull(obj);
			Assert.IsNotNull(key);

			JsonElement e;

			if (obj.TryGetValue(key, out e) &&
				e.IsJsonPrimitive)
			{
				JsonPrimitive p = e.AsJsonPrimitive();

				if (p.IsString)
				{
					return p.AsString();
				}
			}

			return defaultValue;
		}


		public static T GetFromObject<T>(JsonObject obj, string key, T defaultValue)
			where T : IJsonSerializable, new()
		{
			Assert.IsNotNull(obj);
			Assert.IsNotNull(key);

			JsonElement e;

			if (obj.TryGetValue(key, out e))
			{
				try
				{
					var ret = new T();

					ret.FromJsonElement(e);

					return ret;
				}
				catch
				{
					
				}
			}

			return defaultValue;
		}


		public static T GetFromObject<T>(JsonObject obj, string key, Func<T> defaultValue, Func<JsonElement, T> constructor)
		{
			Assert.IsNotNull(obj);
			Assert.IsNotNull(key);
			Assert.IsNotNull(constructor);

			JsonElement e;

			if (obj.TryGetValue(key, out e))
			{
				try
				{
					var ret = constructor(e);
					return ret;
				}
				catch (Exception ex)
				{
					UnityEngine.Debug.LogException(ex);
					return defaultValue();
				}
			}

			return defaultValue();
		}
	}
}
