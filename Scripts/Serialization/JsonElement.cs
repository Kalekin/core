﻿using System;
using System.Text;


namespace Core.Serialization
{
	public abstract class JsonElement
	{
		public bool IsJsonNull
		{
			get { return Type == JsonElementType.Null; }
		}


		public bool IsJsonPrimitive
		{
			get { return Type == JsonElementType.Primitive; }
		}


		public bool IsJsonArray
		{
			get { return Type == JsonElementType.Array; }
		}


		public bool IsJsonObject
		{
			get { return Type == JsonElementType.Object; }
		}


		public abstract JsonElementType Type { get; }




		public virtual bool AsBool()
		{
			throw new NotSupportedException(GetType().Name);
		}


		public virtual byte AsByte()
		{
			throw new NotSupportedException(GetType().Name);
		}


		public virtual char AsChar()
		{
			throw new NotSupportedException(GetType().Name);
		}


		public virtual float AsFloat()
		{
			throw new NotSupportedException(GetType().Name);
		}


		public virtual double AsDouble()
		{
			throw new NotSupportedException(GetType().Name);
		}


		public virtual short AsShort()
		{
			throw new NotSupportedException(GetType().Name);
		}


		public virtual int AsInt()
		{
			throw new NotSupportedException(GetType().Name);
		}


		public virtual long AsLong()
		{
			throw new NotSupportedException(GetType().Name);
		}


		public virtual string AsString()
		{
			throw new NotSupportedException(GetType().Name);
		}


		public JsonArray AsJsonArray()
		{
			if (IsJsonArray)
			{
				return this as JsonArray;
			}

			throw new InvalidCastException(string.Format("{0} is not a JsonArray.", GetType()));
		}


		public JsonNull AsJsonNull()
		{
			if (IsJsonNull)
			{
				return this as JsonNull;
			}

			throw new InvalidCastException(string.Format("{0} is not a JsonNull.", GetType()));
		}


		public JsonObject AsJsonObject()
		{
			if (IsJsonObject)
			{
				return this as JsonObject;
			}

			throw new InvalidCastException(string.Format("{0} is not a JsonObject.", GetType()));
		}


		public JsonPrimitive AsJsonPrimitive()
		{
			if (IsJsonPrimitive)
			{
				return this as JsonPrimitive;
			}

			throw new InvalidCastException(string.Format("{0} is not a JsonPrimitive.", GetType()));
		}


		public virtual void Append(StringBuilder builder)
		{
		}
	}
}
