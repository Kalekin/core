﻿using System.Collections;
using System.Collections.Generic;
using System.Text;


namespace Core.Serialization
{
	public sealed class JsonObject : JsonElement, IDictionary<string, JsonElement>
	{
		private readonly Dictionary<string, JsonElement> _elements;
		private bool _isEmpty;




		public int Count
		{
			get { return _elements.Count; }
		}


		bool ICollection<KeyValuePair<string, JsonElement>>.IsReadOnly
		{
			get { return false; }
		}


		ICollection<string> IDictionary<string, JsonElement>.Keys
		{
			get { return _elements.Keys; }
		}


		ICollection<JsonElement> IDictionary<string, JsonElement>.Values
		{
			get { return _elements.Values; }
		}


		public bool IsEmpty
		{
			get
			{
				return Count == 0;
			}
		}


		public override JsonElementType Type
		{
			get { return JsonElementType.Object; }
		}




		public JsonElement this[string key]
		{
			get { return _elements[key]; }
			set { _elements[key] = value; }
		}




		public JsonObject()
		{
			_elements = new Dictionary<string, JsonElement>();
		}


		public JsonObject(int capacity)
		{
			_elements = new Dictionary<string, JsonElement>(capacity);
		}


		public JsonObject(ICollection<KeyValuePair<string, JsonElement>> keyCollection)
		{
			_elements = new Dictionary<string, JsonElement>(keyCollection.Count);

			foreach (var keyValuePair in keyCollection)
			{
				_elements.Add(keyValuePair.Key, keyValuePair.Value);
			}
		}





		public IEnumerator<KeyValuePair<string, JsonElement>> GetEnumerator()
		{
			return _elements.GetEnumerator();
		}


		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}


		void ICollection<KeyValuePair<string, JsonElement>>.Add(KeyValuePair<string, JsonElement> item)
		{
			_elements.Add(item.Key, item.Value);
		}


		public void Clear()
		{
			_elements.Clear();
		}


		bool ICollection<KeyValuePair<string, JsonElement>>.Contains(KeyValuePair<string, JsonElement> item)
		{
			JsonElement value;

			return _elements.TryGetValue(item.Key, out value) &&
			       value == item.Value;
		}


		void ICollection<KeyValuePair<string, JsonElement>>.CopyTo(KeyValuePair<string, JsonElement>[] array, int arrayIndex)
		{
			int index = 0;

			foreach (var keyValuePair in _elements)
			{
				array[arrayIndex + index] = new KeyValuePair<string, JsonElement>(keyValuePair.Key, keyValuePair.Value);
				index++;
			}
		}


		bool ICollection<KeyValuePair<string, JsonElement>>.Remove(KeyValuePair<string, JsonElement> item)
		{
			return _elements.Remove(item.Key);
		}


		public bool ContainsKey(string key)
		{
			return _elements.ContainsKey(key);
		}


		public void Add(string key, JsonElement value)
		{
            _elements.Add(key, value ?? JsonNull.Instance);
		}


		public bool Remove(string key)
		{
			return _elements.Remove(key);
		}


		public bool TryGetValue(string key, out JsonElement value)
		{
			return _elements.TryGetValue(key, out value);
		}


		public override bool Equals(object obj)
		{
			return obj == this || (obj is JsonObject && ((JsonObject) obj)._elements.Equals(_elements));
		}


		public override int GetHashCode()
		{
			return _elements.GetHashCode();
		}


		public override string ToString()
		{
			var builder = new StringBuilder();
			
			Append(builder);

			return builder.ToString();
		}


		public override void Append(StringBuilder builder)
		{
			builder.Append('{');

			int i = 0;

			foreach (var keyValuePair in _elements)
			{
				builder.Append('"');
				builder.Append(keyValuePair.Key);
				builder.Append('"');
				builder.Append(':');
				keyValuePair.Value.Append(builder);

				if (i != _elements.Count - 1)
				{
					builder.Append(',');
				}

				++i;
			}

			builder.Append('}');
		}
	}
}
