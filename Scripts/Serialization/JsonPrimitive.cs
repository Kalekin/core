﻿using System;
using System.Globalization;
using System.Text;


namespace Core.Serialization
{
	public sealed class JsonPrimitive : JsonElement
	{
		private const string _FALSE_STRING = "false";
		private const string _TRUE_STRING = "true";
		private JsonPrimitiveType _primitiveType;
		private bool _boolValue;
		private long _longValue;
		private double _doubleValue;
		private string _stringValue;




		public bool IsBoolean
		{
			get { return _primitiveType == JsonPrimitiveType.Boolean; }
		}


		public bool IsInteger
		{
			get { return _primitiveType == JsonPrimitiveType.Integer; }
		}


		public bool IsFloatingPoint
		{
			get { return _primitiveType == JsonPrimitiveType.FloatingPoint; }
		}


		public bool IsString
		{
			get { return _primitiveType == JsonPrimitiveType.String; }
		}


		public override JsonElementType Type
		{
			get { return JsonElementType.Primitive; }
		}


		public JsonPrimitiveType PrimitiveType
		{
			get { return _primitiveType; }
		}


		public object Value
		{
			get
			{
				switch (_primitiveType)
				{
					case JsonPrimitiveType.Boolean:
						return _boolValue;
					case JsonPrimitiveType.FloatingPoint:
						return _doubleValue;
					case JsonPrimitiveType.Integer:
						return _longValue;
					case JsonPrimitiveType.String:
						return _stringValue;
					default:
						throw new InvalidOperationException("JsonPrimitive has no valid primitive type.");
				}
			}
		}


		public JsonPrimitive()
		{
			ClearValue();
		}


		public JsonPrimitive(bool value)
		{
			SetValue(value);
		}


		public JsonPrimitive(byte value)
		{
			SetValue(value);
		}


		public JsonPrimitive(char value)
		{
			SetValue(value);
		}


		public JsonPrimitive(float value)
		{
			SetValue(value);
		}


		public JsonPrimitive(double value)
		{
			SetValue(value);
		}


		public JsonPrimitive(short value)
		{
			SetValue(value);
		}


		public JsonPrimitive(int value)
		{
			SetValue(value);
		}


		public JsonPrimitive(long value)
		{
			SetValue(value);
		}


		public JsonPrimitive(string value)
		{
			SetValue(value);
		}




		public void ClearValue()
		{
			_primitiveType = JsonPrimitiveType.None;
			_stringValue = null;
		}


		public void SetValue(bool value)
		{
			_primitiveType = JsonPrimitiveType.Boolean;
			_boolValue = value;
		}


		public void SetValue(char value)
		{
			_primitiveType = JsonPrimitiveType.String;
			_stringValue = char.ToString(value);
		}


		public void SetValue(string value)
		{
			_primitiveType = JsonPrimitiveType.String;
			_stringValue = value;
		}


		public void SetValue(byte value)
		{
			_primitiveType = JsonPrimitiveType.Integer;
			_longValue = value;
		}


		public void SetValue(short value)
		{
			_primitiveType = JsonPrimitiveType.Integer;
			_longValue = value;
		}


		public void SetValue(int value)
		{
			_primitiveType = JsonPrimitiveType.Integer;
			_longValue = value;
		}


		public void SetValue(long value)
		{
			_primitiveType = JsonPrimitiveType.Integer;
			_longValue = value;
		}


		public void SetValue(float value)
		{
			_primitiveType = JsonPrimitiveType.FloatingPoint;
			_doubleValue = value;
		}


		public void SetValue(double value)
		{
			_primitiveType = JsonPrimitiveType.FloatingPoint;
			_doubleValue = value;
		}


		public override bool AsBool()
		{
			if (!IsBoolean)
			{
				throw new InvalidOperationException(string.Format("Tried to access json primitive bool value but primitive is of type {0}", PrimitiveType));
			}

			return _boolValue;
		}


		public override byte AsByte()
		{
			if (!IsInteger)
			{
				throw new InvalidOperationException(string.Format("Tried to access json primitive integer value but primitive is of type {0}", PrimitiveType));
			}
			return (byte) _longValue;
		}


		public override short AsShort()
		{
			if (!IsInteger)
			{
				throw new InvalidOperationException(string.Format("Tried to access json primitive integer value but primitive is of type {0}", PrimitiveType));
			}
			return (short) _longValue;
		}


		public override int AsInt()
		{
			if (!IsInteger)
			{
				throw new InvalidOperationException(string.Format("Tried to access json primitive integer value but primitive is of type {0}", PrimitiveType));
			}

			return (int) _longValue;
		}


		public override long AsLong()
		{
			if (!IsInteger)
			{
				throw new InvalidOperationException(string.Format("Tried to access json primitive integer value but primitive is of type {0}", PrimitiveType));
			}

			return _longValue;
		}


		public override float AsFloat()
		{
			if (IsFloatingPoint)
			{
				return (float)_doubleValue;
			}
			
			if (IsInteger)
			{
				return _longValue;
			}

			throw new InvalidOperationException(string.Format("Tried to access json primitive floating-point value but primitive is of type {0}", PrimitiveType));

		}


		public override double AsDouble()
		{
			if (IsFloatingPoint)
			{
				return _doubleValue;
			}

			if (IsInteger)
			{
				return _longValue;
			}

			throw new InvalidOperationException(string.Format("Tried to access json primitive floating-point value but primitive is of type {0}", PrimitiveType));
		}


		public override char AsChar()
		{
			if (!IsString)
			{
				throw new InvalidOperationException(string.Format("Tried to access json primitive string value but primitive is of type {0}", PrimitiveType));
			}

			if (_stringValue.Length != 0)
			{
				throw new InvalidOperationException("Cannot access json primitive char value from an empty string");
			}

			return _stringValue[0];
		}


		public override string AsString()
		{
			if (!IsString)
			{
				throw new InvalidOperationException(string.Format("Tried to access json primitive string value but primitive is of type {0}", PrimitiveType));
			}

			return _stringValue;
		}


		public override string ToString()
		{
			switch (PrimitiveType)
			{
				case JsonPrimitiveType.Boolean:
					return _boolValue ? _TRUE_STRING : _FALSE_STRING;
				case JsonPrimitiveType.String:
					return '"' + _stringValue + '"';
				case JsonPrimitiveType.FloatingPoint:
					return _doubleValue.ToString(CultureInfo.InvariantCulture);
				case JsonPrimitiveType.Integer:
					return _longValue.ToString(CultureInfo.InvariantCulture);
			}

			return string.Empty;
		}


		public override void Append(StringBuilder builder)
		{
			switch (PrimitiveType)
			{
				case JsonPrimitiveType.Boolean:
					builder.Append(_boolValue ? _TRUE_STRING : _FALSE_STRING);
					break;
				case JsonPrimitiveType.String:
					builder.Append('"');
					builder.Append(_stringValue);
					builder.Append('"');
					break;
				case JsonPrimitiveType.FloatingPoint:
					builder.Append(_doubleValue);
					break;
				case JsonPrimitiveType.Integer:
					builder.Append(_longValue);
					break;
			}
		}
	}


	public enum JsonPrimitiveType
	{
		None,
		Boolean,
		Integer,
		FloatingPoint,
		String
	}
}
