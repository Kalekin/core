﻿using System.Text;


namespace Core.Serialization
{
	public sealed class JsonNull : JsonElement
	{
		public static readonly string NullString = "null";
		public static readonly JsonNull Instance = new JsonNull();




		public override JsonElementType Type
		{
			get { return JsonElementType.Null; }
		}




		public override bool Equals(object obj)
		{
			return this == obj || obj is JsonNull;
		}


		public override int GetHashCode()
		{
			return GetType().GetHashCode();
		}


		public override string AsString()
		{
			return null;
		}


		public override string ToString()
		{
			return NullString;
		}


		public override void Append(StringBuilder builder)
		{
			builder.Append(NullString);
		}
	}
}
