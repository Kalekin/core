﻿namespace Core.Serialization
{
	public interface IJsonSerializable
	{
		void FromJsonElement(JsonElement element);


		JsonElement ToJsonElement(JsonPool pool);
	}
}
