﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;


namespace Core.Serialization
{
	public sealed class JsonArray : JsonElement, IList<JsonElement>
	{
		private readonly List<JsonElement> _elements;
 



		public JsonElement this[int index]
		{
			get { return _elements[index]; }
			set { _elements[index] = value; }
		}


		public override JsonElementType Type
		{
			get { return JsonElementType.Array; }
		}


		public int Count
		{
			get { return _elements.Count; }
		}


		public bool IsReadOnly
		{
			get { return false; }
		}


		public int Capacity
		{
			get { return _elements.Capacity; }
			set { _elements.Capacity = value; }
		}




		public JsonArray()
		{
			_elements = new List<JsonElement>();
		}


		public JsonArray(int capacity)
		{
			_elements = new List<JsonElement>(capacity);
		}


        public JsonArray(IEnumerable<JsonElement> values)
		{
            _elements = new List<JsonElement>();

            foreach (var value in values)
            {
                Add(value);
            }
		}




		public int IndexOf(JsonElement item)
		{
			return _elements.IndexOf(item);
		}


		public void Insert(int index, JsonElement value)
		{
            _elements.Insert(index, value ?? JsonNull.Instance);
		}


		public void RemoveAt(int index)
		{
			_elements.RemoveAt(index);
		}


		public void Add(JsonElement value)
		{
			_elements.Add(value);
		}


		public void Clear()
		{
			_elements.Clear();
		}


		public bool Contains(JsonElement item)
		{
			return _elements.Contains(item);
		}


		public void CopyTo(JsonElement[] array, int arrayIndex)
		{
			_elements.CopyTo(array, arrayIndex);
		}


		public bool Remove(JsonElement item)
		{
			return _elements.Remove(item);
		}


		public void AddRange(JsonArray array)
		{
			_elements.AddRange(array);
		}


		public IEnumerator<JsonElement> GetEnumerator()
		{
			return _elements.GetEnumerator();
		}


		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}


		public override bool AsBool()
		{
			if (_elements.Count == 1)
			{
				return _elements[0].AsBool();
			}

			throw new InvalidOperationException();
		}


		public override byte AsByte()
		{
			if (_elements.Count == 1)
			{
				return _elements[0].AsByte();
			}

			throw new InvalidOperationException();
		}


		public override char AsChar()
		{
			if (_elements.Count == 1)
			{
				return _elements[0].AsChar();
			}

			throw new InvalidOperationException();
		}


		public override float AsFloat()
		{
			if (_elements.Count == 1)
			{
				return _elements[0].AsFloat();
			}

			throw new InvalidOperationException();
		}


		public override double AsDouble()
		{
			if (_elements.Count == 1)
			{
				return _elements[0].AsDouble();
			}

			throw new InvalidOperationException();
		}


		public override short AsShort()
		{
			if (_elements.Count == 1)
			{
				return _elements[0].AsShort();
			}

			throw new InvalidOperationException();
		}


		public override int AsInt()
		{
			if (_elements.Count == 1)
			{
				return _elements[0].AsInt();
			}

			throw new InvalidOperationException();
		}


		public override long AsLong()
		{
			if (_elements.Count == 1)
			{
				return _elements[0].AsLong();
			}

			throw new InvalidOperationException();
		}


		public override string AsString()
		{
			if (_elements.Count == 1)
			{
				return _elements[0].AsString();
			}

			throw new InvalidOperationException();
		}


		public int AsBoolArray(bool[] array)
		{
			int toCopy = _elements.Count < array.Length ? _elements.Count : array.Length;

			for (int i = 0; i < toCopy; i++)
			{
				array[i] = _elements[i].AsBool();
			}

			return toCopy;
		}


		public int AsByteArray(byte[] array)
		{
			int toCopy = _elements.Count < array.Length ? _elements.Count : array.Length;

			for (int i = 0; i < toCopy; i++)
			{
				array[i] = _elements[i].AsByte();
			}

			return toCopy;
		}


		public int AsCharArray(char[] array)
		{
			int toCopy = _elements.Count < array.Length ? _elements.Count : array.Length;

			for (int i = 0; i < toCopy; i++)
			{
				array[i] = _elements[i].AsChar();
			}

			return toCopy;
		}


		public int AsFloatArray(float[] array)
		{
			int toCopy = _elements.Count < array.Length ? _elements.Count : array.Length;

			for (int i = 0; i < toCopy; i++)
			{
				array[i] = _elements[i].AsFloat();
			}

			return toCopy;
		}


		public int AsDoubleArray(double[] array)
		{
			int toCopy = _elements.Count < array.Length ? _elements.Count : array.Length;

			for (int i = 0; i < toCopy; i++)
			{
				array[i] = _elements[i].AsDouble();
			}

			return toCopy;
		}


		public int AsShortArray(short[] array)
		{
			int toCopy = _elements.Count < array.Length ? _elements.Count : array.Length;

			for (int i = 0; i < toCopy; i++)
			{
				array[i] = _elements[i].AsShort();
			}

			return toCopy;
		}


		public int AsIntArray(int[] array)
		{
			int toCopy = _elements.Count < array.Length ? _elements.Count : array.Length;

			for (int i = 0; i < toCopy; i++)
			{
				array[i] = _elements[i].AsInt();
			}

			return toCopy;
		}


		public int AsLongArray(long[] array)
		{
			int toCopy = _elements.Count < array.Length ? _elements.Count : array.Length;

			for (int i = 0; i < toCopy; i++)
			{
				array[i] = _elements[i].AsLong();
			}

			return toCopy;
		}


		public int AsStringArray(string[] array)
		{
			int toCopy = _elements.Count < array.Length ? _elements.Count : array.Length;

			for (int i = 0; i < toCopy; i++)
			{
				array[i] = _elements[i].AsString();
			}

			return toCopy;
		}


		public void AsBoolList(IList<bool> list)
		{
			for (int i = 0; i < _elements.Count; i++)
			{
				list.Add(_elements[i].AsBool());
			}
		}


		public void AsByteList(IList<byte> list)
		{
			for (int i = 0; i < _elements.Count; i++)
			{
				list.Add(_elements[i].AsByte());
			}
		}


		public void AsCharList(IList<char> list)
		{
			for (int i = 0; i < _elements.Count; i++)
			{
				list.Add(_elements[i].AsChar());
			}
		}


		public void AsFloatList(IList<float> list)
		{
			for (int i = 0; i < _elements.Count; i++)
			{
				list.Add(_elements[i].AsFloat());
			}
		}


		public void AsDoubleList(IList<double> list)
		{
			for (int i = 0; i < _elements.Count; i++)
			{
				list.Add(_elements[i].AsDouble());
			}
		}


		public void AsShortList(IList<short> list)
		{
			for (int i = 0; i < _elements.Count; i++)
			{
				list.Add(_elements[i].AsShort());
			}
		}


		public void AsIntList(IList<int> list)
		{
			for (int i = 0; i < _elements.Count; i++)
			{
				list.Add(_elements[i].AsInt());
			}
		}


		public void AsLongList(IList<long> list)
		{
			for (int i = 0; i < _elements.Count; i++)
			{
				list.Add(_elements[i].AsLong());
			}
		}


		public void AsStringList(IList<string> list)
		{
			for (int i = 0; i < _elements.Count; i++)
			{
				list.Add(_elements[i].AsString());
			}
		}


		public override bool Equals(object obj)
		{
			return obj == this || (obj is JsonArray && ((JsonArray) obj)._elements.Equals(_elements));
		}


		public override int GetHashCode()
		{
			return _elements.GetHashCode();
		}


		public override string ToString()
		{
			var builder = new StringBuilder();

			Append(builder);

			return builder.ToString();
		}


		public override void Append(StringBuilder builder)
		{
			builder.Append('[');

		    if (_elements.Count > 0)
		    {
                _elements[0].Append(builder);

                for (int i = 1; i < _elements.Count; ++i)
                {
                    builder.Append(',');
                    _elements[i].Append(builder);
                }
		    }

			builder.Append(']');
		}
	}
}
