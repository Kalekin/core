﻿using System;
using System.Text;
using Core.Debug;


namespace Core.Serialization
{
	public class JsonParser
	{
		private JsonPool _pool;
		private StringBuilder _stringBuilder;
		private JsonStringReader _jsonReader;




		public JsonParser()
		{
			_pool = new JsonPool();
			_stringBuilder = new StringBuilder();
			_jsonReader = new JsonStringReader();
		}




		public JsonElement Create(string json)
		{
			if (string.IsNullOrEmpty(json))
			{
				return JsonNull.Instance;
			}

			_jsonReader.Initialize(json);
			JsonElement ret = ParseValue(_jsonReader, _pool, _stringBuilder);
			_jsonReader.Reset();

			return ret;
		}


		public JsonElement Create<T>(T jsonSerializable)
			where T : class, IJsonSerializable
		{
			Assert.IsNotNull(jsonSerializable);

			return jsonSerializable.ToJsonElement(_pool);
		}


		public JsonElement Create<T>(ref T jsonSerializable)
		   where T : struct, IJsonSerializable
		{
			return jsonSerializable.ToJsonElement(_pool);
		}


		public string Serialize(JsonElement element)
		{
			Assert.IsNotNull(element);

			element.Append(_stringBuilder);
			string ret = _stringBuilder.ToString();
			_stringBuilder.Length = 0;

			return ret;
		}


		public void Recycle(JsonElement element)
		{
			Assert.IsNotNull(element);

			_pool.Recycle(element);
		}


		private static JsonElement ParseByToken(JsonStringReader jsonReader, JsonPool pool, StringBuilder stringBuilder, JsonToken token)
		{
			switch (token)
			{
				case JsonToken.String:
					return ParseString(jsonReader, pool, stringBuilder);
				case JsonToken.Number:
					return ParseNumber(jsonReader, pool);
				case JsonToken.CurlyOpen:
					return ParseObject(jsonReader, pool, stringBuilder);
				case JsonToken.SquaredOpen:
					return ParseArray(jsonReader, pool, stringBuilder);
				case JsonToken.True:
					JsonPrimitive retTrue = pool.GetPrimitive();
					retTrue.SetValue(true);
					return retTrue;
				case JsonToken.False:
					JsonPrimitive retFalse = pool.GetPrimitive();
					retFalse.SetValue(false);
					return retFalse;
				case JsonToken.Null:
					return JsonNull.Instance;
				default:
					return JsonNull.Instance;
			}
		}


		private static JsonPrimitive ParseNumber(JsonStringReader jsonReader, JsonPool pool)
		{
			string number = jsonReader.ReadWord();

			if (number.IndexOf('.') == -1)
			{
				long parsedInt;
				Int64.TryParse(number, out parsedInt);
				JsonPrimitive retInt = pool.GetPrimitive();
				retInt.SetValue(parsedInt);
				return retInt;
			}

			double parsedDouble;
			Double.TryParse(number, out parsedDouble);
			JsonPrimitive ret = pool.GetPrimitive();
			ret.SetValue(parsedDouble);
			return ret;
		}


		private static JsonElement ParseArray(JsonStringReader jsonReader, JsonPool pool, StringBuilder stringBuilder)
		{
			var array = new JsonArray();

			// ditch opening bracket
			jsonReader.Read();

			// [
			var parsing = true;
			while (parsing)
			{
				JsonToken nextToken = GetNextToken(jsonReader);

				switch (nextToken)
				{
					case JsonToken.None:
						return JsonNull.Instance;
					case JsonToken.Comma:
						continue;
					case JsonToken.SquaredClose:
						parsing = false;
						break;
					default:
						array.Add(ParseByToken(jsonReader, pool, stringBuilder, nextToken));
						break;
				}
			}

			return array;
		}


		private static JsonElement ParseObject(JsonStringReader jsonReader, JsonPool pool, StringBuilder stringBuilder)
		{
			JsonObject table = pool.GetObject();
			jsonReader.Read();

			while (true)
			{
				switch (GetNextToken(jsonReader))
				{
					case JsonToken.None:
						return JsonNull.Instance;
					case JsonToken.Comma:
						continue;
					case JsonToken.CurlyClose:
						return table;
					default:

						string name = ReadString(jsonReader, stringBuilder);

						if (name == null)
						{
							return JsonNull.Instance;
						}

						if (GetNextToken(jsonReader) != JsonToken.Colon)
						{
							return JsonNull.Instance;
						}

						jsonReader.Read();

						table[name] = ParseValue(jsonReader, pool, stringBuilder);
						break;
				}
			}
		}


		private static JsonElement ParseValue(JsonStringReader jsonReader, JsonPool pool, StringBuilder stringBuilder)
		{
			JsonToken nextToken = GetNextToken(jsonReader);
			return ParseByToken(jsonReader, pool, stringBuilder, nextToken);
		}


		private static JsonPrimitive ParseString(JsonStringReader jsonReader, JsonPool pool, StringBuilder stringBuilder)
		{
			JsonPrimitive ret = pool.GetPrimitive();
			ret.SetValue(ReadString(jsonReader, stringBuilder));
			return ret;
		}


		private static string ReadString(JsonStringReader jsonReader, StringBuilder stringBuilder)
		{
			// ditch opening quote
			jsonReader.Read();
			bool parsing = true;

			while (parsing)
			{
				if (jsonReader.Peek() == -1)
				{
					break;
				}

				char c = jsonReader.ReadChar();
				switch (c)
				{
					case '"':
						parsing = false;
						break;
					case '\\':
						if (jsonReader.Peek() == -1)
						{
							parsing = false;
							break;
						}

						c = jsonReader.ReadChar();
						switch (c)
						{
							case '"':
							case '\\':
							case '/':
								stringBuilder.Append(c);
								break;
							case 'b':
								stringBuilder.Append('\b');
								break;
							case 'f':
								stringBuilder.Append('\f');
								break;
							case 'n':
								stringBuilder.Append('\n');
								break;
							case 'r':
								stringBuilder.Append('\r');
								break;
							case 't':
								stringBuilder.Append('\t');
								break;
							case 'u':
								var hex = new char[4];

								for (int i = 0; i < 4; i++)
								{
									hex[i] = jsonReader.ReadChar();
								}

								stringBuilder.Append((char)Convert.ToInt32(new string(hex), 16));
								break;
						}
						break;
					default:
						stringBuilder.Append(c);
						break;
				}
			}

			string ret = stringBuilder.ToString();
			stringBuilder.Length = 0;
			return ret;
		}


		private static JsonToken GetNextToken(JsonStringReader reader)
		{
			reader.EatWhiteSpace();

			if (reader.Peek() == -1)
			{
				return JsonToken.None;
			}

			switch (reader.PeekChar())
			{
				case '{':
					return JsonToken.CurlyOpen;
				case '}':
					reader.Read();
					return JsonToken.CurlyClose;
				case '[':
					return JsonToken.SquaredOpen;
				case ']':
					reader.Read();
					return JsonToken.SquaredClose;
				case ',':
					reader.Read();
					return JsonToken.Comma;
				case '"':
					return JsonToken.String;
				case ':':
					return JsonToken.Colon;
				case '0':
				case '1':
				case '2':
				case '3':
				case '4':
				case '5':
				case '6':
				case '7':
				case '8':
				case '9':
				case '-':
					return JsonToken.Number;
			}

			switch (reader.ReadWord())
			{
				case "false":
					return JsonToken.False;
				case "true":
					return JsonToken.True;
				case "null":
					return JsonToken.Null;
			}

			return JsonToken.None;
		}




		private class JsonStringReader
		{
			private const string _WORD_BREAK = "{}[],:\"";
			private StringBuilder _stringBuilder;
			private string _json;
			private int _pos;
			private int _length;




			public void Initialize(string json)
			{
				Assert.IsNotNull(json);
				_json = json;
				_length = _json.Length;
				_stringBuilder = new StringBuilder();
			}


			public void Reset()
			{
				_json = null;
				_pos = 0;
				_length = 0;
			}


			public void EatWhiteSpace()
			{
				while (Char.IsWhiteSpace(PeekChar()))
				{
					Read();

					if (Peek() == -1)
					{
						break;
					}
				}
			}


			public int Peek()
			{
				if (_pos == _length) return -1;

				return _json[_pos];
			}


			public char PeekChar()
			{
				return Convert.ToChar(Peek());
			}


			public int Read()
			{
				if (_pos == _length) return -1;

				return _json[_pos++];
			}


			public char ReadChar()
			{
				return Convert.ToChar(Read());
			}


			public string ReadWord()
			{
				while (!IsWordBreak(PeekChar()))
				{
					_stringBuilder.Append(ReadChar());

					if (Peek() == -1)
					{
						break;
					}
				}

				string ret = _stringBuilder.ToString();
				_stringBuilder.Length = 0;
				return ret;
			}


			private static bool IsWordBreak(char c)
			{
				return Char.IsWhiteSpace(c) || _WORD_BREAK.IndexOf(c) != -1;
			}
		}


		private enum JsonToken
		{
			None,
			CurlyOpen,
			CurlyClose,
			SquaredOpen,
			SquaredClose,
			Colon,
			Comma,
			String,
			Number,
			True,
			False,
			Null
		};
	}
}
