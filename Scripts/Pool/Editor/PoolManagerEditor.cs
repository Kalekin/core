﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;


namespace Core.Pooling.Editor
{
	/*
	[CustomEditor(typeof(PoolManager))]
	public class PoolManagerEditor : UnityEditor.Editor
	{
		private SerializedProperty _poolInfoCategories;
		private List<bool> _expandedStates;




		private void OnEnable()
		{
			_poolInfoCategories = serializedObject.FindProperty("m_PoolInfoCategories");
		}


		public override void OnInspectorGUI()
		{
			foreach (var categoryElement in _poolInfoCategories)
			{
				DrawCategory((SerializedProperty)categoryElement);
			}

			if (GUILayout.Button("Add Category"))
			{
				_poolInfoCategories.InsertArrayElementAtIndex(_poolInfoCategories.arraySize);

				SerializedProperty poolInfoCategoryProperty =
					_poolInfoCategories.GetArrayElementAtIndex(_poolInfoCategories.arraySize - 1);

				poolInfoCategoryProperty.FindPropertyRelative("Category").stringValue = "Unnamed Category";
			}

			serializedObject.ApplyModifiedProperties();
		}


		private static void DrawCategory(SerializedProperty categoryProperty)
		{
			SerializedProperty categoryNameProperty = categoryProperty.FindPropertyRelative("Category");
			SerializedProperty infosProperty = categoryProperty.FindPropertyRelative("Infos");

			Rect r = GUILayoutUtility.GetRect(0f, 50f);
			categoryNameProperty.isExpanded = EditorGUI.Foldout(r, categoryNameProperty.isExpanded, string.Empty, _CategoryFoldoutStyle);

			if (string.IsNullOrEmpty(categoryNameProperty.stringValue))
			{
				GUI.Label(r, categoryNameProperty.stringValue, _CategoryNameStyle);
			}
			else
			{
				categoryNameProperty.stringValue = GUILayout.TextArea(categoryNameProperty.stringValue, 20, _CategoryNameStyle);
			}
		}




		private static readonly GUIStyle _CategoryFoldoutStyle = new GUIStyle
			{

			};

		private static readonly GUIStyle _CategoryNameStyle = new GUIStyle
		{

		};
	}
	 */
}
