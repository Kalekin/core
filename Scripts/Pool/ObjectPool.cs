using System;
using System.Collections.Generic;
using Core.Debug;


namespace Core.Pooling
{
	public class ObjectPool<T> 
	{
		private Stack<T> _stack;
		private Func<T> _constructor;
 		private Action<T> _onGet;
		private Action<T> _onRelease;




		public int CountAll
		{
			get;
			private set;
		}


		public int CountActive
		{
			get
			{
				return CountAll - CountPooled;
			}
		}


		public int CountPooled
		{
			get
			{
				return _stack.Count;
			}
		}




		public ObjectPool(Func<T> constructor, Action<T> onGet, Action<T> onRelease)
		{
			Assert.IsNotNull(constructor);

			_stack = new Stack<T>();
			_constructor = constructor;
			_onGet = onGet;
			_onRelease = onRelease;
		}




		public T Get()
		{
			T t;

			if (_stack.Count == 0)
			{
				t = _constructor();
				CountAll++;
			}
			else
			{
				t = _stack.Pop();
			}

			if (_onGet != null)
			{
				_onGet(t);
			}

			return t;
		}


		public void Release(T obj)
		{
			if (_onRelease != null)
			{
				_onRelease(obj);
			}

			_stack.Push(obj);
		}


		public void Clear()
		{
			Assert.IsTrue(CountActive == 0, "Cannot clear while pool objects are active.");

			_stack.Clear();
		}
	}
}