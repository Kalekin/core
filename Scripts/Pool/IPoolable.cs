namespace Core.Pooling
{
	public interface IPoolable
	{
		void OnSpawn(PoolManager pool);


		void OnRecycle(PoolManager pool);
	}
}