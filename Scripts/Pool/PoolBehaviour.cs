﻿using UnityEngine;


namespace Core.Pooling
{
	public abstract class PoolBehaviour : MonoBehaviour, IPoolable
	{
		protected virtual void Awake()
		{
			OnBirth();
		}


		protected virtual void OnDestroy()
		{
			OnDeath();
		}




		protected virtual void OnSpawn(PoolManager pool)
		{
			OnBirth();
		}


		protected virtual void OnRecycle(PoolManager pool)
		{
			OnDeath();
		}


		void IPoolable.OnSpawn(PoolManager pool)
		{
			OnSpawn(pool);
		}


		void IPoolable.OnRecycle(PoolManager pool)
		{
			OnRecycle(pool);
		}


		protected virtual void OnBirth()
		{
			
		}


		protected virtual void OnDeath()
		{
			
		}
	}
}
