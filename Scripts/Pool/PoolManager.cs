﻿using System;
using System.Collections;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
using Core.Pooling.Editor;
#endif
using UnityEngine;
using Core.Debug;


namespace Core.Pooling
{
	public class PoolManager : MonoBehaviour, ISerializationCallbackReceiver
#if UNITY_EDITOR
		, IPoolManager
#endif
	{
		private static readonly PoolInfoCategory[] _EmptyCategories = new PoolInfoCategory[0];
		[SerializeField] private int m_DefaultPrePoolCount = 10;
		[SerializeField] private PoolInfoCategory[] m_PoolInfoCategories = default (PoolInfoCategory[]);
		private Dictionary<GameObject, List<GameObject>> _instances;
		private Dictionary<GameObject, PoolInfo> _instanceToPoolInfos;
		private Dictionary<GameObject, PoolInfo> _prefabToPoolInfos;
		private Dictionary<GameObject, Component[]> _poolables;



#if UNITY_EDITOR
		PoolInfoCategory[] IPoolManager.PoolInfoCategories
		{
			get { return m_PoolInfoCategories; }
			set { m_PoolInfoCategories = value; }
		}
#endif



		void ISerializationCallbackReceiver.OnAfterDeserialize()
		{
			int prefabs = 0;
			int capacity = 0;

			if (m_PoolInfoCategories == null)
			{
				m_PoolInfoCategories = _EmptyCategories;
			}

			for (int i = 0; i < m_PoolInfoCategories.Length; i++)
			{
				for (int j = 0; j < m_PoolInfoCategories[i].Infos.Length; j++)
				{
					capacity += m_PoolInfoCategories[i].Infos[j].PrePoolCount;
				}

				prefabs += m_PoolInfoCategories[i].Infos.Length;
			}

			_instances = new Dictionary<GameObject, List<GameObject>>(capacity);
			_instanceToPoolInfos = new Dictionary<GameObject, PoolInfo>(capacity);
			_prefabToPoolInfos = new Dictionary<GameObject, PoolInfo>(prefabs);
			_poolables = new Dictionary<GameObject, Component[]>(capacity);

			for (int i = 0; i < m_PoolInfoCategories.Length; i++)
			{
				for (int j = 0; j < m_PoolInfoCategories[i].Infos.Length; j++)
				{
					_instances.Add(m_PoolInfoCategories[i].Infos[j].Prefab, m_PoolInfoCategories[i].Infos[j].Instances);
					_prefabToPoolInfos.Add(m_PoolInfoCategories[i].Infos[j].Prefab, m_PoolInfoCategories[i].Infos[j]);

					for (int k = 0; k < m_PoolInfoCategories[i].Infos[j].Instances.Count; k++)
					{
						GameObject instance = m_PoolInfoCategories[i].Infos[j].Instances[k];
						_instanceToPoolInfos.Add(instance, m_PoolInfoCategories[i].Infos[j]);
						_poolables.Add(instance, m_PoolInfoCategories[i].Infos[j].Poolables[k].Elements);
						
					}
				}
				
			}
		}


		void ISerializationCallbackReceiver.OnBeforeSerialize()
		{
		}




		private PoolInfo AddPoolInfoForPrefab(GameObject prefab)
		{
			var container = new GameObject(prefab.name).GetComponent<Transform>();
			container.SetParent(transform);

			var ret = new PoolInfo(prefab, container, m_DefaultPrePoolCount);

			while (ret.Instances.Count < ret.PrePoolCount)
			{
				#if UNITY_EDITOR
				var obj = ((GameObject)PrefabUtility.InstantiatePrefab(ret.Prefab));
				#else
				var obj = Instantiate(ret.Prefab);
				#endif
				obj.SetActive(false);
				obj.transform.SetParent(container, false);
				obj.name = string.Format("{0} (Pooled)", ret.Prefab.name);
				ret.Instances.Add(obj);
				ret.Poolables.Add(new PoolInfo.PoolableCotainer { Elements = obj.GetComponentsInChildren(typeof(IPoolable), true) });
			}

			_instances.Add(prefab, ret.Instances);
			_prefabToPoolInfos.Add(prefab, ret);

			return ret;
		}


		public GameObject Spawn(GameObject prefab)
		{
			Assert.IsNotNull(prefab);

			PoolInfo poolInfo;

			if (!_prefabToPoolInfos.TryGetValue(prefab, out poolInfo))
			{
				poolInfo = AddPoolInfoForPrefab(prefab);
			}

			GameObject obj;
			Component[] poolables;

			if (poolInfo.Instances.Count > 0)
			{
				obj = poolInfo.Instances[poolInfo.Instances.Count - 1];
				poolInfo.Instances.RemoveAt(poolInfo.Instances.Count - 1);
				poolables = _poolables[obj];
			}
			else
			{
				obj = Instantiate(prefab);
				obj.name = string.Format("{0} (Pooled)", poolInfo.Prefab.name);
				poolables = obj.GetComponentsInChildren(typeof(IPoolable), true);
				_poolables.Add(obj, poolables);
				_instanceToPoolInfos.Add(obj, _prefabToPoolInfos[prefab]);
			}

			obj.transform.SetParent(null);
			obj.SetActive(true);

			for (int i = 0; i < poolables.Length; i++)
			{
				Component component = poolables[i];
				((IPoolable)component).OnSpawn(this);
			}

			return obj;
		}


		public T Spawn<T>(T prefab)
			where T : Component
		{
			Assert.IsNotNull(prefab);

			return Spawn(prefab.gameObject).GetComponent<T>();
		}


		public void SpawnGroup(GameObject prefab, int count, List<GameObject> outCollection)
		{
			Assert.IsNotNull(prefab);
			Assert.IsTrue(count > 0);
			Assert.IsNotNull(outCollection);

			PoolInfo poolInfo;

			if (!_prefabToPoolInfos.TryGetValue(prefab, out poolInfo))
			{
				poolInfo = AddPoolInfoForPrefab(prefab);
			}

			int pooledInstanceCount = Mathf.Min(count, poolInfo.Instances.Count);
			int remainingCount = count - pooledInstanceCount;

			for (int c = 1; c <= pooledInstanceCount; ++c)
			{
				GameObject obj = poolInfo.Instances[poolInfo.Instances.Count - c];
				Component[] poolables = _poolables[obj];

				obj.transform.SetParent(null);
				obj.SetActive(true);

				for (int i = 0; i < poolables.Length; i++)
				{
					Component component = poolables[i];
					((IPoolable)component).OnSpawn(this);
				}

				outCollection.Add(obj);
			}

			poolInfo.Instances.RemoveRange(poolInfo.Instances.Count - pooledInstanceCount, pooledInstanceCount);

			while (remainingCount > 0)
			{
				var obj = Instantiate(prefab);
				obj.name = string.Format("{0} (Pooled)", poolInfo.Prefab.name);
				Component[] poolables = obj.GetComponentsInChildren(typeof(IPoolable), true);
				_poolables.Add(obj, poolables);
				_instanceToPoolInfos.Add(obj, _prefabToPoolInfos[prefab]);

				obj.SetActive(true); 
				
				for (int i = 0; i < poolables.Length; i++)
				{
					Component component = poolables[i];
					((IPoolable)component).OnSpawn(this);
				}

				outCollection.Add(obj);
				remainingCount--;
			}
		}


		public void SpawnGroup<T>(T prefab, int count, List<T> outCollection)
			where T : Component
		{
			Assert.IsNotNull(prefab);
			Assert.IsTrue(count > 0);
			Assert.IsNotNull(outCollection);

			GameObject go = prefab.gameObject;
			PoolInfo poolInfo;

			if (!_prefabToPoolInfos.TryGetValue(go, out poolInfo))
			{
				poolInfo = AddPoolInfoForPrefab(go);
			}

			int pooledInstanceCount = Mathf.Min(count, poolInfo.Instances.Count);
			int remainingCount = count - pooledInstanceCount;

			for (int c = 1; c <= pooledInstanceCount; ++c)
			{
				GameObject obj = poolInfo.Instances[poolInfo.Instances.Count - c];
				Component[] poolables = _poolables[obj];

				obj.transform.SetParent(null);
				obj.SetActive(true);

				for (int i = 0; i < poolables.Length; i++)
				{
					Component component = poolables[i];
					((IPoolable)component).OnSpawn(this);
				}

				outCollection.Add(obj.GetComponent<T>());
			}

			poolInfo.Instances.RemoveRange(poolInfo.Instances.Count - pooledInstanceCount, pooledInstanceCount);

			while (remainingCount > 0)
			{
				var obj = Instantiate(go);
				obj.name = string.Format("{0} (Pooled)", poolInfo.Prefab.name);
				Component[] poolables = obj.GetComponentsInChildren(typeof(IPoolable), true);
				_poolables.Add(obj, poolables);
				_instanceToPoolInfos.Add(obj, _prefabToPoolInfos[go]);

				obj.SetActive(true);

				for (int i = 0; i < poolables.Length; i++)
				{
					Component component = poolables[i];
					((IPoolable)component).OnSpawn(this);
				}

				outCollection.Add(obj.GetComponent<T>());
				remainingCount--;
			}
		}


		public Coroutine SpawnGroupAtRate(GameObject prefab, int count, List<GameObject> outCollection, int rate)
		{
			Assert.IsNotNull(prefab);
			Assert.IsTrue(count > 0);
			Assert.IsNotNull(outCollection);
			Assert.IsTrue(rate > 0);

			return StartCoroutine(SpawnGroupAtRateRoutine(prefab, count, outCollection, rate, null));
		}


		public Coroutine SpawnGroupAtRate(GameObject prefab, int count, List<GameObject> outCollection, int rate,
			Action<GameObject> onSpawnTick)
		{
			Assert.IsNotNull(prefab);     
			Assert.IsTrue(count > 0);
			Assert.IsNotNull(outCollection);
			Assert.IsTrue(rate > 0);

			return StartCoroutine(SpawnGroupAtRateRoutine(prefab, count, outCollection, rate, onSpawnTick));
		}


		private IEnumerator SpawnGroupAtRateRoutine(GameObject prefab, int count, List<GameObject> outCollection, int rate,
			Action<GameObject> onSpawnTick)
		{
			int index = 0;

			while (true)
			{
				SpawnGroup(prefab, Mathf.Min(count, rate), outCollection);
				count -= rate;

				if (onSpawnTick != null)
				{
					while (index < outCollection.Count)
					{
						onSpawnTick(outCollection[index]);
						index++;
					}
				}

				if (count == 0)
				{
					yield break;
				}

				yield return null;
			}
		}


		public Coroutine SpawnGroupAtRate<T>(T prefab, int count, List<T> outCollection, int rate)
			where T : Component
		{
			Assert.IsNotNull(prefab);
			Assert.IsTrue(count > 0);
			Assert.IsNotNull(outCollection);
			Assert.IsTrue(rate > 0);

			return StartCoroutine(SpawnGroupAtRateRoutine(prefab, count, outCollection, rate, null));
		}


		public Coroutine SpawnGroupAtRate<T>(T prefab, int count, List<T> outCollection, int rate,
			Action<GameObject> onSpawnTick)
			where T : Component
		{
			Assert.IsNotNull(prefab);
			Assert.IsTrue(count > 0);
			Assert.IsNotNull(outCollection);
			Assert.IsTrue(rate > 0);

			return StartCoroutine(SpawnGroupAtRateRoutine(prefab, count, outCollection, rate, onSpawnTick));
		}


		private IEnumerator SpawnGroupAtRateRoutine<T>(T prefab, int count, List<T> outCollection, int rate,
			Action<GameObject> onSpawnTick)
			where T : Component
		{
			int index = 0;

			while (true)
			{
				SpawnGroup(prefab, Mathf.Min(count, rate), outCollection);
				count -= rate;

				if (onSpawnTick != null)
				{
					while (index < outCollection.Count)
					{
						onSpawnTick(outCollection[index].gameObject);
						index++;
					}
				}

				if (count == 0)
				{
					yield break;
				}

				yield return null;
			}
		}


		public void Recycle(GameObject obj)
		{
			Assert.IsNotNull(obj);
			Assert.IsTrue(_instanceToPoolInfos.ContainsKey(obj));

			int limit = int.MaxValue;
			RecycleRecursive(obj, ref limit);
		}


		public void Recycle<T>(T obj)
			where T : Component
		{
			Recycle(obj.gameObject);
		}


		public Coroutine Recycle(GameObject obj, int rate)
		{
			Assert.IsNotNull(obj);
			Assert.IsTrue(_instanceToPoolInfos.ContainsKey(obj));
			Assert.IsTrue(rate > 0);

			return StartCoroutine(RecycleAtRateRoutine(obj, rate));
		}


		public Coroutine Recycle<T>(T obj, int rate)
			where T : Component
		{
			Assert.IsNotNull(obj);
			Assert.IsTrue(_instanceToPoolInfos.ContainsKey(obj.gameObject));
			Assert.IsTrue(rate > 0);

			return StartCoroutine(RecycleAtRateRoutine(obj.gameObject, rate));
		}


		public void RecycleGroup(List<GameObject> objs)
		{
			Assert.IsNotNull(objs);

			for (int i = 0; i < objs.Count; i++)
			{
				Recycle(objs[i]);
			}
		}


		public void RecycleGroup<T>(List<T> objs)
			where T : Component
		{
			Assert.IsNotNull(objs);

			for (int i = 0; i < objs.Count; i++)
			{
				Recycle(objs[i].gameObject);
			}
		}


		public Coroutine RecycleGroupAtRate(List<GameObject> objs, int rate)
		{
			Assert.IsNotNull(objs);
			Assert.IsTrue(rate > 0);

			return StartCoroutine(RecycleGroupAtRateRoutine(objs, rate));
		}


		public Coroutine RecycleGroupAtRate<T>(List<T> objs, int rate)
			where T : Component
		{
			Assert.IsNotNull(objs);
			Assert.IsTrue(rate > 0);

			return StartCoroutine(RecycleGroupAtRateRoutine(objs, rate));
		}


		private bool RecycleRecursive(GameObject obj, ref int limit)
		{
			var t = obj.GetComponent<Transform>();

			for (int i = t.childCount - 1; i >= 0; i--)
			{
				GameObject g = t.GetChild(i).gameObject;

				if (!RecycleRecursive(g, ref limit) ||
				    limit == 0)
				{
					return false;
				}
			}

			PoolInfo poolInfo;

			if (!_instanceToPoolInfos.TryGetValue(obj, out poolInfo))
			{
				return true;
			}

			List<GameObject> instances = poolInfo.Instances;
			Component[] poolables = _poolables[obj];

			for (int i = 0; i < poolables.Length; i++)
			{
				Component component = poolables[i];
				((IPoolable)component).OnRecycle(this);
			}

			t.SetParent(poolInfo.Container, false);
			obj.SetActive(false);
			instances.Add(obj);
			limit--;
			return true;
		}


		private IEnumerator RecycleAtRateRoutine(GameObject obj, int rate)
		{
			int limit = rate;

			while (!RecycleRecursive(obj, ref limit))
			{
				yield return null;
				limit = rate;
			}
		}


		private IEnumerator RecycleGroupAtRateRoutine(List<GameObject> objs, int rate)
		{
			int i = 0;

			while (i < objs.Count)
			{
				int limit = rate;

				while (i < objs.Count &&
					   RecycleRecursive(objs[i], ref limit) &&
					   limit > 0)
				{
					i++;
				}

				yield return null;
			}
		}


		private IEnumerator RecycleGroupAtRateRoutine<T>(List<T> objs, int rate)
			where T : Component
		{
			int i = 0;

			while (i < objs.Count)
			{
				int limit = rate;

				while (i < objs.Count &&
					   RecycleRecursive(objs[i].gameObject, ref limit) &&
					   limit > 0)
				{
					i++;
				}

				yield return null;
			}
		}
		


#if UNITY_EDITOR
		[ContextMenu("Rebuild Pool")]
		private void RebuildPool()
		{
			for (int i = transform.childCount - 1; i >= 0; i--)
			{
				DestroyImmediate(transform.GetChild(i).gameObject);
			}

			for (int i = 0; i < m_PoolInfoCategories.Length; i++)
			{
				for (int j = 0; j < m_PoolInfoCategories[i].Infos.Length; j++)
				{
					var instances = new List<GameObject>(m_PoolInfoCategories[i].Infos[j].PrePoolCount);
					var poolables = new List<PoolInfo.PoolableCotainer>(m_PoolInfoCategories[i].Infos[j].PrePoolCount);
					var container = new GameObject(m_PoolInfoCategories[i].Infos[j].Prefab.name).GetComponent<Transform>();
					container.SetParent(transform);

					while (instances.Count < m_PoolInfoCategories[i].Infos[j].PrePoolCount)
					{
						var obj = ((GameObject)PrefabUtility.InstantiatePrefab(m_PoolInfoCategories[i].Infos[j].Prefab));
						obj.SetActive(false);
						obj.transform.SetParent(container, false);
						obj.name = string.Format("{0} (Pooled)", m_PoolInfoCategories[i].Infos[j].Prefab.name);
						instances.Add(obj);
						poolables.Add(new PoolInfo.PoolableCotainer { Elements = obj.GetComponentsInChildren(typeof(IPoolable), true) });
					}

					PoolInfoUtility.SetContainer(ref m_PoolInfoCategories[i].Infos[j], container);
					PoolInfoUtility.SetInstances(ref m_PoolInfoCategories[i].Infos[j], instances);
					PoolInfoUtility.SetPoolables(ref m_PoolInfoCategories[i].Infos[j], poolables);
				}
			}

			EditorUtility.SetDirty(this);
		}
#endif
	}

	namespace Editor
	{
		public interface IPoolManager
		{
			PoolInfoCategory[] PoolInfoCategories { get; set; }
		}
	}
}
