using System;
using System.Collections.Generic;
using UnityEngine;


namespace Core.Pooling
{
	[Serializable]
	public struct PoolInfo
	#if UNITY_EDITOR
		: Editor.IPoolInfo
	#endif
	{
		[SerializeField] private GameObject m_Prefab;
		[SerializeField] private int m_PrePoolCount;
		[SerializeField, HideInInspector] private Transform m_Container;
		[SerializeField, HideInInspector] private List<GameObject> m_Instances;
		[SerializeField, HideInInspector] private List<PoolableCotainer> m_Poolables;




		public GameObject Prefab
		{
			get { return m_Prefab; }
		}


		public int PrePoolCount
		{
			get { return m_PrePoolCount; }
		}


		public List<GameObject> Instances
		{
			get { return m_Instances; }
		}


		public List<PoolableCotainer> Poolables
		{
			get { return m_Poolables; }
		}


		public Transform Container
		{
			get { return m_Container; }
		}




		public PoolInfo(GameObject prefab, Transform container, int prePoolCount)
		{
			m_Prefab = prefab;
			m_Container = container;
			m_PrePoolCount = prePoolCount;
			m_Instances = new List<GameObject>(prePoolCount);
			m_Poolables = new List<PoolableCotainer>(prePoolCount);
		}



#if UNITY_EDITOR
		void Editor.IPoolInfo.SetContainer(Transform container)
		{
			m_Container = container;
		}


		void Editor.IPoolInfo.SetInstances(List<GameObject> instances)
		{
			m_Instances = instances;
		}


		void Editor.IPoolInfo.SetPoolables(List<PoolableCotainer> poolables)
		{
			m_Poolables = poolables;
		}
#endif



		[Serializable]
		public struct PoolableCotainer
		{
			public Component[] Elements;
		}
	}

	namespace Editor
	{
		public interface IPoolInfo
		{
			void SetContainer(Transform container);


			void SetInstances(List<GameObject> instances);


			void SetPoolables(List<PoolInfo.PoolableCotainer> poolables);
		}


		public static class PoolInfoUtility
		{
			public static void SetContainer<T>(ref T poolInfo, Transform container)
				where T : struct, IPoolInfo
			{
				poolInfo.SetContainer(container);
			}


			public static void SetInstances<T>(ref T poolInfo, List<GameObject> instances)
				where T : struct, IPoolInfo
			{
				poolInfo.SetInstances(instances);
			}


			public static void SetPoolables<T>(ref T poolInfo, List<PoolInfo.PoolableCotainer> poolables)
				where T : struct, IPoolInfo
			{
				poolInfo.SetPoolables(poolables);
			}
		}
	}
}