﻿using System;


namespace Core.Pooling
{
	[Serializable]
	public struct PoolInfoCategory
	{
		public string Category;
		public PoolInfo[] Infos;
	}
}
