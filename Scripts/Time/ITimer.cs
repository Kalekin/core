﻿namespace Core.Time
{
	public interface ITimer
	{
		float DeltaTime { get; }


		float Time { get; }
	}
}