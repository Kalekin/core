﻿namespace Core.Time
{
	public abstract class Timer : ITimer
	{
		private static Timer _Default;
		private static Timer _Unscaled;




		public static Timer Default
		{
			get { return _Default; }
		}


		public static Timer Unscaled
		{
			get { return _Unscaled; }
		}


		public abstract float DeltaTime { get; }


		public abstract float Time { get; }




		static Timer()
		{
			_Default = new DefaultTimer();
			_Unscaled = new UnscaledTimer();
		}




		public static Timer GetTimer(TimerType type)
		{
			return type == TimerType.Default ? Default : Unscaled;
		}




		private sealed class DefaultTimer : Timer
		{
			public override float DeltaTime
			{
				get { return UnityEngine.Time.deltaTime; }
			}


			public override float Time
			{
				get { return UnityEngine.Time.time; }
			}
		}


		private sealed class UnscaledTimer : Timer
		{
			public override float DeltaTime
			{
				get { return UnityEngine.Time.unscaledDeltaTime; }
			}


			public override float Time
			{
				get { return UnityEngine.Time.unscaledTime; }
			}
		}
	}
}
