﻿namespace Core.Time
{
	public sealed class ScaleableTimer : Timer
	{
		private int _lastUpdatedFrame;
		private float _timeScale;
		private float _lastUnscaledTime;
		private float _time;




		public override float DeltaTime
		{
			get { return _timeScale; }
		}


		public override float Time
		{
			get
			{
				UpdateTime();
				return _time;
			}
		}


		public float TimeScale
		{
			get { return _timeScale; }
			set
			{
				UpdateTime();
				_timeScale = value;
			}
		}




		public ScaleableTimer()
		{
			_timeScale = 1f;
			Reset();
		}


		public ScaleableTimer(float timeScale)
		{
			_timeScale = timeScale;
			Reset();
		}




		private void UpdateTime()
		{
			if (_lastUpdatedFrame == UnityEngine.Time.frameCount)
			{
				return;
			}

			_lastUpdatedFrame = UnityEngine.Time.frameCount;
			float unscaledTime = UnityEngine.Time.unscaledTime;
			_time += (unscaledTime - _lastUnscaledTime) * _timeScale;
			_lastUnscaledTime = unscaledTime;
		}


		public void Reset()
		{
			_time = 0f;
			_lastUpdatedFrame = UnityEngine.Time.frameCount;
			_lastUnscaledTime = UnityEngine.Time.unscaledTime;
		}
	}
}