﻿using System;
using System.IO;
using Core.Debug;
using Core.Serialization;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;


namespace Core.Types
{
	[Serializable]
	public class ResourceLink<TObject> : ResourceLink, ISerializationCallbackReceiver, IJsonSerializable
		where TObject : UnityEngine.Object
	{
#if UNITY_EDITOR
		[SerializeField] private UnityEngine.Object m_ObjectReference;
#endif
		[SerializeField] private string m_ResourcePath;




		public string ResourcePath
		{
			get
			{
#if UNITY_EDITOR
				ValidatePath();
#endif
				return m_ResourcePath;
			}
		}




		public ResourceLink(string path)
		{
#if UNITY_EDITOR
			if (string.IsNullOrEmpty(path))
			{
				m_ResourcePath = string.Empty;
				m_ObjectReference = null;
			}
			else
			{
				if (!File.Exists(path))
				{
					Debug.Logger.LogWarning(string.Format("Trying to create a ResourceLink at project path {0} but no such file exists.", path));

					m_ObjectReference = null;
					m_ResourcePath = string.Empty;
					return;
				}

				var objectReference = AssetDatabase.LoadAssetAtPath(path, typeof(UnityEngine.Object));

				if (objectReference == null)
				{
					Debug.Logger.LogWarning(string.Format("Trying to create a ResourceLink at project path {0} but no Unity Object found.", path));

					m_ObjectReference = null;
					m_ResourcePath = string.Empty;
					return;
				}

				if (!objectReference.GetType().IsAssignableFrom(typeof(TObject)))
				{
					Debug.Logger.LogWarning(string.Format("Trying to create a ResourceLink for type {0} at project path {1} but found non-inherited type {2} instead.", typeof(TObject).FullName, path, objectReference.GetType().FullName));

					m_ObjectReference = null;
					m_ResourcePath = string.Empty;
					return;
				}

				m_ResourcePath = path;
				m_ObjectReference = objectReference;
				ValidatePath();
			}
#else
			if (string.IsNullOrEmpty(path))
			{
				m_ResourcePath = string.Empty;
			}
#endif
		}


		public ResourceLink()
			: this(string.Empty)
		{

		}




		public static implicit operator string(ResourceLink<TObject> resourceLink)
		{
			return resourceLink.ResourcePath;
		}


		public static implicit operator ResourceLink<TObject>(string path)
		{
			return new ResourceLink<TObject>(path);
		}




		public TObject LoadResource()
		{
			return Resources.Load(ResourcePath, typeof(TObject)) as TObject;
		}


		public ResourceRequest LoadResourceAsync()
		{
			return Resources.LoadAsync<TObject>(ResourcePath);
		}


		public override string ToString()
		{
			return ResourcePath;
		}


		public override bool Equals(object obj)
		{
			if (obj == null || GetType() != obj.GetType())
			{
				return false;
			}

			return ResourcePath.Equals(((ResourceLink<TObject>)obj).ResourcePath);
		}


		public override int GetHashCode()
		{
			return ResourcePath.GetHashCode();
		}

#if UNITY_EDITOR
		private void ValidatePath()
		{
			try
			{
				if (m_ObjectReference.Equals(null) &&
					BuildPipeline.isBuildingPlayer)
				{
					Debug.Logger.LogWarning("The referenced object for this link is missing!");
				}
			}
			catch (NullReferenceException)
			{
			}

			if (m_ObjectReference == null)
			{
				return;
			}


			string assetPath = AssetDatabase.GetAssetPath(m_ObjectReference);
			int resourcesIndex = assetPath.IndexOf("Resources/", StringComparison.InvariantCulture);

			if (resourcesIndex == -1)
			{
				m_ResourcePath = string.Empty;
				return;
			}

			int startIndex = resourcesIndex + "Resources/".Length;
			string resourcesPath = assetPath.Substring(startIndex, assetPath.Length - startIndex);
			m_ResourcePath = string.Format("{0}/{1}", Path.GetDirectoryName(resourcesPath), Path.GetFileNameWithoutExtension(resourcesPath));
		}
#endif

		void ISerializationCallbackReceiver.OnBeforeSerialize()
		{
#if UNITY_EDITOR
			ValidatePath();
#endif
		}


		void ISerializationCallbackReceiver.OnAfterDeserialize()
		{
		}


		void IJsonSerializable.FromJsonElement(JsonElement element)
		{
			Assert.IsNotNull(element);
			Assert.IsTrue(element.IsJsonPrimitive);
			Assert.IsTrue(element.AsJsonPrimitive().IsString);

			m_ResourcePath = element.AsString();
		}


		JsonElement IJsonSerializable.ToJsonElement(JsonPool pool)
		{
			JsonPrimitive ret = pool.GetPrimitive();

			ret.SetValue(m_ResourcePath);

			return ret;
		}
	}
}
