﻿#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;


namespace CoreEditor.Utility
{
	public static class GizmosExt
	{
		public static readonly Color ColliderColor = new Color(162f/255f, 203f/255f, 177f/255f);
		public static void DrawCapsule(Vector3 center, Quaternion rotation, float height, float radius)
		{
			var normal = rotation * Vector3.up;
			var forwardOffset = rotation*Vector3.forward*radius;
			var leftOffset = rotation*Vector3.left*radius;
			var capOffset = new Vector3(0f, Mathf.Max(0f, height*0.5f - radius), 0f);
			Vector3 topCap = center + capOffset;
			Vector3 botCap = center - capOffset;

			Color t = Handles.color;
			Handles.color = Gizmos.color;
			Handles.DrawWireDisc(topCap, normal, radius);
			Handles.DrawWireDisc(botCap, normal, radius);
			Handles.DrawLine(topCap + forwardOffset, botCap + forwardOffset);
			Handles.DrawLine(topCap - forwardOffset, botCap - forwardOffset);
			Handles.DrawLine(topCap + leftOffset, botCap + leftOffset);
			Handles.DrawLine(topCap - leftOffset, botCap - leftOffset);
			Handles.DrawWireArc(topCap, leftOffset, forwardOffset, 180f, radius);
			Handles.DrawWireArc(topCap, forwardOffset, -leftOffset, 180f, radius);
			Handles.DrawWireArc(botCap, leftOffset, -forwardOffset, 180f, radius);
			Handles.DrawWireArc(botCap, -forwardOffset, -leftOffset, 180f, radius);
			Handles.color = t;
		}


		public static void DrawWireCylinder(Vector3 center, Quaternion rotation, float height, float radius)
		{
			var normal = rotation * Vector3.up;
			var forwardOffset = rotation * Vector3.forward * radius;
			var leftOffset = rotation * Vector3.left * radius;
			var capOffset = normal * height * 0.25f;
			Vector3 topCap = center + capOffset;
			Vector3 botCap = center - capOffset;

			Handles.DrawWireDisc(topCap, normal, radius);
			Handles.DrawWireDisc(botCap, normal, radius);
			Handles.DrawLine(topCap + forwardOffset, botCap + forwardOffset);
			Handles.DrawLine(topCap - forwardOffset, botCap - forwardOffset);
			Handles.DrawLine(topCap + leftOffset, botCap + leftOffset);
			Handles.DrawLine(topCap - leftOffset, botCap - leftOffset);
		}
	}
}
#endif