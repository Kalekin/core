﻿#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System.IO;


namespace CoreEditor.Utility
{
	public static class ScriptableObjectUtility
	{
		public static T CreateAsset<T>() where T : ScriptableObject
		{
			var asset = ScriptableObject.CreateInstance<T>();
		
			SaveAsset(asset);

			return asset;
		}


		public static void SaveAsset<T>(T asset) where T : ScriptableObject
		{
			string path = AssetDatabase.GetAssetPath(Selection.activeObject);
			if (path == "")
			{
				path = "Assets";
			}
			else if (Path.GetExtension(path) != "")
			{
				path = path.Replace(Path.GetFileName(AssetDatabase.GetAssetPath(Selection.activeObject)), "");
			}

			string assetPathAndName = AssetDatabase.GenerateUniqueAssetPath(path + "/New " + typeof(T).ToString() + ".asset");

			AssetDatabase.CreateAsset(asset, assetPathAndName);

			AssetDatabase.SaveAssets();
			EditorUtility.FocusProjectWindow();
			Selection.activeObject = asset;
		}
	}
}
#endif