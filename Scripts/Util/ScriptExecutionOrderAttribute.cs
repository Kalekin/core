﻿using System;


namespace Core
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = true)]
    public class ScriptExecutionOrderAttribute : Attribute
    {
        private int _order;




        public int Order
        {
            get
            {
                return _order;
            }
        }




        public ScriptExecutionOrderAttribute(int order)
        {
            _order = order;
        }
    }
}