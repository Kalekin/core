﻿using UnityEditor;
using System.IO;
using System.Linq;
using System.Xml.Linq;


namespace CoreEditor.Utility
{
	public class VisualStudioProjectPostProcess : AssetPostprocessor
	{
		public static void OnGeneratedCSProjectFiles()
		{
			GenerateSoultion();
		}

		
		private static void GenerateSoultion()
		{
			string currentDir = Directory.GetCurrentDirectory();
			string[] projects =
				Directory.GetFiles(currentDir, "*.csproj").Where(proj => !proj.Contains("VisualStudio")).ToArray();

			foreach (var file in projects)
			{
				ProcessProject(file);
			}

			//foreach (var file in Directory.GetFiles(currentDir, "*.sln"))
			//{
			//	if (!file.Contains("VisualStudio"))
			//		ProcessSoultion(file, projects);
			//}
		}


		private static void ProcessSoultion(string file, string[] projects)
		{
			string slnFile = File.ReadAllText(file);
			foreach (var project in projects)
			{
				string projectName = Path.GetFileName(project);
				slnFile = slnFile.Replace(projectName, "VisualStudio." + projectName);
			}
			string newFile = Path.Combine(Path.GetDirectoryName(file), "VisualStudio." + Path.GetFileName(file));

			//File.WriteAllText(newFile, slnFile);
			File.WriteAllText(file, slnFile);
		}


		private static void ProcessProject(string file)
		{
			XNamespace ns = "http://schemas.microsoft.com/developer/msbuild/2003";
			XDocument csproj = XDocument.Load(file);
			var hints = csproj.Descendants(ns + "HintPath");
			foreach (var hint in hints)
			{
				if (hint.Value.StartsWith("/Applications") ||
					hint.Value.StartsWith("/Users"))
				{
					string newHint = hint.Value.Insert(0, "Z:");
					newHint = newHint.Replace('/', '\\');
					hint.Value = newHint;
				}
			}
			string newFile = Path.Combine(Path.GetDirectoryName(file), "VisualStudio." + Path.GetFileName(file));

			//csproj.Save(newFile);
			csproj.Save(file);
		}
	}
}