﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core;
using UnityEditor;
using UnityEngine;


namespace CoreEditor
{
    public class ScriptExecutionOrderManager : ScriptableObject
    {
        [SerializeField, HideInInspector] private List<string> m_CheckList;
        private static int _delayCounter;
        private static ScriptExecutionOrderManager _Instance;




        private bool RequiresCheck
        {
            get
            {
                return CheckList != null && CheckList.Count > 0;
            }
        }

        public List<string> CheckList
        {
            get
            {
                if (m_CheckList == null)
                {
                    m_CheckList = new List<string>();
                }

                return m_CheckList;
            }
        }


        private void OnEnable()
        {
            if (_Instance != null &&
                _Instance != this)
            {
                DestroyImmediate(this);
                return;
            }

            _Instance = this;

            if (RequiresCheck)
            {
                if (EditorApplication.isCompiling)
                {
                    if (EditorApplication.update == null ||
                        !EditorApplication.update.GetInvocationList().Contains((Action) EditorApplicationUpdateHandler))
                    {
                        EditorApplication.update += EditorApplicationUpdateHandler;
                    }
                }
                else
                {
                    if (EditorApplication.update == null ||
                        !EditorApplication.update.GetInvocationList().Contains((Action) EditorApplicationUpdateHandler))
                    {
                        EditorApplication.update += EditorApplicationUpdateHandler;
                    }
                    //DoChecks();
                }
            }
        }



        public static void MarkForCheck(string scriptPath)
        {
            if (_Instance == null)
            {
                _Instance = CreateInstance<ScriptExecutionOrderManager>();
            }


            if (EditorApplication.isCompiling)
            {
                _Instance.CheckList.Add(scriptPath);

                if (EditorApplication.update == null ||
                    !EditorApplication.update.GetInvocationList().Contains((Action) EditorApplicationUpdateHandler))
                {
                    EditorApplication.update += EditorApplicationUpdateHandler;
                }
            }
            else
            {
                CheckItem(scriptPath);
            }
        }




        private static void EditorApplicationUpdateHandler()
        {
            if (!EditorApplication.isCompiling)
            {
                _delayCounter++;
                if (_delayCounter == 5)
                {
                    EditorApplication.update -= EditorApplicationUpdateHandler;
                    DoChecks();
                }
            }
        }



        private static void CheckItem(string scriptPath)
        {
            var script = AssetDatabase.LoadAssetAtPath(scriptPath, typeof (MonoScript)) as MonoScript;

            if (script == null)
            {
                return;
            }

            Type classType = script.GetClass();

            if (classType == null)
            {
                return;
            }

            var scriptExecutionAttributes =
                classType.GetCustomAttributes(typeof (ScriptExecutionOrderAttribute), true) as
                    ScriptExecutionOrderAttribute[];

            if (scriptExecutionAttributes != null &&
                scriptExecutionAttributes.Length > 0)
            {
                ScriptExecutionOrderAttribute scriptExecutionAttribute = scriptExecutionAttributes[0];

                int currentExecutionOrder = MonoImporter.GetExecutionOrder(script);

                if (currentExecutionOrder != scriptExecutionAttribute.Order)
                {
                    MonoImporter.SetExecutionOrder(script, scriptExecutionAttribute.Order);
                }
            }
        }


        private static void DoChecks()
        {
            for (int i = _Instance.CheckList.Count - 1; i >= 0; i--)
            {
                CheckItem(_Instance.CheckList[i]);
                _Instance.CheckList.RemoveAt(i);
            }
        }
    }
}