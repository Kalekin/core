﻿using System;
using Core.Types;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;


namespace CoreEditor
{
	[CustomPropertyDrawer(typeof(ResourceLink), true)]
	public class ResourceLinkPropertyDrawer : PropertyDrawer
	{
		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			SerializedProperty serializedObjectReference = property.FindPropertyRelative("m_ObjectReference");
			SerializedProperty serializedPath = property.FindPropertyRelative("m_ResourcePath");

			Object objectReference = serializedObjectReference.objectReferenceValue;
			Type fieldType = fieldInfo.FieldType.IsArray ? fieldInfo.FieldType.GetElementType() : fieldInfo.FieldType;
			Type inheritType;

			while ((inheritType = fieldType.GetBaseTypeRawGeneric(typeof(ResourceLink<>))) == null)
			{
				fieldType = fieldType.GetGenericArguments()[0];
			} 


			Type referenceType = inheritType.GetGenericArguments()[0];

			Rect objectPosition = position;
			objectPosition.height *= 0.5f;
			Rect labelPosition = position;
			labelPosition.yMin += position.height*0.5f;
			Rect iconPosition = labelPosition;
			iconPosition.yMin += 2;
			iconPosition.xMin = iconPosition.xMax - 14 - EditorGUI.indentLevel*15f;
			labelPosition.xMin += 1;
			labelPosition.xMax = labelPosition.xMax - 1;
			Rect boxPosition = labelPosition;
			boxPosition.xMin += EditorGUI.indentLevel * 15f;

			objectReference = EditorGUI.ObjectField(objectPosition, objectReference, referenceType, false);

			Color color;
			GUIContent icon;
			string pathText;

			if (objectReference == null)
			{
				color = new Color(1f, 0.7f, 0.3f);
				icon = EditorGUIUtility.IconContent("lightMeter/redLight");
				pathText = "No object reference.";
			}
			else if (ObjectIsInResourceFolder(objectReference))
			{
				color = new Color(0.3f, 0.9f, 0.3f);
				icon = EditorGUIUtility.IconContent("lightMeter/greenLight");
				pathText = serializedPath.stringValue;
			}
			else
			{
				color = new Color(0.9f, 0.3f, 0.3f);
				icon = EditorGUIUtility.IconContent("lightMeter/orangeLight");
				pathText = "Referenced object is not in a resources folder.";
			}

			Color t = GUI.contentColor;
			GUI.color = color;
			GUI.Box(boxPosition, GUIContent.none);
			EditorGUI.LabelField(labelPosition, pathText);
			GUI.color = t;

			serializedObjectReference.objectReferenceValue = objectReference;

			EditorGUI.LabelField(iconPosition, icon);
		}


		public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
		{
			return base.GetPropertyHeight(property, label)*2f;
		}


		private static bool ObjectIsInResourceFolder(Object obj)
		{
			string assetPath = AssetDatabase.GetAssetPath(obj);
			int resourcesIndex = assetPath.IndexOf("Resources/", StringComparison.InvariantCulture);

			return resourcesIndex != -1;
		}
	}
}
