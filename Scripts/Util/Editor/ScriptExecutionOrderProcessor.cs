﻿using UnityEditor;


namespace CoreEditor
{
	public class ScriptExecutionOrderPostProcessor : AssetPostprocessor
	{
		private static void OnPostprocessAllAssets(string[] importedAssets, string[] deletedAssets, string[] movedAssets,
			string[] movedFromAssetPaths)
		{

			for (int i = 0; i < importedAssets.Length; ++i)
			{
				string assetPath = importedAssets[i];

				if (assetPath.EndsWith("cs"))
				{
					ScriptExecutionOrderManager.MarkForCheck(assetPath);
				}
			}

		}
	}
}